/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * maths.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * maths.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CORE__MATHS_HPP__
#define GEDEON_CORE__MATHS_HPP__

#include <climits>
#include <cstdlib>
#include <algorithm>
#include <utility>

namespace gedeon {

namespace Maths {

static const double PI = 3.14159265358979323846;

static const double TWO_PI = 6.28318530717958647692;

static const double PI_TWO = 1.57079632679489661923;

static const double PI_FOUR = 0.78539816339744830962;

static const double DEG2RAD = 0.01745329251994329576;

static const double RAD2DEG = 57.2957795130823208767;

static const double GOLDEN_NUMBER = 1.61803398874989484820;


/**
 * \brief Transform a degree value into radian
 * \param deg the input value in degrees
 * \return the corresponding a radian value
 */
static inline double degToRad(const double& deg) {
	return deg * DEG2RAD;
}

/**
 * \brief Transform a radian value into degree
 * \param rad the input value in radians
 * \return the corresponding a degree value
 */
static inline double radToDeg(const double& rad) {
	return rad * RAD2DEG;
}

/**
 * \brief Return if a value is a power of two or not
 * \param value the value we want to find if it is a power of two or not
 * \return the corresponding a degree value
 */
static inline bool isPowerOfTwo(const unsigned int& value) {
	return ((value > 0) && ((value & (value - 1)) == 0));
}

/**
 * \brief Compute the square root of a float value
 * \param value the float value we want the square root value
 * \return the square root
 */
static inline unsigned int fastSqrtui(const unsigned int& value) {
	unsigned int temp, g = 0, b = 0x8000, bshft = 15;
	unsigned int val = value;
	do {
		if (val >= (temp = (((g << 1) + b) << bshft--))) {
			g += b;
			val -= temp;
		}
	} while (b >>= 1);
	return g;
}

/**
 * \brief Compute the square root of a float value
 * \param value the float value we want the square root value
 * \return the square root
 */
inline float fastSqrtuif(const float& value) {
	float vhalf = value * 0.5f;
	long i = *(long *) &value;
	i = 0x5f3759df - (i >> 1);
	float value_ = static_cast<float>(i);
	return value_ * (1.5f - vhalf * value_ * value_);
}

/**
 * \brief Compute the factorial of a given integer
 * \param value the integer used for the factorial
 * \return the factorial
 * \author Francois de Sorbier
 */
inline int factorial(const int& value) {
	int i, factx = 1;
	for (i = 1; i <= value; ++i)
		factx *= i;
	return factx;
}

/**
 * \brief Return the sign of a given value
 * \param a The value we want to know the sign
 * \return the highest power of two
 * \sa ceilPowerOfTwo
 * \author Francois de Sorbier
 */
template<typename T>
static inline T getSign(const T& a) {
	if (a == T(0))
		return a;
	return a > T(0) ? T(1) : T(-1);
}

/**
 * \brief Find the least power of two near a given particular value
 * \param n the upper limit
 * \return the highest power of two
 * \sa ceilPowerOfTwo
 * \author Francois de Sorbier
 */
static inline int ceilPowerOfTwo(const int& n) {
	int k = n;
	if (k == 0)
		return 1;
	k--;
	for (unsigned int i = 1; i < sizeof(int) * CHAR_BIT; i <<= 1)
		k = k | k >> i;
	return k + 1;
}

/**
 * \brief Find the highest power of two near a given integer
 * \param n the upper limit
 * \return the highest power of two
 * \sa ceilPowerOfTwo
 * \author Francois de Sorbier
 */
static inline int floorPowerOfTwo(const int& n) {
	return ceilPowerOfTwo(n) >> 1;
}

/**
 * \brief Return a random number in a given range
 * \param min the minimun bound of the output random value.
 * \param max the maximum bound of the output random value.
 * \return a random value between min and max.
 * \author Francois de Sorbier
 */
template<class T> T getRandom(const T& min, const T& max) {
	T min_ = min;
	T max_ = max;
	if (min > max) {
		std::swap(min_, max_);
	}
	T range = max_ - min_;

	return T(float(range) * float(rand()) / float(RAND_MAX)) + min_;
}

/**
 * \brief Clamp a value given a range
 * \param value the value to be clamped
 * \param min the minimun bound of the clamp
 * \param max the maximum bound of the clamp
 * \return the clamped value
 * \author Francois de Sorbier
 */
template<class T>
static inline T clamp(const T&  value, const T& min, const T&  max){
	return (value > max) ? max : ((value < min) ? min : value);
}

}

}

#endif
