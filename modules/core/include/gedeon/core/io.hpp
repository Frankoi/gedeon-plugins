/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * io.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * io.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CORE__IO_HPP__
#define GEDEON_CORE__IO_HPP__

#include "config.hpp"

#include <sstream>
#include <iomanip>

namespace gedeon {

namespace IO {

/**
 * \brief Convert a numerical value into a string
 *
 *	\param value A numerical value
 *
 *	\return The value converted into a string
 *
 * \author Francois de Sorbier
 */
template<typename T>
static inline std::string numberToString(const T & value) {
	std::ostringstream os;
	os << value;
	return os.str();
}

/**
 * \brief Append a formated integer to a string
 *
 *	\param number The integer we want to append
 *	\param format The number of digit in the string
 *
 *	\return The final formated string with the number included
 *
 * \author Francois de Sorbier
 */
static inline std::string stringEnumerator(
		const unsigned int& number, const unsigned int& format) {
	std::ostringstream oss;
	oss << std::setfill('0') << std::setw(format) << number;
	return oss.str();
}

}

}

#endif
