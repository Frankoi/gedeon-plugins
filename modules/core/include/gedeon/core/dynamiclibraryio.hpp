/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * dynamiclibraryio.hpp created in 03 2014.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * dynamiclibraryio.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CORE__DYNAMIC_LIBRARY_IO_HPP__
#define GEDEON_CORE__DYNAMIC_LIBRARY_IO_HPP__

#include "gedeon/core/config.hpp"
#include "gedeon/core/exception.hpp"

#if defined _WIN32 || defined _MSC_VER
#define VC_EXTRALEAN
#include <windows.h>
#else
#include <dlfcn.h>
#endif

namespace gedeon {

/**
 * \brief The namespace contains functions for management of dynamic libraries
 *
 * \author Francois de Sorbier
 */
class GEDEON_EXPORT DynamicLibraryIO {

	typedef void * DLHandlep; // Handle to the dynamic library pointer

public:
	/**
	 * \brief Constructor that loads a dynamic library defined by its name without extension (.dll/.so)
	 *
	 * An exception is thrown if the loading fails
	 * \param dir The directory where is located the library
	 * \param dlname The name of the dynamic library without its extension (.dll/.so) and the pre-string lib
	 * \exception Could load the dynamic library
	 */
	DynamicLibraryIO(const std::string& dir, const std::string& dlname) {
		std::string dir_up = dir;
		if(dir_up.size() > 0 && dir_up.rfind("/") != (dir_up.size()-1)){
			dir_up.append("/");
		}
#if defined _WIN32 || defined _MSC_VER
		this->handle = ::LoadLibraryA((dir_up + dlname + ".dll").c_str());
		if(0 == this->handle) {
			DWORD dw = ::GetLastError();
			LPVOID lpMsgBuf;
			::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
			throw Exception(std::runtime_error((LPCTSTR)lpMsgBuf));
		}
#else
		this->handle = dlopen(( dir_up + "lib" + dlname + ".so").c_str(), RTLD_LAZY);
		if (0 == this->handle) {
			throw Exception(std::runtime_error(::dlerror()));
		}
#endif
	}

	/**
	 * \brief Destructor
	 */
	~DynamicLibraryIO(void){
#if defined _WIN32 || defined _MSC_VER
		BOOL result = ::FreeLibrary(this->handle);
		if(FALSE == result) {
			DWORD dw = ::GetLastError();
			LPVOID lpMsgBuf;
			::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
			throw Exception(std::runtime_error((LPCTSTR)lpMsgBuf));
		}
#else
		if (0 != this->handle) {
			int result = ::dlclose(this->handle);
			if (0 != result) {
				throw Exception(std::runtime_error(::dlerror()));
			}
		}
#endif
	}

	/**
	 * \brief Return a pointer on a function for which the type is defined by the template FSignaturep and the name by the parameter
	 *
	 * An exception is thrown if the address of the function cannot be obtained
	 *
	 * \param function_name The name of the function we want to get a pointer on
	 * \exception std::runtime_error Could get the address of the given function
	 */
	template<typename FSignaturep>
	FSignaturep getFunctionPointer(const std::string& function_name) {

#if defined _WIN32 || defined _MSC_VER
		FARPROC function_address = ::GetProcAddress(this->handle, function_name.c_str());
		if(function_address == NULL) {
			DWORD dw = ::GetLastError();
			LPVOID lpMsgBuf;
			::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL );
			throw Exception(std::runtime_error((LPCTSTR)lpMsgBuf));
		}
		return reinterpret_cast<FSignaturep>(function_address);
#else
		::dlerror(); // clear
		void *function_address = ::dlsym(this->handle, function_name.c_str());
		char *error = 0;
		if ((error = ::dlerror()) != 0) {
			throw Exception(std::runtime_error(error));
		}
		return reinterpret_cast<FSignaturep>(function_address);

#endif

	}

private:

#if defined _WIN32 || defined _MSC_VER
HMODULE handle;
#else
DLHandlep handle;
#endif

};
// end class DynamicLibraryIO

}// end namespace gedeon

#endif
