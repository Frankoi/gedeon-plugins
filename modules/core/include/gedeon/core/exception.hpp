/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * exception.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * exception.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CORE__EXCEPTION_HPP__
#define GEDEON_CORE__EXCEPTION_HPP__

#include "config.hpp"

#include <string>

namespace gedeon {

/**
 * \brief Class for managing exceptions
 *
 * \author Francois de Sorbier
 */
class GEDEON_EXPORT Exception: std::exception {

public:
	/**
	 * \brief Constructor of an exception based on strings
	 *
	 * \param funcname The name of the function sending the exception
	 * \param err The details of the exception
	 *
	 */
	Exception(const std::string& funcname, const std::string& err);

	/**
	 * \brief Constructor an exception based on a standard exception
	 *
	 * \param e another exception
	 *
	 */
	Exception(const std::exception & e);

	/**
	 * \brief Constructor an exception based on another exception
	 *
	 * \param e another exception
	 *
	 */
	Exception(const Exception & e);

	/**
	 * \brief Destructor
	 *
	 */
	virtual ~Exception() throw () {
	}

protected:
	std::string str;

public:

	/**
	 * \brief return the string related with the exception
	 *
	 * \return the exception string
	 *
	 */
	virtual inline std::string errorString() const {
		return str;
	}

	/**
	 * \brief return the string related with the exception
	 *
	 * \return the exception string
	 *
	 */
	virtual const char* what() const throw () {
		return str.c_str();
	}
};

/**
 * \brief Operator redefinition for displaying the string of the exception on an output stream
 *
 * \param stream The output stream
 * \param err The exception to display
 * \return The exception string appended to the the output stream
 *
 */
GEDEON_EXPORT inline std::ostream& operator <<(std::ostream& stream, const Exception & err) {
	return stream << err.errorString();
}

}

#endif
