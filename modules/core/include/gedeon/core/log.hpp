/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * log.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * log.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CORE__LOG_HPP__
#define GEDEON_CORE__LOG_HPP__

#include <iostream>
#include <fstream>
#include <string>

#include "gedeon/core/event.hpp"
#include "gedeon/core/config.hpp"

namespace gedeon {

/**
 * \brief Enumeration that describes the messages that will be transmitted through the log message system.
 *
 * \sa Log
 *
 * \author Francois de Sorbier
 */
enum LogLevel {
	GEDEON_QUIET = 0, ///<Nothing is displayed
	GEDEON_MINIMAL, ///< Errors only are displayed
	GEDEON_MODERATE, ///< Errors and Infos are displayed
	GEDEON_COMPLETE, ///< Errors, Warning and Infos are displayed
	GEDEON_COMPLETE_WITH_DEBUG, ///< Debug messages, Errors, Warning and Infos are displayed
	GEDEON_DEBUG_ONLY, ///< Debug messages only are displayed
};

/**
 * \brief Class that directly receives log messages
 *
 * This class should is a singleton and is based on an event paradigm.
 *
 * One or multiple logger are attached to this singleton.
 *
 * Every message (error, warning, info and debug) is passed to this structure.
 *
 * It will then transfer the message to the corresponding observers.
 *
 * This design allow to have custom loggers (console, standard output, file).
 *
 * \sa Logger
 *
 * \author Francois de Sorbier
 */
class GEDEON_EXPORT LogEmitter: public EventSender {

public:

	/**
	 * \brief Default constructor
	 *
	 */
	LogEmitter(void) /*: level(COMPLETE) */{
		addEventName("error_log");
		addEventName("debug_log");
		addEventName("info_log");
		addEventName("warning_log");
	}

	/**
	 * \brief Destructor
	 *
	 */
	~LogEmitter(void) {
	}
private:
	/**
	 * \brief Copy constructor
	 *
	 * \param l The Log object that will be copied
	 */
	LogEmitter(const LogEmitter& l);

	/**
	 * \brief Redefinition of the equality operator
	 *
	 * \param l The Log object that will be copied
	 *
	 * \return the copied Log
	 */
	LogEmitter& operator=(const LogEmitter& l);

public:

	/**
	 * \brief Add a warning in the log
	 *
	 * \param functionname The name of the function sending the warning
	 * \param message The details of the warning
	 *
	 */
	void warning(const std::string& functionname, const std::string& message);

	/**
	 * \brief Add a warning in the log
	 *
	 * \param message The details of the warning
	 *
	 */
	void warning(const std::string& message);

	/**
	 * \brief Add an error in the log
	 *
	 * \param functionname The name of the function sending the error
	 * \param message The details of the error
	 *
	 */
	void error(const std::string& functionname, const std::string& message);

	/**
	 * \brief Add an error in the log
	 *
	 * \param message The details of the error
	 *
	 */
	void error(const std::string& message);

	/**
	 * \brief Add a debug info in the log
	 *
	 * \param functionname The name of the function sending the debug info
	 * \param message The details of the debug info
	 *
	 */
	void debug(const std::string& functionname, const std::string& message);

	/**
	 * \brief Add a debug info in the log
	 *
	 * \param message The details of the debug info
	 *
	 */
	void debug(const std::string& message);

	/**
	 * \brief Add an info in the log
	 *
	 * \param message The details of the info
	 *
	 */
	void info(const std::string& message);

};

namespace Log{
	/**
	 * \brief Return the instance on a unique Log
	 *
	 * \return The log object
	 *
	 */
	 GEDEON_EXPORT LogEmitter &getInstance(void);

	/**
	 * \brief Return the instance on a unique Log
	 *
	 * \return The log object
	 *
	 */
	GEDEON_EXPORT LogEmitter &add(void);
}
/**
 * \brief Structure that holds the data sent by the log system to a logger
 *
 * \author Francois de Sorbier
 */
struct GEDEON_EXPORT LogData: public EventData {
	/**
	 * \brief Constructor of the holder of data
	 *
	 * \param evname The name of the event related to the data
	 * \param origin A string depicting the name of the method/class that emitted the log message
	 * \param msg The message that will be logged
	 */
	LogData(const std::string& evname, const std::string& origin, const std::string& msg);

	/**
	 * \brief Default destructor
	 */
	~LogData(void);

	std::string func_origin;  ///< Name of the function that emitted the message
	std::string msg_description;  ///< Logging message
	long long unsigned int timestamp;  ///< Timestamp describing when the message was created
};

/**
 * \brief Default logging message holder
 *
 * Message will be displayed on the standard output
 *
 * \author Francois de Sorbier
 */
class GEDEON_EXPORT Logger {
public:

	/**
	 * \brief Constructor
	 *
	 *	Initialize the connections
	 */
	Logger(void);

	/**
	 * \brief Destructor
	 *
	 *	Destroy the connections
	 */
	virtual ~Logger(void);

	/**
	 * \brief Connect the logger to the Log event system
	 *
	 */
	void connect(void);

	/**
	 * \brief Disconnect the logger to the Log event system
	 *
	 */
	void disconnect(void);

	/**
	 * \brief Automatically called when an warning message is emitted by the logging system
	 *
	 * \param e A weak pointer on the related logging data
	 * \sa LogData
	 */
	virtual void warning(EventDataWPtr e);

	/**
	 * \brief Automatically called when an error message is emitted by the logging system
	 *
	 * \param e A weak pointer on the related logging data
	 * \sa LogData
	 */
	virtual void error(EventDataWPtr e);

	/**
	 * \brief Automatically called when a debug message is emitted by the logging system
	 *
	 * \param e A weak pointer on the related logging data
	 * \sa LogData
	 */
	virtual void debug(EventDataWPtr e);

	/**
	 * \brief Automatically called when an informative message is emitted by the logging system
	 *
	 * \param e A weak pointer on the related logging data
	 * \sa LogData
	 */
	virtual void info(EventDataWPtr e);

	/**
	 * \brief Return the current level of logging
	 *
	 * \return the level
	 * \sa LogLevel
	 *
	 */
	virtual enum LogLevel getLevel(void) const;

	/**
	 * \brief Set the level of logging
	 *
	 * \param level The new logging level
	 * \sa LogLevel
	 *
	 */
	virtual void setLevel(const enum LogLevel & level);

protected:

	LogData* getData(EventDataWPtr e);

	boost::signals2::connection c_info; ///< Holds the info event connection
	boost::signals2::connection c_error; ///< Holds the error event connection
	boost::signals2::connection c_debug; ///< Holds the debug event connection
	boost::signals2::connection c_warning; ///< Holds the warning event connection
	enum LogLevel level; ///< Describes the current level for displaying the logs
};

}

#endif
