/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * core.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * core.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
\***************************************************************************/

#ifndef GEDEON_CORE__CORE_HPP__
#define GEDEON_CORE__CORE_HPP__

#if defined _WIN32 || defined _MSC_VER
#define NOMINMAX
#define _WINSOCKAPI_
#include <windows.h>
#endif

#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/core/maths.hpp"
#include "gedeon/core/time.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/converter.hpp"
#include "gedeon/core/dynamiclibraryio.hpp"

#endif
