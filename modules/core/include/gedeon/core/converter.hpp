/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * converter.hpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * converter.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CORE__CONVERTER_HPP__
#define GEDEON_CORE__CONVERTER_HPP__

#include "config.hpp"

namespace gedeon {

namespace Converter {

/*
 * Measure converters
 */

/**
 * \brief Convert feet into meters
 *
 * \param f The input feet
 * \return the converted feet into meters
 */
GEDEON_EXPORT inline float feetToMeters(float f) {
	return f * 0.3048f;
}

/**
 * \brief Convert meters into feet
 *
 * \param m The input meters
 * \return the converted meters into feet
 */
GEDEON_EXPORT inline float metersToFeet(float m) {
	return m * 3.2808399f;
}

/*
 * Color converters
 */

/**
 * \brief Convert from YUV444 to RGB
 *
 * From http://en.wikipedia.org/wiki/YUV#Y.27UV444_to_RGB888_conversion
 *
 * \param y The luminance
 * \param u The chrominance
 * \param v The chrominance
 * \param r The red component
 * \param g The green component
 * \param b The blue component
 *
 */
GEDEON_EXPORT void yuv444ToRgb24(const unsigned char& y, const unsigned char& u, const unsigned char& v,
				unsigned char& r, unsigned char& g, unsigned char& b);

/**
 * \brief Convert from YUV422 to RGB (YUYV)
 *
 * From http://en.wikipedia.org/wiki/YUV#Y.27UV422_to_RGB888_conversion
 *
 * \param y1 The luminance for the first pixel
 * \param u The chrominance
 * \param y2 The luminance for the second pixel
 * \param v The chrominance
 * \param r1 The red component of the first pixel
 * \param g1 The green component of the first pixel
 * \param b1 The blue component of the first pixel
 * \param r2 The red component of the second pixel
 * \param g2 The green component of the second pixel
 * \param b2 The blue component of the second pixel
 *
 */
GEDEON_EXPORT void yuv422ToRgb24(const unsigned char& y1, const unsigned char& u, const unsigned char& y2, const unsigned char& v,
				unsigned char& r1, unsigned char& g1, unsigned char& b1,
				unsigned char& r2, unsigned char& g2, unsigned char& b2);

/**
 * \brief Convert from YUV411 to RGB (YUYV)
 *
 * From http://en.wikipedia.org/wiki/YUV#Y.27UV411_to_RGB888_conversion
 *
 * \param y1 The luminance for the first pixel
 * \param u The chrominance
 * \param y2 The luminance for the second pixel
 * \param v The chrominance
 * \param y3 The luminance for the third pixel
 * \param y4 The luminance for the fourth pixel
 * \param r1 The red component of the first pixel
 * \param g1 The green component of the first pixel
 * \param b1 The blue component of the first pixel
 * \param r2 The red component of the second pixel
 * \param g2 The green component of the second pixel
 * \param b2 The blue component of the second pixel
 * \param r3 The red component of the third pixel
 * \param g3 The green component of the third pixel
 * \param b3 The blue component of the third pixel
 * \param r4 The red component of the fourth pixel
 * \param g4 The green component of the fourth pixel
 * \param b4 The blue component of the fourth pixel
 *
 */
GEDEON_EXPORT void yuv411ToRgb24(const unsigned char& y1, const unsigned char& u,
		const unsigned char& y2, const unsigned char& v,
		const unsigned char& y3, const unsigned char& y4, unsigned char& r1,
		unsigned char& g1, unsigned char& b1, unsigned char& r2,
		unsigned char& g2, unsigned char& b2, unsigned char& r3,
		unsigned char& g3, unsigned char& b3, unsigned char& r4,
		unsigned char& g4, unsigned char& b4);


/**
 * \brief Convert an RGB color into grayscale
 *
 * From http://en.wikipedia.org/wiki/Grayscale
 *
 * \param r the red channel
 * \param g the green channel
 * \param b the blue channel
 * \return the grayscale value
 *
 */
GEDEON_EXPORT inline unsigned char rgbToGrayscale(const unsigned char& r, const unsigned char& g, const unsigned char& b){
	return static_cast<unsigned char>(r*0.299f + g*0.587 + b*0.114);
}

/**
 * \brief Convert an RGB color into grayscale
 *
 * From http://en.wikipedia.org/wiki/Grayscale
 *
 * \param rgb an array with the three channels
 * \return the grayscale value
 *
 */
GEDEON_EXPORT inline unsigned char rgbToGrayscale(const unsigned char *rgb){
	return static_cast<unsigned char>(rgb[0]*0.299f + rgb[1]*0.587 + rgb[2]*0.114);
}

}

}

#endif
