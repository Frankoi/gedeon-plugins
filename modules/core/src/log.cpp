/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * log.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * log.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/log.hpp"

#include "gedeon/core/time.hpp"

#include <iostream>

namespace gedeon {



namespace Log{
	
	static LogEmitter log_singleton;


	 LogEmitter &getInstance(void){
		return log_singleton;
	}

	 LogEmitter &add(void){
		return log_singleton;
	}
}
/*
 * Logging data
 *
 */

LogData::LogData(const std::string& evname, const std::string& origin,
		const std::string& msg) :
		EventData(evname, 0), func_origin(origin), msg_description(msg) {
	timestamp = getTimeStamp();
}

LogData::~LogData(void) {
}

/*
 * Default logger code
 */

Logger::Logger(void) :
		level(GEDEON_COMPLETE) {
	this->connect();
}

Logger::~Logger(void) {
	this->disconnect();
}

void Logger::connect(void){
	Log::getInstance().connect("info_log",
			boost::bind(&Logger::info, this, _1), this->c_info);
	Log::getInstance().connect("debug_log",
			boost::bind(&Logger::debug, this, _1), this->c_debug);
	Log::getInstance().connect("warning_log",
			boost::bind(&Logger::warning, this, _1), this->c_warning);
	Log::getInstance().connect("error_log",
			boost::bind(&Logger::error, this, _1), this->c_error);
}

void Logger::disconnect(void){
	this->c_info.disconnect();
	this->c_debug.disconnect();
	this->c_warning.disconnect();
	this->c_error.disconnect();
}

LogData* Logger::getData(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		try {
			LogData * ld = dynamic_cast<LogData*>(evptr.get());
			return ld;
		} catch (std::bad_cast e) {
			return 0;
		}
	}
	return 0;
}

void Logger::warning(EventDataWPtr e) {
	if (this->level == GEDEON_COMPLETE || this->level == GEDEON_COMPLETE_WITH_DEBUG) {
		LogData * ld = getData(e);
		std::string s = "WARNING ";
		if (false == ld->func_origin.empty()) {
			s.append("[" + ld->func_origin + "]: " + ld->msg_description);
		} else {
			s.append(": " + ld->msg_description);
		}
		std::cout << s << std::endl; // Nothing to do
	}
}

void Logger::error(EventDataWPtr e) {
	if (this->level == GEDEON_MINIMAL || this->level == GEDEON_MODERATE
			|| this->level == GEDEON_COMPLETE || this->level == GEDEON_COMPLETE_WITH_DEBUG) {
		LogData * ld = getData(e);
		std::string s = "Error ";
		if (false == ld->func_origin.empty()) {
			s.append("[" + ld->func_origin + "]: " + ld->msg_description);
		} else {
			s.append(": " + ld->msg_description);
		}
		std::cout << s << std::endl;
	}
}

void Logger::debug(EventDataWPtr e) {
	if (this->level == GEDEON_DEBUG_ONLY || this->level == GEDEON_COMPLETE_WITH_DEBUG) {
		LogData * ld = getData(e);
		std::string s = "Error ";
		if (false == ld->func_origin.empty()) {
			s.append("[" + ld->func_origin + "]: " + ld->msg_description);
		} else {
			s.append(": " + ld->msg_description);
		}
		std::cout << s << std::endl;
	}
}

void Logger::info(EventDataWPtr e) {
	if (this->level == GEDEON_MODERATE || this->level == GEDEON_COMPLETE
			|| this->level == GEDEON_COMPLETE_WITH_DEBUG) {
		LogData * ld = getData(e);
		std::cout << "INFO: " << ld->msg_description << std::endl;
	}
}

enum LogLevel Logger::getLevel(void) const {
	return (this->level);
}

void Logger::setLevel(const enum LogLevel & level) {
	this->level = level;
}

/*
 * Log manage code
 */

void LogEmitter::warning(const std::string& functionname, const std::string& message) {
	EventDataPtr lg_ptr(new LogData("warning_log", functionname, message));
	emitEvent(lg_ptr);
}

void LogEmitter::warning(const std::string& message) {
	warning("", message);
}

void LogEmitter::error(const std::string& functionname, const std::string& message) {
	EventDataPtr lg_ptr(new LogData("error_log", functionname, message));
	emitEvent(lg_ptr);
}

void LogEmitter::error(const std::string& message) {
	error("", message);
}

void LogEmitter::debug(const std::string& functionname, const std::string& message) {
	EventDataPtr lg_ptr(new LogData("debug_log", functionname, message));
	emitEvent(lg_ptr);
}

void LogEmitter::debug(const std::string& message) {
	debug("", message);
}

void LogEmitter::info(const std::string& message) {
	EventDataPtr lg_ptr(new LogData("info_log", "", message));
	emitEvent(lg_ptr);
}

}
