/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * event.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * event.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/event.hpp"

namespace gedeon {

EventReceiver::~EventReceiver(void) {
	std::list<boost::signals2::connection>::iterator it =
			this->list_of_connections.begin();
	while (it != this->list_of_connections.end()) {
		(*it).disconnect();
	}
	this->list_of_connections.clear();
}

bool EventSender::connect(const std::string& eventname,
		const event_data_sender::slot_type & fn,
		boost::signals2::connection& c) {

	if (this->event_data_map.find(eventname) == this->event_data_map.end()) {
		return false;
	}
	c = this->event_data_map[eventname]->connect(fn);
	return true;
}

bool EventSender::addEventName(const std::string& eventname) {
	if (0 >= this->event_data_map.count(eventname)) {
		this->event_data_map[eventname] = new event_data_sender;
		return true;
	}
	return false;
}

bool EventSender::removeEventName(const std::string& eventname) {
	std::map<std::string, event_data_sender*>::iterator it = this->event_data_map.find(eventname);
	if(it != this->event_data_map.end()){
		this->event_data_map.erase(it);
		return true;
	}
	return false;
}

void EventSender::clear(void) {
	this->event_data_map.clear();
}

bool EventSender::emitEvent(EventDataPtr data) {
	if (0 < this->event_data_map.count(data->name)) {
		(*(this->event_data_map[data->name]))(EventDataWPtr(data));
		return true;
	}
	return false;
}

}

