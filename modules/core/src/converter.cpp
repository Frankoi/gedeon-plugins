/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * converter.cpp created in 06 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * converter.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#include "gedeon/core/converter.hpp"
#include "gedeon/core/maths.hpp"

namespace gedeon {

namespace Converter {

void yuv444ToRgb24(const unsigned char& y, const unsigned char& u,
		const unsigned char& v, unsigned char& r, unsigned char& g,
		unsigned char& b) {
	int C = int(y) - 16;
	int D = int(u) - 128;
	int E = int(v) - 128;
	r = static_cast<unsigned char>(Maths::clamp<int>(
			(298 * C + 409 * E + 128) >> 8, 0, 255));
	g = static_cast<unsigned char>(Maths::clamp<int>(
			(298 * C - 100 * D - 208 * E + 128) >> 8, 0, 255));
	b = static_cast<unsigned char>(Maths::clamp<int>(
			(298 * C + 516 * D + 128) >> 8, 0, 255));
}

void yuv422ToRgb24(const unsigned char& y1, const unsigned char& u,
		const unsigned char& y2, const unsigned char& v, unsigned char& r1,
		unsigned char& g1, unsigned char& b1, unsigned char& r2,
		unsigned char& g2, unsigned char& b2) {

	yuv444ToRgb24(y1, u, v, r1, g1, b1);
	yuv444ToRgb24(y2, u, v, r2, g2, b2);
}

void yuv411ToRgb24(const unsigned char& y1, const unsigned char& u,
		const unsigned char& y2, const unsigned char& v,
		const unsigned char& y3, const unsigned char& y4, unsigned char& r1,
		unsigned char& g1, unsigned char& b1, unsigned char& r2,
		unsigned char& g2, unsigned char& b2, unsigned char& r3,
		unsigned char& g3, unsigned char& b3, unsigned char& r4,
		unsigned char& g4, unsigned char& b4) {

	yuv444ToRgb24(y1, u, v, r1, g1, b1);
	yuv444ToRgb24(y2, u, v, r2, g2, b2);
	yuv444ToRgb24(y3, u, v, r3, g3, b3);
	yuv444ToRgb24(y4, u, v, r4, g4, b4);
}

}

}
