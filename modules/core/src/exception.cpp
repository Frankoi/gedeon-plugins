/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * exception.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * exception.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
\***************************************************************************/

#include "gedeon/core/exception.hpp"

namespace gedeon{

	Exception::Exception(const std::string& funcname, const std::string& err)
	{
		if(funcname=="" && err=="")
			str = "Error : No message";
		if(funcname!="" && err!="")
			str = "(" + funcname + ") Error : " + err;
		if(funcname=="" && err!="")
			str = "Error : " + err;
		if(funcname!="" && err=="")
			str = "(" + funcname + ") Error : No message";
	}

	Exception::Exception(const Exception & e) : std::exception()
	{
		str = e.errorString();
	}


	Exception::Exception(const std::exception & e) : std::exception(e)
	{
		str = e.what();
	}

}

