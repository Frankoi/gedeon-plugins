/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * depthimage.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * depthimage.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/datatypes/depthimage.hpp"

namespace gedeon {


	DepthImage::DepthImage(void) :
		 size(0, 0), max_(0), timestamp(0) {
		this->data.reset();
	}

	DepthImage::DepthImage(const Size &s, const float* d) :
			size(s), max_(0) {

		assert(s.width*s.height > 0);

		allocateMemory();
		if (this->data != 0) {
			setData(d);
		}
	}

	DepthImage::DepthImage(const DepthImage& img){
		this->size = img.getSize();
		this->max_ = img.getMax();
		allocateMemory();
		if (this->data != 0) {
			setData(img.getData().get());
		}
	}

	DepthImage::~DepthImage(void) {
		freeMemory();
	}

	Size DepthImage::getSize(void) const {
		return this->size;
	}


	void DepthImage::setSize(const Size &s) {

		assert(s.width*s.height > 0);
		if (this->size != s) {
			boost::mutex::scoped_lock lock(mutex);
			this->size = s;
			freeMemory();
			allocateMemory();
		}
	}

	bool DepthImage::isSet(void) const{
		return (this->size.width > 0 && this->size.height > 0);
	}

	void DepthImage::setData(const float *d) {

		assert(size.width*size.height > 0);

		if (d != 0 && this->data.get() != 0) {
			boost::mutex::scoped_lock lock(mutex);
			memcpy(this->data.get(), d,
					this->size.width * this->size.height * sizeof(float));
		}
	}

	DepthImageDataArr DepthImage::getData(void) const {
		boost::mutex::scoped_lock lock(mutex);
		return this->data;
	}

	float DepthImage::getMax(void) const {
		return this->max_;
	}

	void DepthImage::setMax(const float& v) {
		this->max_ = v;
	}

	void DepthImage::allocateMemory() {
		this->data.reset(
				new float[this->size.width * this->size.height]);
		memset(this->data.get(),0,this->size.width * this->size.height * sizeof(float));
	}

	void DepthImage::freeMemory() {
		this->data.reset();
	}

	void DepthImage::release(void) {
		this->data.reset();
		this->size.width = 0;
		this->size.height = 0;
	}

	DepthImage& DepthImage::operator=(const DepthImage& depthimage){
		if (this == &depthimage) return *this;
		setSize(depthimage.getSize());
		setData(depthimage.getData().get());
		return *this;
	}

}
