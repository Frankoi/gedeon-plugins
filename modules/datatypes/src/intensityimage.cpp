/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * intensityimage.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * intensityimage.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/datatypes/intensityimage.hpp"

#include <cstring>

namespace gedeon {

IntensityImage::IntensityImage(void) :
		size(0, 0), timestamp(0) {
	this->data.reset();
}

IntensityImage::IntensityImage(const Size &s_, const unsigned char* d) :
		size(s_) {

	this->data.reset();

	allocateMemory();

	if (this->data != 0) {
		setData(d);
	}
}

IntensityImage::IntensityImage(const IntensityImage& img){
	this->size = img.getSize();
	allocateMemory();
	if (this->data != 0) {
		setData(img.getData().get());
	}
}

IntensityImage::~IntensityImage(void) {
}

Size IntensityImage::getSize(void) const {
	return this->size;
}

void IntensityImage::setSize(const Size &s) {

	assert(s.width*s.height > 0);

	if (this->size != s) {
		boost::mutex::scoped_lock lock(mutex);
		this->size = s;
		freeMemory();
		allocateMemory();
	}
}

bool IntensityImage::isSet(void) const{
	return (this->size.width > 0 && this->size.height > 0);
}

void IntensityImage::setData(const unsigned char *d) {

	assert(size.width*size.height > 0);

	if (0 != d && this->data) {
		boost::mutex::scoped_lock lock(mutex);
		memcpy(this->data.get(), d,
				this->size.width * this->size.height
						* sizeof(unsigned char));
	}
}

IntensityImageDataArr IntensityImage::getData(void) const{
	boost::mutex::scoped_lock lock(mutex);
	return this->data;

}

void IntensityImage::allocateMemory() {
	this->data.reset(
			new unsigned char[this->size.width * this->size.height]);
	memset(this->data.get(),0,this->size.width * this->size.height);
}

void IntensityImage::freeMemory() {
	this->data.reset();
}


void IntensityImage::release(void) {
	this->data.reset();
	this->size.width = 0;
	this->size.height = 0;
}


IntensityImage& IntensityImage::operator=(const IntensityImage& intensityimage){
	if (this == &intensityimage) return *this;
	setSize(intensityimage.getSize());
	setData(intensityimage.getData().get());
	return *this;
}

}

