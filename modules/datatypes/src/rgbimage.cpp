/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * rgbimage.cpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * rgbimage.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#include "gedeon/datatypes/rgbimage.hpp"

#include <cstring>

namespace gedeon {

RGBImage::RGBImage(void) :
		size(0, 0), timestamp(0) {
	this->data.reset();
}

RGBImage::RGBImage(const Size &s_, const unsigned char* d) :
		size(s_) {

	this->data.reset();

	allocateMemory();

	if (this->data != 0) {
		setData(d);
	}
}

RGBImage::RGBImage(const RGBImage& img){
	this->size = img.getSize();
	allocateMemory();
	if (this->data != 0) {
		setData(img.getData().get());
	}
}

RGBImage::~RGBImage(void) {
}

Size RGBImage::getSize(void) const {
	return this->size;
}

void RGBImage::setSize(const Size &s) {

	assert(s.width*s.height > 0);

	if (this->size != s) {
		boost::mutex::scoped_lock lock(mutex);
		this->size = s;
		freeMemory();
		allocateMemory();
	}
}

bool RGBImage::isSet(void) const{
	return (this->size.width > 0 && this->size.height > 0);
}

void RGBImage::setData(const unsigned char *d) {

	assert(size.width*size.height > 0);

	if (0 != d && this->data) {
		boost::mutex::scoped_lock lock(mutex);
		memcpy(this->data.get(), d,
				3 * this->size.width * this->size.height
						* sizeof(unsigned char));
	}
}

RGBImageDataArr RGBImage::getData(void) const{
	boost::mutex::scoped_lock lock(mutex);
	return this->data;

}

void RGBImage::allocateMemory() {
	this->data.reset(
			new unsigned char[3 * this->size.width * this->size.height]);
	memset(this->data.get(),0,this->size.width * this->size.height * 3);
}

void RGBImage::release(void) {
	this->data.reset();
	this->size.width = 0;
	this->size.height = 0;
}

void RGBImage::freeMemory() {
	this->data.reset();

}

RGBImage& RGBImage::operator=(const RGBImage &rgbimage){
	if (this == &rgbimage) return *this;
	setSize(rgbimage.getSize());
	setData(rgbimage.getData().get());
	return *this;
}

}

