/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * intensityimage.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * intensityimage.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/


#ifndef GEDEON_DATATYPES__INTENSITY_IMAGE_HPP__
#define GEDEON_DATATYPES__INTENSITY_IMAGE_HPP__

#include "gedeon/core/config.hpp"

#include <gedeon/datatypes/size.hpp>
#include <boost/shared_array.hpp>
#include <boost/thread/mutex.hpp>

namespace gedeon{

typedef boost::shared_array<unsigned char> IntensityImageDataArr;

/**
 * \brief Class for managing an intensity image
 *
 * \author Francois de Sorbier
 */
class GEDEON_EXPORT IntensityImage {

public:

	/**
	 * \brief Default constructor
	 */
	IntensityImage(void);

	/**
	 * \brief Constructor
	 *
	 * \param[in] s The size of the image
	 * \param[in] data The optional data we want to fill the image with
	 */
	IntensityImage(const Size& s, const unsigned char* data = 0);

	/**
	 * \brief Copy constructor
	 *
	 * \param[in] img the image to copy
	 */
	IntensityImage(const IntensityImage& img);

	/**
	 * \brief Destructor
	 */
	~IntensityImage(void);

	/**
	 * \brief Return the size of the image
	 *
	 * \return the size
	 */
	Size getSize(void) const;

	/**
	 * \brief Set the image with a new size
	 *
	 * Data inside of the image are removed
	 *
	 * \param[in] s The new size of the image
	 */
	void setSize(const Size& s);

	/**
	 * \brief Return if the image contains data or not
	 *
	 * \return true is there are data, otherwise false
	 */
	bool isSet(void) const;

	/**
	 * \brief Set data in the image
	 *
	 * \param[in] d data to copy in the image
	 */
	void setData(const unsigned char *d);

	/**
	 * \brief Return a smart pointer on the array of data
	 *
	 * \return the smart pointer on data
	 */
	IntensityImageDataArr getData(void) const;

	/**
	 * \brief Overloading of the assignment operator
	 *
	 * \param[in] intensityimage The input IntensityImage to copy
	 *
	 * \return The result of the copy
	 */
	IntensityImage& operator=(const IntensityImage &intensityimage);

	/**
	 * \brief Release the image
	 *
	 * data are released and size set to 0
	 */
	void release(void);

	/**
	 * \brief Get the time of capture or arrival of the data
	 *
	 * return the timestamp
	 */
	long long unsigned int getTimeStamp(void) const{
		return this->timestamp;
	}

	/**
	 * \brief Set the time of capture or arrival of the data
	 *
	 * \param[in] ts the timestamp
	 */
	void setTimeStamp(const long long unsigned int& ts) {
		this->timestamp = ts;
	}

protected:

	void allocateMemory();

	void freeMemory();


protected:
	mutable boost::mutex mutex;
	IntensityImageDataArr data;
	Size size;
	long long unsigned int timestamp;

};

}

#endif
