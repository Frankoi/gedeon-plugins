/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * pointcloud.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * pointcloud.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/


#ifndef GEDEON_DATATYPES__POINT_CLOUD_HPP__
#define GEDEON_DATATYPES__POINT_CLOUD_HPP__

#include "gedeon/core/config.hpp"

#include <gedeon/datatypes/size.hpp>
#include <boost/shared_array.hpp>
#include <boost/thread/mutex.hpp>

namespace gedeon{

typedef boost::shared_array<float> PointCloudDataArr;
typedef boost::shared_array<bool> PointCloudValidityDataArr;

/**
 * \brief Class for managing a point cloud
 *
 * \author Francois de Sorbier
 */
class GEDEON_EXPORT PointCloud {

public:

	/**
	 * \brief Default constructor
	 */
	PointCloud(void);

	/**
	 * \brief Constructor
	 *
	 * The validity is initialized to true for each pixel if no data are given
	 *
	 * \param[in] s The size of the point cloud
	 * \param[in] data The optional data we want to fill the point cloud with
	 */
	PointCloud(const Size& s, const float* data = 0);

	/**
	 * \brief Copy constructor
	 *
	 * \param[in] pc the point cloud to copy
	 */
	PointCloud(const PointCloud& pc);

	/**
	 * \brief Destructor
	 */
	~PointCloud(void);

	/**
	 * \brief Set the size of the point cloud
	 *
	 * Data inside of the point cloud are removed
	 *
	 * \param[in] s The new size of the point cloud
	 */
	void setSize(const Size& s);

	/**
	 * \brief Return the size of the point cloud
	 *
	 * \return the size of the point cloud
	 */
	Size getSize(void) const;

	/**
	 * \brief Return a smart pointer on the data of the point cloud
	 *
	 * \return a smart pointer
	 */
	PointCloudDataArr getData(void) const;

	/**
	 * \brief Set data in the point cloud
	 *
	 * \param[in] d data to copy in the point cloud
	 */
	void setData(const float * d);

	/**
	 * \brief Overloading of the assignment operator
	 *
	 * \param[in] pointcloud The input PointCloud to copy
	 *
	 * \return The result of the copy
	 */
	PointCloud& operator=(const PointCloud &pointcloud);

private:

	mutable boost::mutex mutex;
	PointCloudDataArr data;
	Size size;

};

}

#endif
