/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * converter.hpp created in 10 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * converter.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_DATATYPES__CONVERTER_HPP__
#define GEDEON_DATATYPES__CONVERTER_HPP__

#include "gedeon/datatypes/rgbimage.hpp"
#include "gedeon/datatypes/intensityimage.hpp"
#include "gedeon/datatypes/depthimage.hpp"
#include "gedeon/core/converter.hpp"

#include <opencv2/opencv.hpp>

namespace gedeon {

namespace Converter {

/**
 * \brief Convert a rgb image into an intensity image
 *
 * \param iimg the output intensity image
 * \param rgbimg the input rgb image
 * \return if conversion was fine or not
 */
inline bool convertImage(IntensityImage& iimg, const RGBImage& rgbimg) {

	iimg.setSize(rgbimg.getSize());
	const unsigned int size = iimg.getSize().width*iimg.getSize().height;
	unsigned char *tmp_buffer = new unsigned char[size];
	unsigned char* ptr_rgbimg = rgbimg.getData().get();
	for(unsigned int i = 0; i < size; ++i){
		tmp_buffer[i] = rgbToGrayscale(ptr_rgbimg + 3*i);
	}
	iimg.setData(tmp_buffer);
	delete tmp_buffer;
	return true;
}

/**
 * \brief Convert a rgb image into an intensity image
 *
 * \param rgbimg the output rgb image
 * \param iimg the input intensity image
 * \return if conversion was fine or not
 */
inline bool convertImage(RGBImage& rgbimg, const IntensityImage& iimg) {

	rgbimg.setSize(iimg.getSize());
	const unsigned int size = rgbimg.getSize().width*rgbimg.getSize().height;
	unsigned char *tmp_buffer = new unsigned char[size*3];
	unsigned char* ptr_iimg = iimg.getData().get();
	for(unsigned int i = 0; i < size; ++i){
		tmp_buffer[i*3] = tmp_buffer[i*3 + 1] = tmp_buffer[i*3 + 2] = ptr_iimg[i];
	}
	rgbimg.setData(tmp_buffer);
	delete tmp_buffer;
	return true;
}

}

}

#endif
