/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * rgbdimage.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * rgbdimage.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/


#ifndef GEDEON_DATATYPES__RGBD_IMAGE_HPP__
#define GEDEON_DATATYPES__RGBD_IMAGE_HPP__

#include "gedeon/core/config.hpp"

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

#include "gedeon/datatypes/rgbimage.hpp"
#include "gedeon/datatypes/depthimage.hpp"
#include "gedeon/datatypes/intensityimage.hpp"

namespace gedeon{

/**
 * \brief Structure that hold an RGBD image made of an RGB image and a depth image
 *
 * \author Francois de Sorbier
 */
class RGBDImage {
public:
	RGBImage color;

	DepthImage depth;

};

typedef boost::shared_ptr<RGBDImage> RGBDImagePtr;
typedef boost::weak_ptr<RGBDImage> RGBDImageWPtr;



/**
 * \brief Structure that hold an RGBDI image made of an RGB image, a depth image and an intensity image.
 *
 * \author Francois de Sorbier
 */
class RGBDIImage {
public:
	RGBImage color;

	IntensityImage intensity;

	DepthImage depth;

};

typedef boost::shared_ptr<RGBDIImage> RGBDIImagePtr;
typedef boost::weak_ptr<RGBDIImage> RGBDIImageWPtr;

/**
 * \brief Structure that hold an RGBI image made of an RGB image and an intensity image
 *
 * \author Francois de Sorbier
 */
struct RGBIImage {

	RGBImage color;

	IntensityImage intensity;

};

typedef boost::shared_ptr<RGBIImage> RGBIImagePtr;
typedef boost::weak_ptr<RGBIImage> RGBIImageWPtr;

/**
 * \brief Structure that hold an DI image made of an depth image and an intensity image
 *
 * \author Francois de Sorbier
 */
struct  DIImage {

	DepthImage depth;

	IntensityImage intensity;

};

typedef boost::shared_ptr<DIImage> DIImagePtr;
typedef boost::weak_ptr<DIImage> DIImageWPtr;


}

#endif
