/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * size.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * size.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_DATATYPES__SIZE_HPP__
#define GEDEON_DATATYPES__SIZE_HPP__


#include "gedeon/core/config.hpp"

#include <iostream>
#include <cassert>

namespace gedeon{

/**
 * \brief Structure for defining a 2D resolution
 *
 * \author Francois de Sorbier
 */
struct GEDEON_EXPORT Size{

	/**
	 * \brief Default constructor
	 */
	Size(void);

	/**
	 * \brief Constructor
	 * \param[in] w Horizontal size
	 * \param[in] h Vertical size
	 */
	Size(const unsigned int& w, const unsigned int& h);

	/**
	 * \brief Copy constructor
	 */
	Size(const Size& s);

	/**
	 * \brief Destructor
	 */
	~Size(void);


	/**
	 * \brief Return the horizontal size
	 * \return the width
	 */
	unsigned int getWidth(void)const;

	/**
	 * \brief Return the vertical size
	 * \return the height
	 */
	unsigned int getHeight(void)const;

	/**
	 * \brief Redefine the + operator
	 * \param[in] s The size we want to add with
	 * \return The updated size
	 */
	Size operator+(const Size& s);

	/**
	 * \brief Redefine the += operator
	 * \param[in] s The size we want to add with
	 * \return The updated size
	 */
	Size& operator+=(const Size& s);

	/**
	 * \brief Redefine the - operator
	 * \param[in] s The size we want to subtract with
	 * \return The updated size
	 */
	Size operator-(const Size& s);

	/**
	 * \brief Redefine the -= operator
	 * \param[in] s The size we want to subtract with
	 * \return The updated size
	 */
	Size& operator-=(const Size& s);

	/**
	 * \brief Redefine the * operator
	 * \param[in] v The value we want to multiply with
	 * \return The updated size
	 */
	Size operator*(const unsigned int& v);

	/**
	 * \brief Redefine the *= operator
	 * \param[in] v The value we want to multiply with
	 * \return The updated size
	 */
	Size& operator*=(const unsigned int& v);

	/**
	 * \brief Redefine the / operator
	 * \param[in] v The value we want to divide with
	 * \return The updated size
	 */
	Size operator/(const unsigned int& v);

	/**
	 * \brief Redefine the /= operator
	 * \param[in] v The value we want to divide with
	 * \return The updated size
	 */
	Size& operator/=(const unsigned int& v);

	/**
	 * \brief Redefine the = operator
	 * \param[in] s The size we want to copy
	 * \return The updated size
	 */
	Size& operator=(const Size& s);

	/**
	 * \brief Redefine the == operator
	 * \param[in] s The size we want to compare with
	 * \return The updated size
	 */
	bool operator==(const Size& s) const;

	/**
	 * \brief Redefine the != operator
	 * \param[in] s The size we want to compare with
	 * \return The updated size
	 */
	bool operator!=(const Size& s) const;

	/**
	 * \var unsigend int width
	 * \brief The horizontal size
	 */
	unsigned int width;

	/**
	 * \var unsigned int height
	 * \brief The vertical size
	 */
	unsigned int height;

};

inline std::ostream &operator<<(std::ostream &out, const Size& s){
        out<<"["<< s.width <<","<< s.height <<"]";
        return out;
}

}

#endif
