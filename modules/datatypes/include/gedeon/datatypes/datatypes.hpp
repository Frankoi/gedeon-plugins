/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * core.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * core.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/

#ifndef GEDEON_DATATYPES__DATATYPES_HPP__
#define GEDEON_DATATYPES__DATATYPES_HPP__

#include "gedeon/core/config.hpp"

#include "gedeon/datatypes/size.hpp"
#include "gedeon/datatypes/depthimage.hpp"
#include "gedeon/datatypes/rgbimage.hpp"
#include "gedeon/datatypes/intensityimage.hpp"
#include "gedeon/datatypes/pointcloud.hpp"
#include "gedeon/datatypes/rgbdimage.hpp"
#include "gedeon/datatypes/converter.hpp"

#endif
