/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * depthimage.hpp created in 05 2013.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * depthimage.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The HVRL Engine Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ***************************************************************************/


#ifndef GEDEON_DATATYPES__DEPTH_IMAGE_HPP__
#define GEDEON_DATATYPES__DEPTH_IMAGE_HPP__

#include "gedeon/core/config.hpp"

#include <gedeon/datatypes/size.hpp>
#include <boost/shared_array.hpp>
#include <boost/thread/mutex.hpp>

namespace gedeon{

typedef boost::shared_array<float> DepthImageDataArr;

/**
 * \brief Class for managing a depth image
 *
 * \author Francois de Sorbier
 */
class GEDEON_EXPORT DepthImage {

public:

	/**
	 * \brief Default constructor
	 */
	DepthImage(void);

	/**
	 * \brief Constructor
	 *
	 * \param[in] s The size of the image
	 * \param[in] data The optional data we want to fill the image with
	 */
	DepthImage(const Size &s, const float* data = 0);

	/**
	 * \brief Copy constructor
	 *
	 * \param[in] img the depthimage to copy
	 */
	DepthImage(const DepthImage& img);

	/**
	 * \brief Destructor
	 */
	~DepthImage(void);

	/**
	 * \brief Return the size of the image
	 *
	 * \return the size
	 */
	Size getSize(void) const;

	/**
	 * \brief Return if the image contains data or not
	 *
	 * \return true is there are data, otherwise false
	 */
	bool isSet(void) const;

	/**
	 * \brief Set the image with a new size
	 *
	 * Data inside of the image are removed
	 *
	 * \param[in] s The new size of the image
	 */
	void setSize(const Size &s);

	/**
	 * \brief Set data in the image
	 *
	 * \param[in] d data to copy in the image
	 */
	void setData(const float *d);

	/**
	 * \brief Return a smart pointer on the array of data
	 *
	 * \return the smart pointer on data
	 */
	DepthImageDataArr getData(void) const;

	/**
	 * \brief The maximum value is returned for sampling the image of the depth
	 *
	 * \return The maximum value for sampling
	 */
	float getMax(void)const;

	/**
	 * \brief The maximum value is returned for sampling the image of the depth
	 *
	 * \param[in] v The new maximum value
	 */
	void setMax(const float& v);

	/**
	 * \brief Overloading of the assignment operator
	 *
	 * \param[in] depthimage The input DepthImage to copy
	 *
	 * \return The result of the copy
	 */
	DepthImage& operator=(const DepthImage &depthimage);

	/**
	 * \brief Release the image
	 *
	 * data are released and size set to 0
	 */
	void release(void);

	/**
	 * \brief Get the time of capture or arrival of the data
	 *
	 * return the timestamp
	 */
	long long unsigned int getTimeStamp(void) const{
		return this->timestamp;
	}

	/**
	 * \brief Set the time of capture or arrival of the data
	 *
	 * \param[in] ts the timestamp
	 */
	void setTimeStamp(const long long unsigned int& ts) {
		this->timestamp = ts;
	}

protected:

	void allocateMemory(void);

	void freeMemory(void);


protected:


	mutable boost::mutex mutex;
	DepthImageDataArr data;
	Size size;
	float max_;
	long long unsigned int timestamp;

};

}

#endif
