/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * defaults.hpp created in 04 2014.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * defaults.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CAPTURE_DEVICE__DEFAULTS__HPP__
#define GEDEON_CAPTURE_DEVICE__DEFAULTS__HPP__

// Internal includes
#include "gedeon/datatypes/size.hpp"

// External includes
#include <list>

namespace gedeon {

static const float DEFAULT_FRAMERATE = 30.0f;

enum PixelFormat{
	RGB888U = 0,
	YUV411,
	YUV422,
	YUV444,
	GRAY32F,
	GRAY8U,
};

static const std::string  PixelFormatString[] = { "RGB888U", "YUV411", "YUV422", "YUV444", "GRAY32F", "GRAY8U"};

/**
 * \brief Defines the format for a specific sensor of the camera
 *
 * \author Francois de Sorbier
 */
struct Format {
	Size resolution; ///< Resolution of the sensor
	PixelFormat format; ///< Pixel format internal to the capture device
	unsigned int framerate_num; ///< Numerator part of the current framerate (Numerator/Denumerator)
	unsigned int framerate_denom; ///< Denumerator part of the current framerate (Numerator/Denumerator)
	std::list< std::pair<unsigned int ,unsigned int > > framerates; ///< List of available framerates for the sensor with this given resolution and format (Numerator,Denumerator)

	/**
	 * \brief Constructor
	 *
	 */
	Format(void) {
		this->resolution.height = 0;
		this->resolution.width = 0;
		this->format = RGB888U;
		this->framerate_num = DEFAULT_FRAMERATE;
		this->framerate_denom = 1;
	}

	/**
	 * \brief Destructor
	 *
	 */
	~Format(void){
		this->framerates.clear();
	}

	/**
	 * \brief Equality test
	 *
	 * \param a Another format to compare with
	 *
	 * \return true if format are identical, false otherwise
	 */
	bool operator==(Format const& a) {
		if (this->resolution == a.resolution && this->framerate_num == a.framerate_num
				&& this->framerate_denom == a.framerate_denom && this->format == a.format) {
			return true;
		}
		return false;
	}

	/**
	 * \brief Difference test
	 *
	 * \param a Another format to compare with
	 *
	 * \return true if format are different, false otherwise
	 */
	bool operator!=(Format const& a) {
		return !(*this == a);
	}
};

/**
 * \brief Holds the formats of a device
 *
 * \author Francois de Sorbier
 */
struct DeviceFormat {

	Format color; ///< Color sensor format
	Format depth; ///< Depth sensor format
	Format intensity; ///< Intensity sensor format

	/**
	 * \brief Equality test
	 *
	 * \param a Another DeviceFormat to compare with
	 *
	 * \return true if format are identical, false otherwise
	 */
	bool operator==(DeviceFormat const& a) {
		if (this->color == a.color
				&& this->depth == a.depth
				&& this->intensity == a.intensity) {
			return true;
		}
		return false;
	}

	/**
	 * \brief Difference test
	 *
	 * \param a Another DeviceFormat to compare with
	 *
	 * \return true if format are different, false otherwise
	 */
	bool operator!=(DeviceFormat const& a) {
		return !(*this == a);
	}

};

} //end namespace gedeon

#endif // end GEDEON_CAPTURE_DEVICE__DEFAULTS__HPP__
