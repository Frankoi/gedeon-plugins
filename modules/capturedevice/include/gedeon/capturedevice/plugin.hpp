/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * plugin.hpp created in 05 2014.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * plugin.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CAPTURE_DEVICE__PLUGIN_HPP__
#define GEDEON_CAPTURE_DEVICE__PLUGIN_HPP__

// Internal includes
#include "gedeon/core/dynamiclibraryio.hpp"
#include "gedeon/capturedevice/driver.hpp"

namespace gedeon {

/**
 * \brief Class for loading a plugin of a capture device
 *
 * \author Francois de Sorbier
 */
class Plugin {

	typedef Driver* getDriverFunction(void);
	typedef void releaseDriverFunction(void);
	typedef int getEngineVersionFunction(void);

	Driver* driver;
	getDriverFunction* getDriverf;
	releaseDriverFunction* releaseDriverf;
	getEngineVersionFunction* getVersionf;
	DynamicLibraryIO *dnl;
	int version;

public:
	/**
	 * \brief Constructor that loads a plugin
	 *
	 * An exception is thrown if the loading fails
	 * \param directory The directory where is located the plugin
	 * \param plugin_name The name of the plugin
	 * \exception Could load the dynamic library
	 */
	Plugin(const std::string& directory, const std::string& plugin_name) :
			driver(0), getDriverf(0), releaseDriverf(0), getVersionf(0), dnl(0), version(
					-1) {

		this->dnl = new DynamicLibraryIO(directory, plugin_name);

		this->getDriverf = dnl->getFunctionPointer<getDriverFunction*>(
				"getDriver");
		this->releaseDriverf = dnl->getFunctionPointer<releaseDriverFunction*>(
				"releaseDriver");
		this->getVersionf = dnl->getFunctionPointer<getEngineVersionFunction*>(
				"getVersion");

		this->driver = this->getDriverf();
		this->version = this->getVersionf();

		if (DRIVER_VERSION != this->version) {
			throw Exception("Plugin::Plugin", "Driver version is not correct");
		}

		if (0 == this->driver) {
			throw Exception("Plugin::Plugin", "Failed to get the driver");
		}
	}

	/**
	 * \brief Destructor
	 *
	 */
	~Plugin(void) {

		this->releaseDriverf();

		this->getDriverf = 0;
		this->releaseDriverf = 0;
		this->getVersionf = 0;

		this->driver = 0;

		delete this->dnl;

	}

	/**
	 * \brief return the driver associated with the plugin
	 *
	 * \return a reference on the driver
	 */
	Driver& getDriver(void) {
		return *(this->driver);
	}
};

} // end namespace gedeon

#endif // end GEDEON_CORE__PLUGIN_HPP__
