/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * property.hpp created in 03 2014.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * property.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CAPTURE_DEVICE__PROPERTY__HPP__
#define GEDEON_CAPTURE_DEVICE__PROPERTY__HPP__

// External includes
#include <string>

namespace gedeon {

typedef boost::shared_ptr<float> MatPtr;

/**
 * \brief Defines the name of a property and holds an optional description
 *
 * \author Francois de Sorbier
 */
struct Type{
	std::string name; ///< The name of the property
	std::string description; ///< The description of the property
};

/**
 * \brief Defines the content of the property like the type of data.
 *
 * \author Francois de Sorbier
 */
struct PropertyInfo{

	bool has_auto_manual_mode; ///< Defines if the property is the auto/manual mode

	bool has_on_off; ///< Defines if the property is a trigger

	bool has_fvalue; ///< Defines if the property is a floating point value
	float fmin; ///< Defines if the minimum value for the floating point value
	float fmax; ///< Defines if the maximum value for the floating point value

	bool has_ivalue; ///< Defines if the property is an integer value
	int imin; ///< Defines if the minimum value for the integer value
	int imax; ///< Defines if the maximum value for the integer value

	bool has_svalue; ///< Defines if the property is a string value

	float has_matrix;  ///< Defines if the property has a matrix

	bool changeable; ///< Defines if the property can be changed or not

	/**
	 * \brief Constructor
	 *
	 */
	PropertyInfo(void): has_auto_manual_mode(false), has_on_off(false), has_fvalue(false), fmin(0.0f), fmax(0.0f), has_ivalue(false), imin(0), imax(0), has_svalue(false), has_matrix(false), changeable(false){

	}
};

/**
 * \brief Defines the property itself
 *
 * \author Francois de Sorbier
 */
struct Property{
	Type type; ///< Property ID ( \sa PropertyType )
	PropertyInfo info; ///< Information about the property ( \sa PropertyInfo )
	bool auto_manual_mode; ///< Defines if the auto/manual status
	bool on_off; ///< Defines if the on/off status
	float fvalue; ///< Defines the floating point value
	float fstep; ///< Defines the step for changing the float value
	int ivalue; ///< Defines the integer value
	int istep; ///< Defines the step for changing the int value
	std::string svalue;  ///< Defines the string value
	MatPtr mat;  ///< A smart pointer that contains values of the matrix if not null
	Size mat_size;  ///< Size of the matrix

	Property(void){
		this->auto_manual_mode = false;
		this->on_off = false;
		this->fvalue = 0.0f;
		this->fstep = 1.0f;
		this->ivalue = 0;
		this->istep = 1;
		this->mat.reset();
		this->mat_size = Size(0,0);
	}

};

} // namespace gedeon

#endif // GEDEON_CAPTURE_DEVICE__PROPERTY__HPP__
