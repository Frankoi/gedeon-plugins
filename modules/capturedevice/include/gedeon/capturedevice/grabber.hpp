/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * grabber.hpp created in 03 2014.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * grabber.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CAPTURE_DEVICE__GRABBER__HPP__
#define GEDEON_CAPTURE_DEVICE__GRABBER__HPP__

// Internal includes
#include "gedeon/core/time.hpp"
#include "gedeon/datatypes/rgbdimage.hpp"
#include "gedeon/capturedevice/formats.hpp"
#include "gedeon/capturedevice/property.hpp"
#include "gedeon/capturedevice/auxiliarybuffer.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include <list>

namespace gedeon {

/**
 * \brief Abstract class for describing a Grabber
 *
 * \author Francois de Sorbier
 *
 */
class Grabber: public EventSender {

protected:

	std::string name;

	bool stream_paused;
	bool stream_running;
	bool stream_finished;
	float adaptative_framerate;

	RGBDIImagePtr rgbdi;

	boost::thread *pthread;
	boost::mutex mutex;
	EventDataPtr event_updated;
	EventDataPtr event_ready;
	EventDataPtr event_paused;
	EventDataPtr event_stopped;
	EventDataPtr event_playing;

	DeviceFormat device_format;
	std::list<DeviceFormat> device_format_list;

	std::map<std::string, Property> porperties_map;

	std::map<std::string, AuxiliaryBuffer> auxiliary_buffer_map;

public:

	/**
	 * \brief Default Constructor
	 *
	 */
	Grabber(void) :
			stream_paused(false), stream_running(false), stream_finished(true), adaptative_framerate(
					DEFAULT_FRAMERATE), pthread(0) {

		this->event_updated.reset(new EventData("updated"));
		this->event_ready.reset(new EventData("ready"));
		this->event_paused.reset(new EventData("paused"));
		this->event_stopped.reset(new EventData("stopped"));
		this->event_playing.reset(new EventData("playing"));

		addEventName("updated");
		addEventName("ready");
		addEventName("paused");
		addEventName("stopped");
		addEventName("playing");
	}

	/**
	 * \brief Default destructor
	 *
	 */
	~Grabber(void) {

		if (0 != this->pthread) {
			this->pthread->interrupt();
			delete this->pthread;
		}

		this->porperties_map.clear();
		this->device_format_list.clear();
		this->auxiliary_buffer_map.clear();
	}

	/**
	 * \brief Return the name of the grabber
	 *
	 * \return the name of the grabber
	 */
	virtual std::string getName(void) const {
		return this->name;
	}

	/**
	 * \brief Return the rgbdi image captured by the device
	 *
	 * \return The RGBDI image
	 */
	virtual RGBDIImagePtr getImage(void) {
		return this->rgbdi;
	}

	/**
	 * \brief Return the properties available on the capture device
	 *
	 * \param from The iterator on the first element of the properties
	 * \param to The iterator on the last element of the properties
	 *
	 * \sa getProperty
	 * \sa setProperty
	 * \sa Property
	 */
	virtual void getAvailableProperties(
			std::map<std::string, Property>::const_iterator& from,
			std::map<std::string, Property>::const_iterator& to) const {
		from = this->porperties_map.begin();
		to = this->porperties_map.end();
	}

	/**
	 * \brief Return a given property described by its name
	 *
	 * \param property The property that will be filled (the name should be set)
	 *
	 * \return true if the property was found, otherwise false
	 *
	 * \sa Property
	 * \sa getAvailableProperties
	 * \sa setProperty
	 */
	virtual bool getProperty(Property& property) {
		if (this->porperties_map.count(property.type.name)>0){
			property = this->porperties_map[property.type.name];
			return true;
		}
		return false;
	}

	/**
	 * \brief Set a given property described by its name
	 *
	 * \param property The property that will be set with its content
	 *
	 * \return true if the property was found and correctly set, otherwise false
	 *
	 * \sa Property
	 * \sa getAvailableProperties
	 * \sa getProperty
	 */
	virtual bool setProperty(const Property& property) = 0;

	/**
	 * \brief Return the formats available on the capture device
	 *
	 * \param from The iterator on the first element of the formats
	 * \param to The iterator on the last element of the formats
	 *
	 * \sa getCurrentFormat
	 * \sa setFormat
	 * \sa DeviceFormat
	 */
	virtual void getAvailableFormats(
			std::list<DeviceFormat>::const_iterator& from,
			std::list<DeviceFormat>::const_iterator& to) const {
		from = this->device_format_list.begin();
		to = this->device_format_list.end();
	}

	/**
	 * \brief Set the given format
	 *
	 * \param df The new format that will be set
	 *
	 * \return true if the format is correct and available, otherwise false
	 *
	 * \sa getCurrentFormat
	 * \sa getAvailableFormats
	 * \sa DeviceFormat
	 */
	virtual bool setFormat(const DeviceFormat& df) = 0;

	/**
	 * \brief Return the current format
	 *
	 * \return The current format
	 *
	 * \sa setFormat
	 * \sa getAvailableFormats
	 * \sa DeviceFormat
	 */
	virtual DeviceFormat getCurrentFormat(void) {
		return this->device_format;
	}

	/**
	 * \brief Return the auxiliary buffers available for the device
	 *
	 * \param from The iterator on the first element of the auxiliary buffers
	 * \param to The iterator on the last element of the auxiliary buffers
	 *
	 * \sa getAuxialiaryBuffers
	 */
	virtual void getAvailableAuxiliaryBuffers(
			std::map<std::string, AuxiliaryBuffer>::const_iterator& from,
			std::map<std::string, AuxiliaryBuffer>::const_iterator& to) const {
		from = this->auxiliary_buffer_map.begin();
		to = this->auxiliary_buffer_map.end();
	}


	/**
	 * \brief Return the auxiliary buffers available for the device
	 *
	 * \param from The iterator on the first element of the auxiliary buffers
	 * \param to The iterator on the last element of the auxiliary buffers
	 *
	 * \sa getAvailableAuxialiaryBuffers
	 */
	virtual bool getAuxiliaryBuffers(AuxiliaryBuffer& buffer) {

		if (this->auxiliary_buffer_map.count(buffer.type.name)>0){
			buffer = this->auxiliary_buffer_map[buffer.type.name];
			return true;
		}
		return false;
	}

	/**
	 * \brief Start the capture from the device
	 *
	 * \sa stopCapture
	 * \sa pauseCapture
	 */
	virtual void startCapture(void) = 0;

	/**
	 * \brief Stop the capture from the device
	 * Stop also the recording if available
	 *
	 * \sa startCapture
	 * \sa pauseCapture
	 */
	virtual void stopCapture(void) {
		if (true == this->stream_running) {
			this->stream_running = false;

			while (false == this->stream_finished) {
				millisecSleep(40);
			}
			if (0 != this->pthread) {
				this->pthread->interrupt();
				delete this->pthread;
				this->pthread = 0;
			}
			emitEvent(this->event_stopped);
		}
	}

	/**
	 * \brief Pause the capture from the device
	 *
	 * \sa startCapture
	 * \sa stopCapture
	 */
	virtual void pauseCapture(void) {
		this->stream_paused = !this->stream_paused;
		if (true == this->stream_paused) {
			emitEvent(this->event_paused);
		}
	}

};
// end class Grabber

	typedef boost::shared_ptr<Grabber> GrabberPtr;

} // end namespace gedeon

#endif // GEDEON_CAPTURE_DEVICE__GRABBER__HPP__
