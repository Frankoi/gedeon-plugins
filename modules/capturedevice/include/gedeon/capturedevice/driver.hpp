/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * driver.hpp created in 03 2014.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * driver.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CAPTURE_DEVICE__DRIVER__HPP__
#define GEDEON_CAPTURE_DEVICE__DRIVER__HPP__

// Internal includes
#include "gedeon/capturedevice/grabber.hpp"

// External includes

namespace gedeon {

static const int DRIVER_VERSION = 101;

/**
 * \brief Abstract class for describing a Driver
 *
 * \author Francois de Sorbier
 *
 */
class Driver {

public:

	/**
	 * \brief Default destructor
	 *
	 */
	~Driver(void) {
		this->list_grabbers.clear();
	}

	/**
	 * \brief Ask to scan the network interface for finding the available cameras
	 *
	 * \return true or false if something went wrong or not
	 *
	 */
	virtual bool update(void) = 0;

	/**
	 * \brief Give the name of the driver
	 *
	 * \return the name of the driver
	 *
	 */
	virtual std::string getName(void) const {
		return this->driver_name;
	}

	/**
	 * \brief Return the list of available grabbers
	 *
	 * \return Return a list of grabber names defined by
	 *
	 */
	virtual void populate(std::list<std::string>::const_iterator& from,
			std::list<std::string>::const_iterator& to) const {
		from = this->list_grabbers.begin();
		to = this->list_grabbers.end();
	}

	/**
	 * \brief Return the number of available grabbers
	 *
	 * \return Return the number of available grabbers
	 *
	 */
	virtual unsigned int getCount(void) const {
		return this->list_grabbers.size();
	}

	/**
	 * \brief The function return a pointer on a Grabber hold by the driver
	 *
	 * 	If the driver has not been yet created, it first creates it and returns it.
	 *
	 * 	If the grabber was not find, it returns an empty grabber
	 *
	 * \param name The name of the grabber to return
	 *
	 * \sa populate
	 *
	 * \return Return a smart pointer on a Grabber
	 *
	 */
	virtual GrabberPtr getGrabber(const std::string& name) = 0;

	/**
	 * \brief The function return a pointer on a Grabber hold by the driver on a given file stream
	 *
	 * 	If this functionnality is not supported, it returns an empty grabber
	 *
	 * 	If the driver has not been yet created, it first creates it and returns it.
	 *
	 * \param name The filename of a recorder stream
	 *
	 * \return Return a smart pointer on a Grabber
	 *
	 */
	virtual GrabberPtr getGrabberFromFile(const std::string& name){
		GrabberPtr p;
		p.reset();
		return p;
	}

protected:

	std::string driver_name;
	std::list<std::string> list_grabbers;

};
// class Driver


#if defined _WIN32 || defined _MSC_VER
#define GEDEON_EXPORT_PLUGIN __declspec(dllexport)
#else
//#define GEDEON_EXPORT __attribute__ ((visibility ("default")))
#define GEDEON_EXPORT_PLUGIN
#endif

}// namespace gedeon

#endif // GEDEON_CAPTURE_DEVICE__DRIVER__HPP__
