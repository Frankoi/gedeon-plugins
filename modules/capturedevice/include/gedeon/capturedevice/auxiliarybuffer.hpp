/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * auxiliarybuffer.hpp created in 05 2014.
 * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * auxiliarybuffer.hpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#ifndef GEDEON_CAPTURE_DEVICE__AUXIALIARY_HPP__
#define GEDEON_CAPTURE_DEVICE__AUXIALIARY_HPP__

// Internal includes
#include "gedeon/datatypes/size.hpp"
#include "gedeon/capturedevice/property.hpp"

// External includes
#include <boost/shared_ptr.hpp>

namespace gedeon {

typedef boost::shared_ptr<unsigned char> DataPtr;

static const unsigned int GEDEON_8U = 0;
static const unsigned int GEDEON_8S = 1;
static const unsigned int GEDEON_16U = 2;
static const unsigned int GEDEON_16S = 3;
static const unsigned int GEDEON_32U = 4;
static const unsigned int GEDEON_32S = 5;
static const unsigned int GEDEON_32F = 6;
static const unsigned int GEDEON_64F = 7;

static const std::string  depthString[] = { "unsigned char", "char", "unsigned short int", "short int", "unsigned int", "int", "float", "double"};

struct AuxiliaryBuffer {

	Type type;     ///< The name and the description of the buffer (\sa Type )
	int depth;     ///< The type of data inside of the buffer
	int channels;  ///< the number of channels per pixels
	Size size;     ///< The resolution of the buffer
	DataPtr data;  ///< Data inside of the buffer stores as unsigned bytes

	AuxiliaryBuffer(void) :
			depth(-1), channels(-1) {
	}

	~AuxiliaryBuffer(void) {
		this->data.reset();
	}
};

} // end namespace gedeon

#endif // end GEDEON_CAPTURE_DEVICE__AUXIALIARY_HPP__
