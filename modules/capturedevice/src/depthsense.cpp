/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * depthsense.cpp created in 05 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * depthsense.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

// Internal includes
#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/converter.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/capturedevice/driver.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>
#include <map>
#include <DepthSense.hxx>

namespace gedeon {

class DepthSenseGrabber: public Grabber {

private:
	DepthSense::DepthNode depth_node;
	DepthSense::ColorNode color_node;
	DepthSense::Device device;
	DepthSense::Context context;

	bool updated_info[2];
	boost::mutex updated_mutex;

	unsigned char* color_buffer;
	float* depth_buffer;
	float* vertex_buffer;
	int* confidence_buffer;
	float* uv_buffer;
	boost::mutex buffer_mutex[2];
	unsigned int timestamp[2];

	float framerate_color;
	float framerate_depth;

public:

	DepthSenseGrabber(const DepthSense::Device& d, const DepthSense::Context& c) :
			Grabber() {

		this->device = d;
		this->context = c;

		this->updated_info[0] = false;
		this->updated_info[1] = false;
		this->depth_buffer = 0;
		this->color_buffer = 0;
		this->confidence_buffer = 0;
		this->vertex_buffer = 0;
		this->uv_buffer = 0;

		this->rgbdi.reset(new RGBDIImage);

		std::vector<DepthSense::Node> nodes = this->device.getNodes();
		std::vector<DepthSense::Node>::iterator it_nodes = nodes.begin();
		while (it_nodes != nodes.end()) {
			if (it_nodes->is<DepthSense::DepthNode>()) {
				int width = 0, height = 0;
				this->depth_node = it_nodes->as<DepthSense::DepthNode>();
				this->depth_node.newSampleReceivedEvent().connect(
						&onNewDepthSample_cb, this);
				try {
					this->context.requestControl(this->depth_node);
				} catch (DepthSense::Exception e) {
					throw Exception("DepthSenseGrabber::DepthSenseGrabber",
							e.getMessage());
				}
				this->depth_node.setEnableDepthMapFloatingPoint(true);
				this->depth_node.setEnableConfidenceMap(true);
				this->depth_node.setEnableUvMap(true);
				this->depth_node.setEnableVerticesFloatingPoint(true);
				this->depth_node.setEnableDenoising(true);
				DepthSense::DepthNode::Configuration config =
						this->depth_node.getConfiguration();
				DepthSense::FrameFormat_toResolution(config.frameFormat, &width,
						&height);
				this->rgbdi.get()->depth.setSize(Size(width, height));
				this->framerate_depth = config.framerate;
				this->depth_buffer = new float[width * height];
				this->confidence_buffer = new int[width * height];
				this->vertex_buffer = new float[width * height * 3];
				this->uv_buffer = new float[width * height * 2];
				memset(this->depth_buffer, 0, width * height * sizeof(float));
				this->device_format.depth.format = GRAY32F;
				this->device_format.depth.resolution.width = width;
				this->device_format.depth.resolution.height = height;
				this->device_format.depth.framerate_num = config.framerate;
			}
			if (it_nodes->is<DepthSense::ColorNode>()) {
				int width = 0, height = 0;
				this->color_node = it_nodes->as<DepthSense::ColorNode>();
				this->color_node.newSampleReceivedEvent().connect(
						&onNewColorSample_cb, this);
				try {
					this->context.requestControl(this->color_node);
				} catch (DepthSense::Exception e) {
					throw Exception("DepthSenseGrabber::DepthSenseGrabber",
							e.getMessage());
				}
				this->color_node.setEnableColorMap(true);
				DepthSense::ColorNode::Configuration config =
						this->color_node.getConfiguration();
				DepthSense::FrameFormat_toResolution(config.frameFormat, &width,
						&height);
				this->rgbdi.get()->color.setSize(Size(width, height));
				this->framerate_color = config.framerate;
				this->color_buffer = new unsigned char[width * height * 3];
				memset(this->color_buffer, 0, width * height * 3);
				this->device_format.color.format = RGB888U;
				this->device_format.color.resolution.width = width;
				this->device_format.color.resolution.height = height;
				this->device_format.color.framerate_num = config.framerate;
			}

			++it_nodes;
		}
		feedFormat();
		generateProperties();
		generateAuxBuffers();

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);

	}

	~DepthSenseGrabber(void) {

		this->stopCapture();
		if (true == this->depth_node.isSet()) {
			this->context.releaseControl(this->depth_node);
			this->depth_node.unset();
		}
		if (true == this->color_node.isSet()) {
			this->context.releaseControl(this->color_node);
			this->color_node.unset();
		}
		if (0 != this->color_buffer) {
			delete[] this->color_buffer;
		}
		if (0 != this->depth_buffer) {
			delete[] this->depth_buffer;
		}
		if (0 != this->confidence_buffer) {
			delete[] this->confidence_buffer;
		}
		if (0 != this->vertex_buffer) {
			delete[] this->vertex_buffer;
		}

		this->device.unset();
	}

	void startCapture(void) {

		this->stream_paused = false;
		if (false == this->stream_running) {
			if (true == this->color_node.isSet()) {
				this->context.registerNode(this->color_node);
			}
			if (true == this->depth_node.isSet()) {
				this->context.registerNode(this->depth_node);
			}
			this->stream_running = true;
			millisecSleep(100);
			emitEvent(this->event_playing);
		}

	}

	void stopCapture(void) {
		if (true == this->stream_running) {
			this->stream_running = false;
			if (true == this->color_node.isSet()) {
				this->context.unregisterNode(this->color_node);
			}
			if (true == this->depth_node.isSet()) {
				this->context.unregisterNode(this->depth_node);
			}
			millisecSleep(100);
			emitEvent(this->event_stopped);
		}
	}

	RGBDIImagePtr getImage(void) {
		unsigned char *color = this->rgbdi.get()->color.getData().get();
		{
			boost::mutex::scoped_lock l(this->buffer_mutex[1]);
			size_t buffer_size = this->rgbdi.get()->color.getSize().width
					* this->rgbdi.get()->color.getSize().height * 3;
			memcpy(color, this->color_buffer, buffer_size);
			this->rgbdi.get()->color.setTimeStamp(timestamp[1]);
		}
		float *depth = this->rgbdi.get()->depth.getData().get();
		unsigned int size_tmp = this->rgbdi.get()->depth.getSize().width * this->rgbdi.get()->depth.getSize().height;
		{
			size_t buffer_size = size_tmp * sizeof(float);
			boost::mutex::scoped_lock l(this->buffer_mutex[0]);
			memcpy(depth, this->depth_buffer, buffer_size);
			this->rgbdi.get()->depth.setTimeStamp(timestamp[0]);
		}

		if (0 != this->vertex_buffer) {
			std::string s("Vertex map");
			unsigned char * p = this->auxiliary_buffer_map[s].data.get();
			memcpy(p, this->vertex_buffer, size_tmp * 3 * sizeof(float));
		}

		if (0 != this->confidence_buffer) {
			std::string s("Confidence map");
			unsigned char * p = this->auxiliary_buffer_map[s].data.get();
			memcpy(p, this->confidence_buffer,size_tmp * sizeof(int));
		}

		if (0 != this->uv_buffer) {
			std::string s("UV map");
			unsigned char * p = this->auxiliary_buffer_map[s].data.get();
			memcpy(p, this->uv_buffer, size_tmp * 2 * sizeof(float));
		}

		return this->rgbdi;
	}

	bool setFormat(const DeviceFormat& df) {
		std::list<DeviceFormat>::const_iterator it =
				this->device_format_list.begin();
		bool found = false;
		if (df.color.resolution.width > 0) {
			while (it != this->device_format_list.end() && false == found) {
				if (it->color.resolution == df.color.resolution) {
					found = true;
				}
				++it;
			}
			if (true == found) {
				if (false == applyNewColorFormat(df)) {
					return false;
				} else {
					this->device_format.color.framerate_num =
							df.color.framerate_num;
					this->device_format.color.resolution = df.color.resolution;
				}
			}
		}

		if (df.depth.resolution.width > 0) {
			it = this->device_format_list.begin();
			found = false;
			while (it != this->device_format_list.end() && false == found) {
				if (it->depth.resolution == df.depth.resolution) {
					found = true;
				}
				++it;
			}
			if (true == found) {
				if (false == applyNewDepthFormat(df)) {
					return false;
				} else {
					this->device_format.depth.framerate_num =
							df.depth.framerate_num;
					this->device_format.depth.resolution = df.depth.resolution;
				}
			}
		}

		return true;
	}

	void onNewColorSample(DepthSense::ColorNode::NewSampleReceivedData data) {
		if (false == this->stream_paused) {
			if (0 != this->color_buffer) {
				unsigned char *tmp = this->color_buffer;
				{
					boost::mutex::scoped_lock l(this->buffer_mutex[1]);
					if (DepthSense::COMPRESSION_TYPE_MJPEG
							== data.captureConfiguration.compression) {
						for (int i = 0; i < data.colorMap.size(); ++i) {
							tmp[i * 3] = data.colorMap[i * 3 + 2];
							tmp[i * 3 + 1] = data.colorMap[i * 3 + 1];
							tmp[i * 3 + 2] = data.colorMap[i * 3];
						}

					} else {
						// YUV
						for (int i = 0; i < data.colorMap.size(); i += 4) {
							unsigned char y1 = data.colorMap[i];
							unsigned char u = data.colorMap[i + 1];
							unsigned char y2 = data.colorMap[i + 2];
							unsigned char v = data.colorMap[i + 3];

							Converter::yuv422ToRgb24(y1, u, y2, v, *tmp,
									*(tmp + 1), *(tmp + 2), *(tmp + 3),
									*(tmp + 4), *(tmp + 5));

							tmp += 6;
						}
					}
					//timestamp[1] = data.timeOfCapture;
					timestamp[1] = getTimeStamp();
				}
			}
			{
				boost::mutex::scoped_lock l(this->updated_mutex);
				this->updated_info[1] = true;
				if (this->updated_info[0] && this->updated_info[1]) {
					emitEvent(this->event_updated);
					this->updated_info[0] = false;
					this->updated_info[1] = false;
				}
			}
		}

	}

	void onNewDepthSample(DepthSense::DepthNode::NewSampleReceivedData data) {
		if (false == this->stream_paused) {
			int width, height;
			DepthSense::FrameFormat_toResolution(
					data.captureConfiguration.frameFormat, &width, &height);
			if (0 != this->depth_buffer) {
				{
					boost::mutex::scoped_lock l(this->buffer_mutex[0]);
					memcpy(this->depth_buffer, data.depthMapFloatingPoint,
							width * height * sizeof(float));
					//timestamp[0] = data.timeOfCapture;
					timestamp[0] = getTimeStamp();
				}
				if (0 != this->confidence_buffer) {
					memcpy(this->confidence_buffer, data.confidenceMap,
							width * height * sizeof(int));
				}
				if (0 != this->vertex_buffer) {
					for (int i = 0; i < width * height; ++i) {
						this->vertex_buffer[i * 3] =
								data.verticesFloatingPoint[i].x;
						this->vertex_buffer[i * 3 + 1] =
								data.verticesFloatingPoint[i].y;
						this->vertex_buffer[i * 3 + 2] =
								data.verticesFloatingPoint[i].z;
					}
				}
				if (0 != this->uv_buffer) {
					for (int i = 0; i < width * height; ++i) {
						this->uv_buffer[i * 2] =
								data.uvMap[i].u;
						this->uv_buffer[i * 2 + 1] =
								data.uvMap[i].v;
					}
				}
			}
			{
				boost::mutex::scoped_lock l(this->updated_mutex);
				this->updated_info[0] = true;
				if (this->updated_info[0] && this->updated_info[1]) {
					emitEvent(this->event_updated);
					this->updated_info[0] = false;
					this->updated_info[1] = false;

				}
			}
		}

	}

	bool setProperty(const Property& property) {
		if ("Close mode" == property.type.name) {
			DepthSense::DepthNode::Configuration config =
					this->depth_node.getConfiguration();
			if (true == property.on_off) {
				config.mode = DepthSense::DepthNode::CAMERA_MODE_CLOSE_MODE;
			} else {
				config.mode = DepthSense::DepthNode::CAMERA_MODE_LONG_RANGE;
			}
			try {
				this->depth_node.setConfiguration(config);
			} catch (DepthSense::Exception e) {
				Log::add().error("setProperty", e.getMessage());
				return false;
			}
			this->porperties_map[property.type.name].on_off =
					!this->porperties_map[property.type.name].on_off;

			return true;
		}
		if ("Use MJPEG compression" == property.type.name) {
			bool running = this->stream_running;
			stopCapture();
			DepthSense::ColorNode::Configuration config =
					this->color_node.getConfiguration();
			if (true == property.on_off) {
				config.compression = DepthSense::COMPRESSION_TYPE_MJPEG;
			} else {
				config.compression = DepthSense::COMPRESSION_TYPE_YUY2;
			}
			try {
				this->color_node.setConfiguration(config);
			} catch (DepthSense::Exception e) {
				Log::add().error("setProperty", e.getMessage());
				if (true == running) {
					startCapture();
				}
				return false;
			}
			this->porperties_map[property.type.name].on_off =
					!this->porperties_map[property.type.name].on_off;

			if (true == running) {
				startCapture();
			}
			return true;
		}
		if ("Power line frequency 50Hz" == property.type.name) {
			DepthSense::ColorNode::Configuration config =
					this->color_node.getConfiguration();
			if (true == property.on_off) {
				config.powerLineFrequency =
						DepthSense::POWER_LINE_FREQUENCY_50HZ;
			} else {
				config.powerLineFrequency =
						DepthSense::POWER_LINE_FREQUENCY_60HZ;
			}
			try {
				this->color_node.setConfiguration(config);
			} catch (DepthSense::Exception e) {
				Log::add().error("setProperty", e.getMessage());
				return false;
			}
			this->porperties_map[property.type.name].on_off =
					!this->porperties_map[property.type.name].on_off;
			return true;
		}
		if ("Brightness" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				if (this->porperties_map[property.type.name].info.imax
						>= property.ivalue
						&& this->porperties_map[property.type.name].info.imin
								<= property.ivalue) {
					try {
						this->color_node.setBrightness(property.ivalue);
					} catch (DepthSense::Exception e) {
						Log::add().error("setProperty", e.getMessage());
						return false;
					}
					this->porperties_map[property.type.name].ivalue =
							this->color_node.getBrightness();
					return true;

				}
			}
		}
		if ("Contrast" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				if (this->porperties_map[property.type.name].info.imax
						>= property.ivalue
						&& this->porperties_map[property.type.name].info.imin
								<= property.ivalue) {
					try {
						this->color_node.setContrast(property.ivalue);
					} catch (DepthSense::Exception e) {
						Log::add().error("setProperty", e.getMessage());
						return false;
					}
					this->porperties_map[property.type.name].ivalue =
							this->color_node.getContrast();
					return true;

				}
			}
		}
		if ("Exposure" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				if (this->porperties_map[property.type.name].info.imax
						>= property.ivalue
						&& this->porperties_map[property.type.name].info.imin
								<= property.ivalue) {
					try {
						this->color_node.setExposure(property.ivalue);
					} catch (DepthSense::Exception e) {
						Log::add().error("setProperty", e.getMessage());
						return false;
					}
					this->porperties_map[property.type.name].ivalue =
							this->color_node.getExposure();
					return true;

				}
			}
		}
		if ("Gamma" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				if (this->porperties_map[property.type.name].info.imax
						>= property.ivalue
						&& this->porperties_map[property.type.name].info.imin
								<= property.ivalue) {
					try {
						this->color_node.setGamma(property.ivalue);
					} catch (DepthSense::Exception e) {
						Log::add().error("setProperty", e.getMessage());
						return false;
					}
					this->porperties_map[property.type.name].ivalue =
							this->color_node.getGamma();
					return true;

				}
			}
		}
		if ("Hue" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				if (this->porperties_map[property.type.name].info.imax
						>= property.ivalue
						&& this->porperties_map[property.type.name].info.imin
								<= property.ivalue) {
					try {
						this->color_node.setHue(property.ivalue);
					} catch (DepthSense::Exception e) {
						Log::add().error("setProperty", e.getMessage());
						return false;
					}
					this->porperties_map[property.type.name].ivalue =
							this->color_node.getHue();
					return true;

				}
			}
		}
		if ("Sharpness" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				if (this->porperties_map[property.type.name].info.imax
						>= property.ivalue
						&& this->porperties_map[property.type.name].info.imin
								<= property.ivalue) {
					try {
						this->color_node.setSharpness(property.ivalue);
					} catch (DepthSense::Exception e) {
						Log::add().error("setProperty", e.getMessage());
						return false;
					}
					this->porperties_map[property.type.name].ivalue =
							this->color_node.getSharpness();
					return true;

				}
			}
		}
		if ("White balance" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				if (this->porperties_map[property.type.name].info.imax
						>= property.ivalue
						&& this->porperties_map[property.type.name].info.imin
								<= property.ivalue) {
					try {
						this->color_node.setWhiteBalance(property.ivalue);
					} catch (DepthSense::Exception e) {
						Log::add().error("setProperty", e.getMessage());
						return false;
					}
					this->porperties_map[property.type.name].ivalue =
							this->color_node.getWhiteBalance();
					return true;

				}
			}
		}
		if ("Exposure auto" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				try {
					if (true == property.auto_manual_mode) {
						this->color_node.setExposureAuto(
								DepthSense::EXPOSURE_AUTO_APERTURE_PRIORITY);
					} else {
						this->color_node.setExposureAuto(
								DepthSense::EXPOSURE_AUTO_MANUAL);
					}
				} catch (DepthSense::Exception e) {
					Log::add().error("setProperty", e.getMessage());
					return false;
				}
				if (DepthSense::EXPOSURE_AUTO_APERTURE_PRIORITY
						== this->color_node.getExposureAuto()) {
					this->porperties_map[property.type.name].auto_manual_mode =
							true;
				} else {
					this->porperties_map[property.type.name].auto_manual_mode =
							false;
				}
				return true;

			}
		}
		if ("Exposure auto priority" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				try {
					this->color_node.setExposureAutoPriority(
							property.auto_manual_mode);
				} catch (DepthSense::Exception e) {
					Log::add().error("setProperty", e.getMessage());
					return false;
				}
				this->porperties_map[property.type.name].auto_manual_mode =
						this->color_node.getExposureAutoPriority();
				return true;
			}
		}
		if ("White balance auto" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				try {
					this->color_node.setWhiteBalanceAuto(
							property.auto_manual_mode);
				} catch (DepthSense::Exception e) {
					Log::add().error("setProperty", e.getMessage());
					return false;
				}
				this->porperties_map[property.type.name].auto_manual_mode =
						this->color_node.getWhiteBalanceAuto();
				return true;
			}
		}
		if ("Denoising" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {
				try {
					this->depth_node.setEnableDenoising(
							property.auto_manual_mode);
				} catch (DepthSense::Exception e) {
					Log::add().error("setProperty", e.getMessage());
					return false;
				}
				this->porperties_map[property.type.name].auto_manual_mode =
						this->depth_node.getEnableDenoising();
				return true;
			}
		}
		if ("Illumination level" == property.type.name) {
			if (true
					== this->porperties_map[property.type.name].info.changeable) {

				try {
					this->depth_node.setIlluminationLevel(property.ivalue);
				} catch (DepthSense::Exception e) {
					Log::add().error("setProperty", e.getMessage());
					return false;
				}
				this->porperties_map[property.type.name].ivalue =
						this->depth_node.getIlluminationLevel();
				return true;
			}
		}

		return false;
	}

private:

	static void onNewDepthSample_cb(DepthSense::DepthNode node,
			DepthSense::DepthNode::NewSampleReceivedData data,
			DepthSenseGrabber* g) {
		if (0 != g) {
			g->onNewDepthSample(data);
		}
	}

	static void onNewColorSample_cb(DepthSense::ColorNode node,
			DepthSense::ColorNode::NewSampleReceivedData data,
			DepthSenseGrabber* g) {
		if (0 != g) {
			g->onNewColorSample(data);
		}
	}

	DepthSense::FrameFormat getDepthSenseFormat(const Size& s) {
		if (160 == s.width && 120 == s.height) {
			return DepthSense::FRAME_FORMAT_QQVGA;
		}
		if (176 == s.width && 144 == s.height) {
			return DepthSense::FRAME_FORMAT_QCIF;
		}
		if (240 == s.width && 160 == s.height) {
			return DepthSense::FRAME_FORMAT_HQVGA;
		}
		if (320 == s.width && 240 == s.height) {
			return DepthSense::FRAME_FORMAT_QVGA;
		}
		if (352 == s.width && 288 == s.height) {
			return DepthSense::FRAME_FORMAT_CIF;
		}
		if (480 == s.width && 320 == s.height) {
			return DepthSense::FRAME_FORMAT_HVGA;
		}
		if (640 == s.width && 480 == s.height) {
			return DepthSense::FRAME_FORMAT_VGA;
		}
		if (1280 == s.width && 720 == s.height) {
			return DepthSense::FRAME_FORMAT_WXGA_H;
		}
		if (320 == s.width && 120 == s.height) {
			return DepthSense::FRAME_FORMAT_DS311;
		}
		if (1024 == s.width && 768 == s.height) {
			return DepthSense::FRAME_FORMAT_XGA;
		}
		if (800 == s.width && 600 == s.height) {
			return DepthSense::FRAME_FORMAT_SVGA;
		}
		if (636 == s.width && 480 == s.height) {
			return DepthSense::FRAME_FORMAT_OVVGA;
		}
		if (640 == s.width && 240 == s.height) {
			return DepthSense::FRAME_FORMAT_WHVGA;
		}
		if (640 == s.width && 360 == s.height) {
			return DepthSense::FRAME_FORMAT_NHD;
		}
		return DepthSense::FRAME_FORMAT_UNKNOWN;
	}

	bool applyNewColorFormat(const DeviceFormat& df) {
		DepthSense::ColorNode::Configuration config =
				this->color_node.getConfiguration();
		config.frameFormat = getDepthSenseFormat(df.color.resolution);
		config.framerate = df.color.framerate_num;
		try {
			this->color_node.setConfiguration(config);
		} catch (DepthSense::Exception e) {
			Log::add().error("DepthSenseGrabber::applyNewColorFormat",
					e.getMessage());
			return false;
		}
		this->device_format.color.resolution = df.color.resolution;
		this->device_format.color.framerate_num = df.color.framerate_num;
		delete[] this->color_buffer;
		this->color_buffer = new unsigned char[df.color.resolution.width
				* df.color.resolution.width * 3];
		updateParameters();
		return true;
	}

	bool applyNewDepthFormat(const DeviceFormat& df) {
		DepthSense::DepthNode::Configuration config =
				this->depth_node.getConfiguration();
		config.frameFormat = getDepthSenseFormat(df.depth.resolution);
		config.framerate = df.depth.framerate_num;
		try {
			this->depth_node.setConfiguration(config);
		} catch (DepthSense::Exception e) {
			Log::add().error("DepthSenseGrabber::applyNewDepthFormat",
					e.getMessage());
			return false;
		}
		this->device_format.depth.resolution = df.depth.resolution;
		this->device_format.depth.framerate_num = df.depth.framerate_num;
		delete[] this->depth_buffer;
		this->depth_buffer = new float[df.depth.resolution.width
				* df.depth.resolution.width];
		delete[] this->vertex_buffer;
		this->vertex_buffer = new float[df.depth.resolution.width
				* df.depth.resolution.width * 3];
		delete[] this->confidence_buffer;
		this->confidence_buffer = new int[df.depth.resolution.width
				* df.depth.resolution.width];
		updateParameters();
		generateAuxBuffers();
		return true;
	}

	bool checkColorFormat(const DepthSense::FrameFormat& f,
			const unsigned int& width, const unsigned int& height) {
		DepthSense::ColorNode::Configuration config_save =
				this->color_node.getConfiguration();
		DepthSense::ColorNode::Configuration config =
				this->color_node.getConfiguration();

		bool ok = true;
		config.frameFormat = f;
		try {
			this->color_node.setConfiguration(config);
		} catch (DepthSense::Exception e) {
			ok = false;
		}
		if (true == ok) {
			DeviceFormat df;
			df.color.resolution.width = width;
			df.color.resolution.height = height;
			df.color.framerate_num = config_save.framerate;
			df.color.format = RGB888U;
			for (int i = 1; i < 20; ++i) {
				ok = true;
				config.framerate = i * 5;
				try {
					this->color_node.setConfiguration(config);
				} catch (DepthSense::Exception e) {
					ok = false;
				}
				if (true == ok) {
					df.color.framerates.push_back(
							std::pair<unsigned int, unsigned int>(i * 5, 1));
				}
			}
			this->device_format_list.push_back(df);
		}
		this->color_node.setConfiguration(config_save);
		return true;
	}

	bool checkDepthFormat(const DepthSense::FrameFormat& f,
			const unsigned int& width, const unsigned int& height) {
		DepthSense::DepthNode::Configuration config_save =
				this->depth_node.getConfiguration();
		DepthSense::DepthNode::Configuration config =
				this->depth_node.getConfiguration();

		bool ok = true;
		config.frameFormat = f;
		try {
			this->depth_node.setConfiguration(config);
		} catch (DepthSense::Exception e) {
			ok = false;
		}
		if (true == ok) {
			DeviceFormat df;
			df.depth.resolution.width = width;
			df.depth.resolution.height = height;
			df.depth.framerate_num = config_save.framerate;
			df.depth.format = GRAY32F;
			for (int i = 1; i < 20; ++i) {
				ok = true;
				config.framerate = i * 5;
				try {
					this->depth_node.setConfiguration(config);
				} catch (DepthSense::Exception e) {
					ok = false;
				}
				if (true == ok) {
					df.depth.framerates.push_back(
							std::pair<unsigned int, unsigned int>(i * 5, 1));
				}

			}
			this->device_format_list.push_back(df);
		}
		this->depth_node.setConfiguration(config_save);
		return true;
	}

	void feedFormat(void) {
		this->device_format_list.clear();

		checkColorFormat(DepthSense::FRAME_FORMAT_QQVGA, 160, 120);
		checkColorFormat(DepthSense::FRAME_FORMAT_QCIF, 176, 144);
		checkColorFormat(DepthSense::FRAME_FORMAT_HQVGA, 240, 160);
		checkColorFormat(DepthSense::FRAME_FORMAT_QVGA, 320, 240);
		checkColorFormat(DepthSense::FRAME_FORMAT_CIF, 352, 288);
		checkColorFormat(DepthSense::FRAME_FORMAT_HVGA, 480, 320);
		checkColorFormat(DepthSense::FRAME_FORMAT_VGA, 640, 480);
		checkColorFormat(DepthSense::FRAME_FORMAT_WXGA_H, 1280, 720);
		checkColorFormat(DepthSense::FRAME_FORMAT_DS311, 320, 120);
		checkColorFormat(DepthSense::FRAME_FORMAT_XGA, 1024, 768);
		checkColorFormat(DepthSense::FRAME_FORMAT_SVGA, 800, 600);
		checkColorFormat(DepthSense::FRAME_FORMAT_OVVGA, 636, 480);
		checkColorFormat(DepthSense::FRAME_FORMAT_WHVGA, 640, 240);
		checkColorFormat(DepthSense::FRAME_FORMAT_NHD, 640, 360);

		checkDepthFormat(DepthSense::FRAME_FORMAT_QQVGA, 160, 120);
		checkDepthFormat(DepthSense::FRAME_FORMAT_QCIF, 176, 144);
		checkDepthFormat(DepthSense::FRAME_FORMAT_HQVGA, 240, 160);
		checkDepthFormat(DepthSense::FRAME_FORMAT_QVGA, 320, 240);
		checkDepthFormat(DepthSense::FRAME_FORMAT_CIF, 352, 288);
		checkDepthFormat(DepthSense::FRAME_FORMAT_HVGA, 480, 320);
		checkDepthFormat(DepthSense::FRAME_FORMAT_VGA, 640, 480);
		checkDepthFormat(DepthSense::FRAME_FORMAT_WXGA_H, 1280, 720);
		checkDepthFormat(DepthSense::FRAME_FORMAT_DS311, 320, 120);
		checkDepthFormat(DepthSense::FRAME_FORMAT_XGA, 1024, 768);
		checkDepthFormat(DepthSense::FRAME_FORMAT_SVGA, 800, 600);
		checkDepthFormat(DepthSense::FRAME_FORMAT_OVVGA, 636, 480);
		checkDepthFormat(DepthSense::FRAME_FORMAT_WHVGA, 640, 240);
		checkDepthFormat(DepthSense::FRAME_FORMAT_NHD, 640, 360);

	}

	void updateParameters(void) {

		DepthSense::StereoCameraParameters params =
				this->device.getStereoCameraParameters();

		{
			Property p;
			p.type.name = "Depth intrinsic";
			p.info.has_matrix = true;
			p.info.changeable = false;
			p.mat.reset(new float[9]);
			p.mat.get()[0] = params.depthIntrinsics.fx;
			p.mat.get()[4] = params.depthIntrinsics.fy;
			p.mat.get()[2] = params.depthIntrinsics.cx;
			p.mat.get()[5] = params.depthIntrinsics.cy;
			p.mat_size = Size(3,3);
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Depth distortion";
			p.info.has_matrix = true;
			p.info.changeable = false;
			p.mat.reset(new float[5]);
			p.mat.get()[0] = params.depthIntrinsics.k1;
			p.mat.get()[1] = params.depthIntrinsics.k2;
			p.mat.get()[2] = params.depthIntrinsics.k3;
			p.mat.get()[3] = params.depthIntrinsics.p1;
			p.mat.get()[4] = params.depthIntrinsics.p2;
			p.mat_size = Size(5,1);
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Color intrinsic";
			p.info.has_matrix = true;
			p.info.changeable = false;
			p.mat.reset(new float[9]);
			p.mat.get()[0] = params.colorIntrinsics.fx;
			p.mat.get()[4] = params.colorIntrinsics.fy;
			p.mat.get()[2] = params.colorIntrinsics.cx;
			p.mat.get()[5] = params.colorIntrinsics.cy;
			p.mat_size = Size(3,3);
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Color distortion";
			p.info.has_matrix = true;
			p.info.changeable = false;
			p.mat.reset(new float[5]);
			p.mat.get()[0] = params.colorIntrinsics.k1;
			p.mat.get()[1] = params.colorIntrinsics.k2;
			p.mat.get()[2] = params.colorIntrinsics.k3;
			p.mat.get()[3] = params.colorIntrinsics.p1;
			p.mat.get()[4] = params.colorIntrinsics.p2;
			p.mat_size = Size(5,1);
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Extrinsic distortion";
			p.info.has_matrix = true;
			p.info.changeable = false;
			p.mat.reset(new float[12]);
			p.mat.get()[0] = params.extrinsics.r11;
			p.mat.get()[1] = params.extrinsics.r12;
			p.mat.get()[2] = params.extrinsics.r13;
			p.mat.get()[3] = params.extrinsics.t1;
			p.mat.get()[4] = params.extrinsics.r21;
			p.mat.get()[5] = params.extrinsics.r22;
			p.mat.get()[6] = params.extrinsics.r23;
			p.mat.get()[7] = params.extrinsics.t2;
			p.mat.get()[8] = params.extrinsics.r31;
			p.mat.get()[9] = params.extrinsics.r32;
			p.mat.get()[10] = params.extrinsics.r33;
			p.mat.get()[11] = params.extrinsics.t3;
			p.mat_size = Size(4,3);
			this->porperties_map[p.type.name] = p;
		}
	}

	void generateProperties(void) {
		{
			Property p;
			p.type.name = "Close mode";
			p.type.description =
					"Switch between the near and long range capture mode";
			DepthSense::DepthNode::Configuration config =
					this->depth_node.getConfiguration();
			if (DepthSense::DepthNode::CAMERA_MODE_LONG_RANGE == config.mode) {
				p.on_off = false;
			} else {
				p.on_off = true;
			}
			p.info.has_on_off = true;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Use MJPEG compression";
			p.type.description = "May not work with some devices";
			DepthSense::ColorNode::Configuration config =
					this->color_node.getConfiguration();
			if (DepthSense::COMPRESSION_TYPE_MJPEG == config.compression) {
				p.on_off = true;
			} else {
				p.on_off = false;
			}
			p.info.has_on_off = true;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Power line frequency 50Hz";
			p.type.description = "";
			DepthSense::ColorNode::Configuration config =
					this->color_node.getConfiguration();
			if (DepthSense::POWER_LINE_FREQUENCY_50HZ
					== config.powerLineFrequency) {
				p.on_off = true;
			} else {
				p.on_off = false;
			}
			p.info.has_on_off = true;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Depth framerate";
			p.type.description = "";
			p.info.has_fvalue = true;
			p.fvalue = this->framerate_depth;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Color framerate";
			p.type.description = "";
			p.info.has_fvalue = true;
			p.fvalue = this->framerate_color;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Brightness";
			p.type.description = "For color image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.brightnessIsReadOnly();
			p.info.imin = -10;
			p.info.imax = 10;
			p.ivalue = this->color_node.getBrightness();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Contrast";
			p.type.description = "For color image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.contrastIsReadOnly();
			p.info.imin = 1;
			p.info.imax = 32;
			p.ivalue = this->color_node.getContrast();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Exposure";
			p.type.description = "For color image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.exposureIsReadOnly();
			p.info.imin = 156;
			p.info.imax = 5000;
			p.ivalue = this->color_node.getExposure();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Exposure auto";
			p.type.description = "For color image";
			p.info.has_auto_manual_mode = true;
			p.info.changeable = !this->color_node.exposureAutoIsReadOnly();
			if (DepthSense::EXPOSURE_AUTO_APERTURE_PRIORITY
					== this->color_node.getExposureAuto()) {
				p.auto_manual_mode = true;
			} else {
				p.auto_manual_mode = false;
			}
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Exposure auto priority";
			p.type.description = "For color image";
			p.info.has_auto_manual_mode = true;
			p.info.changeable =
					!this->color_node.exposureAutoPriorityIsReadOnly();
			p.auto_manual_mode = this->color_node.getExposureAutoPriority();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Gamma";
			p.type.description = "For color image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.gammaIsReadOnly();
			p.info.imin = 100;
			p.info.imax = 200;
			p.ivalue = this->color_node.getGamma();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Hue";
			p.type.description = "For color image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.hueIsReadOnly();
			p.info.imin = -5;
			p.info.imax = 5;
			p.ivalue = this->color_node.getHue();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Saturation";
			p.type.description = "For color image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.saturationIsReadOnly();
			p.info.imin = 0;
			p.info.imax = 20;
			p.ivalue = this->color_node.getSaturation();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Sharpness";
			p.type.description = "For color image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.sharpnessIsReadOnly();
			p.info.imin = 0;
			p.info.imax = 10;
			p.ivalue = this->color_node.getSharpness();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "White balance";
			p.type.description = "For color image, step of 1850";
			p.info.has_ivalue = true;
			p.info.changeable = !this->color_node.whiteBalanceIsReadOnly();
			p.info.imin = 2800;
			p.info.imax = 6500;
			p.istep = 1850;
			p.ivalue = this->color_node.getWhiteBalance();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "White balance auto";
			p.type.description = "For color image";
			p.info.has_auto_manual_mode = true;
			p.info.changeable = !this->color_node.whiteBalanceAutoIsReadOnly();
			p.auto_manual_mode = this->color_node.getWhiteBalanceAuto();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Illumination level";
			p.type.description = "For the depth image";
			p.info.has_ivalue = true;
			p.info.changeable = !this->depth_node.illuminationLevelIsReadOnly();
			p.info.imin = 0;
			p.info.imax = 0;
			p.ivalue = this->depth_node.getIlluminationLevel();
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Denoising";
			p.type.description = "For the depth image";
			p.info.has_auto_manual_mode = true;
			p.info.changeable = !this->depth_node.enableDenoisingIsReadOnly();
			p.auto_manual_mode = this->depth_node.getEnableDenoising();
			this->porperties_map[p.type.name] = p;
		}

		updateParameters();
	}

	void generateAuxBuffers(void) {

		int width, height;
		DepthSense::DepthNode::Configuration config =
				this->depth_node.getConfiguration();
		DepthSense::FrameFormat_toResolution(config.frameFormat, &width,
				&height);

		{
			std::string s("Confidence map");
			if (this->auxiliary_buffer_map.count(s) == 0) {
				AuxiliaryBuffer b;
				b.type.name = s;
				b.size = Size(width, height);
				b.depth = GEDEON_32S;
				b.channels = 1;
				b.data.reset(new unsigned char[width * height * sizeof(int)]);
				this->auxiliary_buffer_map[s] = b;
			} else {
				this->auxiliary_buffer_map[s].data.reset(
						new unsigned char[width * height * sizeof(int)]);
			}
		}
		{
			std::string s("Vertex map");
			if (this->auxiliary_buffer_map.count(s) == 0) {
				AuxiliaryBuffer b;
				b.type.name = s;
				b.type.description =
						"Expressed in meters. Saturated pixels are expressed with -2.";
				b.size = Size(width, height);
				b.depth = GEDEON_32F;
				b.channels = 3;
				b.data.reset(
						new unsigned char[width * height * 3 * sizeof(float)]);
				this->auxiliary_buffer_map[s] = b;
			} else {
				this->auxiliary_buffer_map[s].data.reset(
						new unsigned char[width * height * 3 * sizeof(float)]);
			}
		}
		{
			std::string s("UV map");
			if (this->auxiliary_buffer_map.count(s) == 0) {
				AuxiliaryBuffer b;
				b.type.name = s;
				b.type.description =
						"This map represents the normalized coordinates of each pixel in the color map. Invalid pixels are given the special value -FLT_MAX";
				b.size = Size(width, height);
				b.depth = GEDEON_32F;
				b.channels = 2;
				b.data.reset(
						new unsigned char[width * height * 2 * sizeof(float)]);
				this->auxiliary_buffer_map[s] = b;
			} else {
				this->auxiliary_buffer_map[s].data.reset(
						new unsigned char[width * height * 2 * sizeof(float)]);
			}
		}

	}

}
;

class DepthSenseDriver: public Driver {

public:

	DepthSenseDriver(void) :
			Driver() {
		this->driver_name = std::string("depthsense");

		try {
			this->g_context = DepthSense::Context::create();
		} catch (DepthSense::Exception e) {
			throw Exception("DepthSenseDriver::DepthSenseDriver",
					e.getMessage());
		}
		pthread = new boost::thread(
				boost::bind(&DepthSenseDriver::startServer, this));
	}

	~DepthSenseDriver(void) {
		this->g_context.stopNodes();
		this->g_context.quit();
		this->grabbers.clear();
		this->devices.clear();
		if (0 != pthread) {
			delete pthread;
		}
	}

	bool update(void) {

		this->list_grabbers.clear();
		this->devices.clear();

		std::vector<DepthSense::Device> devices = this->g_context.getDevices();
		std::vector<DepthSense::Device>::iterator it = devices.begin();

		while (it != devices.end()) {
			this->list_grabbers.push_back(
					it->Model_toString(it->getModel()) + " "
							+ it->getSerialNumber());
			this->devices[it->Model_toString(it->getModel()) + " "
					+ it->getSerialNumber()] = *it;
			++it;
		}

		/*
		 * Remove no more connected devices
		 */
		std::vector<std::string> to_remove;
		std::map<std::string, GrabberPtr>::const_iterator it_grabbers =
				this->grabbers.begin();
		while (it_grabbers != this->grabbers.end()) {
			if (std::find(this->list_grabbers.begin(),
					this->list_grabbers.end(), it_grabbers->first)
					== this->list_grabbers.end()) {
				to_remove.push_back(it_grabbers->first);
			}
			++it_grabbers;
		}
		std::vector<std::string>::const_iterator it_to_remove =
				to_remove.begin();
		while (it_to_remove != to_remove.end()) {
			this->grabbers.erase(*it_to_remove);
			++it_to_remove;
		}

		to_remove.clear();
		return true;
	}

	GrabberPtr getGrabber(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			if (0 >= this->devices.count(name)) {
				Log::add().error("DepthSenseDriver::getGrabber",
						"This camera was not found (" + name + ")");
				GrabberPtr p;
				p.reset();
				return p;
			}
			try {

				this->grabbers[name].reset(
						new DepthSenseGrabber(this->devices[name],
								this->g_context));
			} catch (Exception e) {
				Log::add().error("DepthSenseDriver::getGrabber", e.what());
				GrabberPtr p;
				p.reset();
				return p;
			}
		}

		return this->grabbers[name];
	}

	void startServer(void) {
		this->g_context.startNodes();
		this->g_context.run();
	}

private:

	std::map<std::string, GrabberPtr> grabbers;
	std::map<std::string, DepthSense::Device> devices;
	DepthSense::Context g_context;
	boost::thread *pthread;

};
// end class DepthSenseDriver

static DepthSenseDriver* depthsense_driver = 0;

extern "C" GEDEON_EXPORT_PLUGIN int getVersion(void) {
	return DRIVER_VERSION;
}

extern "C" GEDEON_EXPORT_PLUGIN Driver* getDriver(void) {
	if (0 == depthsense_driver) {
		depthsense_driver = new DepthSenseDriver();
	}
	return dynamic_cast<Driver*>(depthsense_driver);
}

extern "C" GEDEON_EXPORT_PLUGIN void releaseDriver(void) {
	if (0 != depthsense_driver) {
		delete depthsense_driver;
		depthsense_driver = 0;
	}
}

}
// end namespace gedeon

