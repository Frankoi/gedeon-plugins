/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * depthsense.cpp created in 05 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * depthsense.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

// Internal includes
#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/capturedevice/driver.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <map>
#include <XnCppWrapper.h>
#include <XnLog.h>
#include <XnTypes.h>

namespace gedeon {

class OpenNIGrabber: public Grabber {

private:

	bool has_depth;
	bool has_color;
	bool has_ir;
	bool is_file;

	unsigned int timestamp[2];

	xn::DepthGenerator depth;
	xn::IRGenerator ir;
	xn::ImageGenerator color;
	xn::Context *context;

	boost::mutex color_mutex;
	boost::mutex depth_mutex;
	boost::mutex ir_mutex;

	xn::Recorder recorder;
	xn::Player player;
	unsigned char* color_buffer;
	float* depth_buffer;
	unsigned char* ir_buffer;
public:

	OpenNIGrabber(xn::Context* c, xn::NodeInfo node) :
			Grabber() {

		const XnProductionNodeDescription& description = node.GetDescription();
		std::string n(
				std::string(description.strVendor) + " "
						+ std::string(description.strName));
		const XnChar * creation_info = node.GetCreationInfo();
		if (c != 0) {
			n += std::string("[") + creation_info + "]";
		}

		this->name = n;

		this->color_buffer = 0;
		this->depth_buffer = 0;
		this->ir_buffer = 0;

		this->is_file = false;
		this->context = c;

		this->rgbdi.reset(new RGBDIImage);

		XnStatus nRetVal = XN_STATUS_OK;

		this->has_ir = false;
		this->has_color = false;
		this->has_depth = false;

		nRetVal = this->depth.Create(*this->context);
		if (XN_STATUS_OK != nRetVal) {
			throw Exception("OpenNIGrabber::OpenNIGrabber",
					xnGetStatusString(nRetVal));
		}
		nRetVal = this->color.Create(*this->context);
		if (XN_STATUS_OK != nRetVal) {
			throw Exception("OpenNIGrabber::OpenNIGrabber",
					xnGetStatusString(nRetVal));
		}
		nRetVal = this->ir.Create(*this->context);
		if (XN_STATUS_OK != nRetVal) {
			throw Exception("OpenNIGrabber::OpenNIGrabber",
					xnGetStatusString(nRetVal));
		}

		if (this->depth.IsValid()) {
			xn::DepthMetaData depthmetadata;
			this->depth.GetMetaData(depthmetadata);
			unsigned int width = depthmetadata.FullXRes();
			unsigned int height = depthmetadata.FullYRes();
			this->device_format.depth.resolution = Size(width, height);
			this->device_format.depth.format = GRAY32F;
			this->device_format.depth.framerate_num = depthmetadata.FPS();
			this->adaptative_framerate = depthmetadata.FPS();
			this->rgbdi.get()->depth.setSize(Size(width, height));
			this->depth_buffer = new float[width * height];
			this->has_depth = true;
		}

		if (this->color.IsValid()) {
			xn::ImageMetaData colormetadata;
			this->color.GetMetaData(colormetadata);
			unsigned int width = colormetadata.FullXRes();
			unsigned int height = colormetadata.FullYRes();
			this->device_format.color.resolution = Size(width, height);
			this->device_format.color.format = RGB888U;
			this->device_format.color.framerate_num = colormetadata.FPS();
			this->rgbdi.get()->color.setSize(Size(width, height));
			this->color_buffer = new unsigned char[width * height * 3];
			this->has_color = true;
		}

		if (this->depth.IsCapabilitySupported(
				XN_CAPABILITY_ALTERNATIVE_VIEW_POINT) && this->color.IsValid()
				&& this->depth.IsValid()) {
			this->depth.GetAlternativeViewPointCap().SetViewPoint(this->color);
		}

		if (this->ir.IsValid()) {
			xn::IRMetaData irmetadata;
			this->ir.GetMetaData(irmetadata);
			unsigned int width = irmetadata.FullXRes();
			unsigned int height = irmetadata.FullYRes();
			this->device_format.intensity.resolution = Size(width, height);
			this->device_format.intensity.format = GRAY8U;
			this->device_format.intensity.framerate_num = irmetadata.FPS();
			this->ir_buffer = new unsigned char[width * height];
			if (this->has_color) {
				this->has_ir = false;
			} else {
				this->rgbdi.get()->intensity.setSize(Size(width, height));
				this->has_ir = true;
			}
		}

		generateProperties();
		feedFormat();

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);
	}

	OpenNIGrabber(xn::Context* c, const std::string& filename) :
			Grabber() {

		this->name = filename;

		this->color_buffer = 0;
		this->depth_buffer = 0;
		this->ir_buffer = 0;

		this->is_file = true;
		this->context = c;

		this->rgbdi.reset(new RGBDIImage);

		XnStatus nRetVal = this->context->OpenFileRecording(filename.c_str(),
				this->player);
		if (nRetVal != XN_STATUS_OK) {
			throw Exception("OpenNIGrabber::OpenNIGrabber",
					xnGetStatusString(nRetVal));
		}
		this->has_ir = false;
		this->has_color = false;
		this->has_depth = false;

		xn::NodeInfoList list;
		this->player.EnumerateNodes(list);
		for (xn::NodeInfoList::Iterator it = list.Begin(); it != list.End();
				++it) {
			if (XN_NODE_TYPE_DEPTH == (*it).GetDescription().Type) {
				(*it).GetInstance(this->depth);
				this->has_depth = true;
			}
			if (XN_NODE_TYPE_IMAGE == (*it).GetDescription().Type) {
				(*it).GetInstance(this->color);
				this->has_color = true;
			}
			if (XN_NODE_TYPE_IR == (*it).GetDescription().Type) {
				(*it).GetInstance(this->ir);
				this->has_ir = true;
			}
		}

		if (this->has_depth) {
			xn::DepthMetaData depthmetadata;
			this->depth.GetMetaData(depthmetadata);
			unsigned int width = depthmetadata.FullXRes();
			unsigned int height = depthmetadata.FullYRes();
			this->device_format.depth.resolution = Size(width, height);
			this->device_format.depth.format = GRAY32F;
			this->device_format.depth.framerate_num = depthmetadata.FPS();
			this->adaptative_framerate = depthmetadata.FPS();
			this->rgbdi.get()->depth.setSize(Size(width, height));
			this->depth_buffer = new float[width * height];
		}

		if (this->has_color) {
			xn::ImageMetaData colormetadata;
			this->color.GetMetaData(colormetadata);
			unsigned int width = colormetadata.FullXRes();
			unsigned int height = colormetadata.FullYRes();
			this->device_format.color.resolution = Size(width, height);
			this->device_format.color.format = RGB888U;
			this->device_format.color.framerate_num = colormetadata.FPS();
			this->rgbdi.get()->color.setSize(Size(width, height));
			this->color_buffer = new unsigned char[width * height * 3];
		}

		if (this->depth.IsCapabilitySupported(
				XN_CAPABILITY_ALTERNATIVE_VIEW_POINT)) {
			this->depth.GetAlternativeViewPointCap().SetViewPoint(this->color);
		}

		if (this->has_ir) {
			xn::IRMetaData irmetadata;
			this->ir.GetMetaData(irmetadata);
			unsigned int width = irmetadata.FullXRes();
			unsigned int height = irmetadata.FullYRes();
			this->device_format.intensity.resolution = Size(width, height);
			this->device_format.intensity.format = GRAY8U;
			this->device_format.intensity.framerate_num = irmetadata.FPS();
			this->rgbdi.get()->intensity.setSize(Size(width, height));
			this->ir_buffer = new unsigned char[width * height];
		}

		generateProperties();

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);
	}

	~OpenNIGrabber(void) {
		this->stopCapture();

		if (0 != this->color_buffer) {
			delete[] this->color_buffer;
		}

		if (0 != this->depth_buffer) {
			delete[] this->depth_buffer;
		}

		if (0 != this->ir_buffer) {
			delete[] this->ir_buffer;
		}

		if (true == this->recorder.IsValid()) {
			this->recorder.RemoveNodeFromRecording(this->depth);
			this->recorder.RemoveNodeFromRecording(this->ir);
			this->recorder.RemoveNodeFromRecording(this->color);
			this->recorder.Release();
		}
	}

	void startCapture(void) {
		this->stream_paused = false;
		if (false == this->stream_running) {
			XnStatus nRetVal;
			if (this->is_file == false) {
				if (true == this->has_color) {
					nRetVal = this->color.StartGenerating();
					if (XN_STATUS_OK != nRetVal) {
						Log::add().error("OpenNIGrabber::startCapture",
								std::string("color: ")
										+ xnGetStatusString(nRetVal));
					}
				}
				if (true == this->has_depth) {
					nRetVal = this->depth.StartGenerating();
					if (XN_STATUS_OK != nRetVal) {
						Log::add().error("OpenNIGrabber::startCapture",
								std::string("Depth: ")
										+ xnGetStatusString(nRetVal));
					}
				}
				if (true == this->has_ir) {
					nRetVal = this->ir.StartGenerating();
					if (XN_STATUS_OK != nRetVal) {
						Log::add().error("OpenNIGrabber::startCapture",
								std::string("IR: ")
										+ xnGetStatusString(nRetVal));
					}
				}
			}

			if (0 != this->pthread) {
				this->pthread->interrupt();
				delete this->pthread;
				this->pthread = 0;
			}
			this->pthread = new boost::thread(
					boost::bind(&OpenNIGrabber::run, this));
			this->stream_running = true;
			millisecSleep(100);
			emitEvent(this->event_playing);
		}
	}

	void stopCapture(void) {

		if (true == this->stream_running) {
			this->stream_running = false;

			while (false == this->stream_finished) {
				millisecSleep(40);
			}
			if (0 != this->pthread) {
				this->pthread->interrupt();
				delete this->pthread;
				this->pthread = 0;
			}
			emitEvent(this->event_stopped);

			if (this->player.IsValid() != false) {
				if (this->color.IsValid()) {
					this->color.StopGenerating();
				}
				if (this->depth.IsValid()) {
					this->depth.StopGenerating();
				}
				if (this->ir.IsValid()) {
					this->ir.StopGenerating();
				}
			}
			if (true == this->recorder.IsValid()) {
				this->recorder.RemoveNodeFromRecording(this->depth);
				this->recorder.RemoveNodeFromRecording(this->ir);
				this->recorder.RemoveNodeFromRecording(this->color);
				this->recorder.Release();
			}
		}

	}

	RGBDIImagePtr getImage(void) {
		if (true == this->has_color) {
			unsigned char *color = this->rgbdi.get()->color.getData().get();
			{
				size_t buffer_size = this->rgbdi.get()->color.getSize().width
						* this->rgbdi.get()->color.getSize().height * 3;
				this->rgbdi.get()->color.setTimeStamp(timestamp[0]);
				boost::mutex::scoped_lock l(this->color_mutex);
				memcpy(color, this->color_buffer, buffer_size);
			}
		}
		if (true == this->has_depth) {
			float *depth = this->rgbdi.get()->depth.getData().get();
			{
				size_t buffer_size = this->rgbdi.get()->depth.getSize().width
						* this->rgbdi.get()->depth.getSize().height;
				this->rgbdi.get()->depth.setTimeStamp(timestamp[1]);
				boost::mutex::scoped_lock l(this->depth_mutex);
				memcpy(depth, this->depth_buffer, buffer_size * sizeof(float));
			}
		}
		if (true == this->has_ir) {
			unsigned char *ir = this->rgbdi.get()->intensity.getData().get();
			{
				size_t buffer_size =
						this->rgbdi.get()->intensity.getSize().width
								* this->rgbdi.get()->intensity.getSize().height;
				this->rgbdi.get()->intensity.setTimeStamp(timestamp[0]);
				boost::mutex::scoped_lock l(this->ir_mutex);
				memcpy(ir, this->ir_buffer, buffer_size);
			}
		}

		return this->rgbdi;
	}

	bool setFormat(const DeviceFormat& df) {

		XnStatus nRetVal;
		bool running = this->stream_running;
		stopCapture();

		if (df.color.resolution.width > 0 && df.color.resolution.height > 0
				&& 0 < df.color.framerate_num) {
			bool found = false;
			std::list<DeviceFormat>::const_iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end() && false == found) {
				if (it->color.resolution == df.color.resolution) {
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->color.framerates.begin();
					while (itf != it->color.framerates.end()) {
						if (itf->first == df.color.framerate_num) {
							found = true;
							break;
						}
						++itf;
					}
					break;
				}
				++it;
			}
			if (true == found) {
				if (this->color.IsValid()) {
					XnMapOutputMode outputMode;
					outputMode.nXRes = df.color.resolution.width;
					outputMode.nYRes = df.color.resolution.height;
					outputMode.nFPS = df.color.framerate_num;
					nRetVal = this->color.SetMapOutputMode(outputMode);
					if (XN_STATUS_OK != nRetVal) {
						Log::add().error("OpenNIGrabber::switchColorIR",
								xnGetStatusString(nRetVal));
					}
					this->color.GetMapOutputMode(outputMode);
					unsigned int width = outputMode.nXRes;
					unsigned int height = outputMode.nYRes;
					unsigned int framerate = outputMode.nFPS;

					this->device_format.color.resolution = Size(width, height);
					this->device_format.color.framerate_num = framerate;

					if (this->has_color) {
						this->rgbdi.get()->color.setSize(Size(width, height));
					}

					delete[] this->color_buffer;
					this->color_buffer = new unsigned char[width * height * 3];
				} else {
					this->device_format.intensity.resolution =
							df.intensity.resolution;
					this->device_format.intensity.framerate_num =
							df.intensity.framerate_num;
				}
			}
		}

		if (df.depth.resolution.width > 0 && df.depth.resolution.height > 0
				&& 0 < df.depth.framerate_num) {
			bool found = false;
			std::list<DeviceFormat>::const_iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end() && false == found) {
				if (it->depth.resolution == df.depth.resolution) {
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->depth.framerates.begin();
					while (itf != it->depth.framerates.end()) {
						if (itf->first == df.depth.framerate_num) {
							found = true;
							break;
						}
						++itf;
					}
					break;
				}
				++it;
			}
			if (true == found) {

				XnMapOutputMode outputMode;
				outputMode.nXRes = df.depth.resolution.width;
				outputMode.nYRes = df.depth.resolution.height;
				outputMode.nFPS = df.depth.framerate_num;
				nRetVal = this->depth.SetMapOutputMode(outputMode);
				if (XN_STATUS_OK != nRetVal) {
					Log::add().error("OpenNIGrabber::switchColorIR",
							xnGetStatusString(nRetVal));
				}

				this->depth.GetMapOutputMode(outputMode);
				unsigned int width = outputMode.nXRes;
				unsigned int height = outputMode.nYRes;
				unsigned int framerate = outputMode.nFPS;
				this->device_format.depth.resolution = Size(width, height);
				this->device_format.depth.framerate_num = framerate;
				this->rgbdi.get()->depth.setSize(Size(width, height));
				delete[] this->depth_buffer;
				this->depth_buffer = new float[width * height];
			}

		}

		if (df.intensity.resolution.width > 0
				&& df.intensity.resolution.height > 0
				&& 0 < df.intensity.framerate_num) {
			bool found = false;
			std::list<DeviceFormat>::const_iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end() && false == found) {
				if (it->intensity.resolution == df.intensity.resolution) {
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->intensity.framerates.begin();
					while (itf != it->intensity.framerates.end()) {
						if (itf->first == df.intensity.framerate_num) {
							found = true;
							break;
						}
						++itf;
					}
					break;
				}
				++it;
			}
			if (true == found) {

				if (this->ir.IsValid()) {
					XnMapOutputMode outputMode;
					outputMode.nXRes = df.intensity.resolution.width;
					outputMode.nYRes = df.intensity.resolution.height;
					outputMode.nFPS = df.intensity.framerate_num;
					nRetVal = this->ir.SetMapOutputMode(outputMode);
					if (XN_STATUS_OK != nRetVal) {
						Log::add().error("OpenNIGrabber::switchColorIR",
								xnGetStatusString(nRetVal));
					}

					this->ir.GetMapOutputMode(outputMode);
					unsigned int width = outputMode.nXRes;
					unsigned int height = outputMode.nYRes;
					unsigned int framerate = outputMode.nFPS;

					this->device_format.intensity.resolution = Size(width,
							height);
					this->device_format.intensity.framerate_num = framerate;

					if (this->has_ir) {
						this->rgbdi.get()->intensity.setSize(
								Size(width, height));
					}
					delete[] this->ir_buffer;
					this->ir_buffer = new unsigned char[width * height];
				} else {
					this->device_format.intensity.resolution =
							df.intensity.resolution;
					this->device_format.intensity.framerate_num =
							df.intensity.framerate_num;
				}
			}

		}

		if (running) {
			startCapture();
		}

		return true;
	}

	bool setProperty(const Property& property) {

		if ("Infrared" == property.type.name) {
			switchColorIR();
			return true;
		}

		if ("Record" == property.type.name) {
			if (false == this->recorder.IsValid()) {
				if (property.svalue != "") {
					std::string namelower = property.svalue;
					std::string name = property.svalue;
					std::transform(namelower.begin(), namelower.end(),
							namelower.begin(), ::tolower);
					if (false
							== boost::algorithm::ends_with(namelower, ".oni")) {
						name += ".oni";
					}
					this->recorder.Create(*this->context);
					this->recorder.SetDestination(XN_RECORD_MEDIUM_FILE,
							name.c_str());
					this->recorder.AddNodeToRecording(this->depth);
					if (this->has_color) {
						this->recorder.AddNodeToRecording(this->color);
					} else {
						this->recorder.AddNodeToRecording(this->ir);
					}
					this->porperties_map[property.type.name].svalue = name;
					this->porperties_map["Is recording"].on_off = true;
				} else {
					return false;
				}
			} else {
				this->recorder.RemoveNodeFromRecording(this->depth);
				this->recorder.RemoveNodeFromRecording(this->ir);
				this->recorder.RemoveNodeFromRecording(this->color);
				this->recorder.Release();
				this->porperties_map["Is recording"].on_off = false;
			}
			return true;
		}

		return false;
	}

	void notify(EventDataWPtr e) {

	}

	void run(void) {

		this->stream_finished = false;
		this->stream_running = true;

		Timer timer;
		timer.start();
		long int current_time;

		while (true == this->stream_running) {
			current_time = timer.get();

			if (false == this->stream_paused) {

				if (this->is_file == true) {
					this->player.ReadNext();
				}

				if (true == this->has_color) {
					if (this->color.IsNewDataAvailable()) {
						this->color.WaitAndUpdateData();
						boost::mutex::scoped_lock l(this->color_mutex);
						Size s = this->rgbdi.get()->color.getSize();
						memcpy(this->color_buffer, color.GetImageMap(),
								s.height * s.width * 3 * sizeof(XnUInt8));
						timestamp[0] = getTimeStamp();
					}
				}

				if (true == this->has_depth) {
					if (this->depth.IsNewDataAvailable()) {
						this->depth.WaitAndUpdateData();
						boost::mutex::scoped_lock l(this->depth_mutex);
						Size s = this->rgbdi.get()->depth.getSize();
						for (unsigned int i = 0; i < s.height * s.width; ++i) {
							this->depth_buffer[i] = depth.GetDepthMap()[i]
									* 0.001f;
						}
						timestamp[1] = getTimeStamp();
					}
				}

				if (true == this->has_ir) {
					if (this->ir.IsNewDataAvailable()) {
						this->ir.WaitAndUpdateData();
						Size s = this->rgbdi.get()->intensity.getSize();
						boost::mutex::scoped_lock l(this->ir_mutex);
						for (unsigned int i = 0; i < s.height * s.width; ++i) {
							this->ir_buffer[i] =
									static_cast<unsigned char>(ir.GetIRMap()[i]
											* 0.2491234275f);
						}
						timestamp[0] = getTimeStamp();
					}
				}

				if (true == this->recorder.IsValid()) {
					this->recorder.Record();
				}

				emitEvent(this->event_updated);
			}

			current_time = (1000.0 / this->adaptative_framerate)
					- (timer.get() - current_time);

			if (current_time > 0) {
				boost::this_thread::sleep(
						boost::posix_time::milliseconds(current_time));
				this->adaptative_framerate =
						static_cast<float>(this->device_format.depth.framerate_num)
								/ this->device_format.depth.framerate_denom;
			} else {
				this->adaptative_framerate =
						static_cast<float>(this->device_format.depth.framerate_num)
								/ this->device_format.depth.framerate_denom
								- current_time;
			}
		}
		timer.stop();
		this->stream_finished = true;
	}

private:
	void switchColorIR(void) {
		XnStatus nRetVal;
		XnMapOutputMode outputMode;
		bool running = this->stream_running;
		stopCapture();

		if (this->has_color) {
			if (this->color.IsValid()) {

				this->color.Release();
				this->has_color = false;
				this->rgbdi.get()->color.release();

				nRetVal = this->ir.Create(*this->context);
				if (XN_STATUS_OK != nRetVal) {
					Log::add().error("OpenNIGrabber::switchColorIR",
							xnGetStatusString(nRetVal));
				}
				outputMode.nXRes =
						this->device_format.intensity.resolution.width;
				outputMode.nYRes =
						this->device_format.intensity.resolution.height;
				outputMode.nFPS = this->device_format.intensity.framerate_num;
				this->ir.SetMapOutputMode(outputMode);

				this->ir.GetMapOutputMode(outputMode);
				unsigned int width = outputMode.nXRes;
				unsigned int height = outputMode.nYRes;
				this->device_format.intensity.framerate_num = outputMode.nFPS;
				this->device_format.intensity.resolution = Size(width, height);
				this->has_ir = true;

				this->rgbdi.get()->intensity.setSize(Size(width, height));
				delete[] this->ir_buffer;
				this->ir_buffer = new unsigned char[width * height];
			}
		} else {
			if (this->ir.IsValid()) {
				this->ir.Release();
				this->has_ir = false;
				this->rgbdi.get()->intensity.release();

				nRetVal = this->color.Create(*this->context);
				if (XN_STATUS_OK != nRetVal) {
					Log::add().error("OpenNIGrabber::switchColorIR",
							xnGetStatusString(nRetVal));
				}

				outputMode.nXRes = this->device_format.color.resolution.width;
				outputMode.nYRes = this->device_format.color.resolution.height;
				outputMode.nFPS = this->device_format.color.framerate_num;
				this->color.SetMapOutputMode(outputMode);

				this->has_color = true;
				this->color.GetMapOutputMode(outputMode);
				unsigned int width = outputMode.nXRes;
				unsigned int height = outputMode.nYRes;
				this->device_format.color.framerate_num = outputMode.nFPS;
				this->device_format.color.resolution = Size(width, height);

				this->rgbdi.get()->color.setSize(Size(width, height));
				delete[] this->color_buffer;
				this->color_buffer = new unsigned char[width * height * 3];
			}
		}

		this->depth.Release();
		nRetVal = this->depth.Create(*this->context);
		if (XN_STATUS_OK != nRetVal) {
			Log::add().error("OpenNIGrabber::switchColorIR",
					xnGetStatusString(nRetVal));
		}
		outputMode.nXRes = this->device_format.depth.resolution.width;
		outputMode.nYRes = this->device_format.depth.resolution.height;
		outputMode.nFPS = this->device_format.depth.framerate_num;
		this->depth.SetMapOutputMode(outputMode);

		if (running) {
			startCapture();
		}

	}

	void checkDepth(void) {

		XnUInt32 nCount = this->depth.GetSupportedMapOutputModesCount();
		XnMapOutputMode *aModes = new XnMapOutputMode[nCount];
		this->depth.GetSupportedMapOutputModes(aModes, nCount);
		for (unsigned int i = 0; i < nCount; ++i) {
			Size s(aModes[i].nXRes, aModes[i].nYRes);
			std::list<DeviceFormat>::iterator it =
					this->device_format_list.begin();
			bool found = false;
			while (it != this->device_format_list.end()) {
				if (it->depth.resolution == s) {
					bool f_f = false;
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->depth.framerates.begin();
					while (itf != it->depth.framerates.end()) {
						if (itf->first == aModes[i].nFPS) {
							f_f = true;
							break;
						}
						++itf;
					}
					if (f_f == false) {
						it->depth.framerates.push_back(
								std::pair<unsigned int, unsigned int>(
										aModes[i].nFPS, 1));
					}
					found = true;
				}
				++it;
			}
			if (false == found) {
				DeviceFormat df;
				df.depth.resolution = s;
				df.depth.format = GRAY32F;
				df.depth.framerates.push_back(
						std::pair<unsigned int, unsigned int>(aModes[i].nFPS,
								1));
				this->device_format_list.push_back(df);
			}
		}
		delete[] aModes;
	}

	void checkColor(void) {

		if (this->has_color == false) {
			this->color.Create(*this->context);
		}

		XnUInt32 nCount = this->color.GetSupportedMapOutputModesCount();
		XnMapOutputMode *aModes = new XnMapOutputMode[nCount];
		this->color.GetSupportedMapOutputModes(aModes, nCount);
		for (unsigned int i = 0; i < nCount; ++i) {
			bool found = false;
			Size s(aModes[i].nXRes, aModes[i].nYRes);
			std::list<DeviceFormat>::iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end()) {
				if (it->color.resolution == s) {
					bool f_f = false;
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->color.framerates.begin();
					while (itf != it->color.framerates.end()) {
						if (itf->first == aModes[i].nFPS) {
							f_f = true;
							break;
						}
						++itf;
					}
					if (f_f == false) {

						it->color.framerates.push_back(
								std::pair<unsigned int, unsigned int>(
										aModes[i].nFPS, 1));
					}
					found = true;
				}
				++it;
			}
			if (false == found) {
				DeviceFormat df;
				df.color.resolution = s;
				df.color.format = RGB888U;
				df.color.framerates.push_back(
						std::pair<unsigned int, unsigned int>(aModes[i].nFPS,
								1));
				this->device_format_list.push_back(df);
			}

		}
		delete[] aModes;

		if (this->has_color == false) {
			this->color.Release();
		}
	}

	void checkIR(void) {

		if (this->has_ir == false) {
			this->ir.Create(*this->context);
		}

		XnUInt32 nCount = this->ir.GetSupportedMapOutputModesCount();
		XnMapOutputMode *aModes = new XnMapOutputMode[nCount];
		this->ir.GetSupportedMapOutputModes(aModes, nCount);
		for (unsigned int i = 0; i < nCount; ++i) {
			bool found = false;
			Size s(aModes[i].nXRes, aModes[i].nYRes);
			std::list<DeviceFormat>::iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end()) {
				if (it->intensity.resolution == s) {
					bool f_f = false;
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->intensity.framerates.begin();
					while (itf != it->intensity.framerates.end()) {
						if (itf->first == aModes[i].nFPS) {
							f_f = true;
							break;
						}
						++itf;
					}
					if (f_f == false) {
						it->intensity.framerates.push_back(
								std::pair<unsigned int, unsigned int>(
										aModes[i].nFPS, 1));
					}
					found = true;
				}
				++it;
			}
			if (false == found) {
				DeviceFormat df;
				df.intensity.resolution = s;
				df.intensity.format = GRAY8U;
				df.intensity.framerates.push_back(
						std::pair<unsigned int, unsigned int>(aModes[i].nFPS,
								1));
				this->device_format_list.push_back(df);
			}

		}
		delete[] aModes;
		if (this->has_ir == false) {
			this->ir.Release();
		}
	}

	void feedFormat(void) {
		this->device_format_list.clear();

		if (true == this->depth.IsValid()) {
			checkDepth();
		}

		if (true == this->color.IsValid()) {
			checkColor();
		}

		if (true == this->ir.IsValid()) {
			checkIR();
		}

	}

	void generateProperties(void) {
		if (this->color.IsValid() && this->ir.IsValid()) {
			Property p;
			p.type.name = "Infrared";
			p.type.description = "Switch between infrared and color capture";
			p.on_off = false;
			p.info.has_on_off = true;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Zero plane distance";
			p.type.description = "The focal length in mm";
			XnUInt64 value;
			this->depth.GetIntProperty("ZPD", value);
			p.ivalue = value;
			p.info.has_ivalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Plane pixel size";
			p.type.description = "The pixel size in mm";
			XnDouble value;
			this->depth.GetRealProperty("ZPPS", value);
			p.fvalue = value;
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Record";
			p.type.description =
					"Start/stop to save consecutive frames into a ONI file (string)";
			p.info.has_svalue = true;
			p.svalue = "default.oni";
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Is recording";
			p.on_off = false;
			p.info.has_on_off = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
	}

};
// end class OpenNIGrabber

class OpenNIDriver: public Driver {

public:

	OpenNIDriver(void) :
			Driver() {
		this->driver_name = std::string("OpenNI");

		XnStatus nRetVal = XN_STATUS_OK;
		nRetVal = this->context.Init();
		if (XN_STATUS_OK != nRetVal) {
			throw Exception("OpenNIDriver::OpenNIDriver",
					xnGetStatusString(nRetVal));
		}

		XnLicense m_license;
		strcpy(m_license.strVendor, "PrimeSense");
		strcpy(m_license.strKey, "0KOIk2JeIBYClPWVnMoRKn5cdY4=");
		this->context.AddLicense(m_license);

	}

	~OpenNIDriver(void) {
		this->context.Release();
		this->devices.clear();
		this->grabbers.clear();
	}

	bool update(void) {

		this->list_grabbers.clear();
		this->devices.clear();

		XnStatus nRetVal = XN_STATUS_OK;
		xn::NodeInfoList list;
		xn::EnumerationErrors errors;
		nRetVal = this->context.EnumerateProductionTrees(XN_NODE_TYPE_DEVICE,
				NULL, list, &errors);
		if (XN_STATUS_OK != nRetVal) {
			Log::add().error("OpenNIDriver::update",
					xnGetStatusString(nRetVal));
			return false;
		}

		for (xn::NodeInfoList::Iterator it = list.Begin(); it != list.End();
				++it) {
			xn::NodeInfo deviceNodeInfo = *it;
			this->context.CreateProductionTree(deviceNodeInfo);
			const XnProductionNodeDescription& description =
					deviceNodeInfo.GetDescription();
			const XnChar * c = deviceNodeInfo.GetCreationInfo();
			std::string name(
					std::string(description.strVendor) + " "
							+ std::string(description.strName));
			if (c != 0) {
				name += std::string("[") + c + "]";
			}
			this->list_grabbers.push_back(name);
			this->devices[name] = new xn::NodeInfo(deviceNodeInfo);
		}

		std::vector<std::string> to_remove;
		std::map<std::string, GrabberPtr>::const_iterator it_grabbers =
				this->grabbers.begin();
		while (it_grabbers != this->grabbers.end()) {
			if (std::find(this->list_grabbers.begin(),
					this->list_grabbers.end(), it_grabbers->first)
					== this->list_grabbers.end()) {
				to_remove.push_back(it_grabbers->first);
			}
			++it_grabbers;
		}

		std::vector<std::string>::const_iterator it_to_remove =
				to_remove.begin();
		while (it_to_remove != to_remove.end()) {
			this->grabbers.erase(*it_to_remove);
			++it_to_remove;
		}

		to_remove.clear();

		return true;
	}

	GrabberPtr getGrabberFromFile(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			this->grabbers[name].reset(new OpenNIGrabber(&this->context, name));
		}
		return this->grabbers[name];
	}

	GrabberPtr getGrabber(const std::string& name) {

		if (0 >= this->grabbers.count(name)) {
			try {

				this->grabbers[name].reset(
						new OpenNIGrabber(&this->context,
								*this->devices[name]));
			} catch (Exception e) {
				Log::add().error("OpenNIDriver::getGrabber", e.what());
				GrabberPtr p;
				p.reset();
				return p;
			}
		}

		return this->grabbers[name];
	}

private:

	std::map<std::string, GrabberPtr> grabbers;
	std::map<std::string, xn::NodeInfo*> devices;
	xn::Context context;
};
// end class OpenNIDriver

static OpenNIDriver* openni_driver = 0;

extern "C" GEDEON_EXPORT_PLUGIN int getVersion(void) {
	return DRIVER_VERSION;
}

extern "C" GEDEON_EXPORT_PLUGIN Driver* getDriver(void) {
	if (0 == openni_driver) {
		openni_driver = new OpenNIDriver();
	}
	return dynamic_cast<Driver*>(openni_driver);
}

extern "C" GEDEON_EXPORT_PLUGIN void releaseDriver(void) {
	if (0 != openni_driver) {
		delete openni_driver;
		openni_driver = 0;
	}
}

}
// end namespace gedeon

