/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereye.cpp created in 04 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereye.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

// Internal includes
#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/time.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/converter.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/capturedevice/driver.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <map>


#if defined _WIN32 || defined _MSC_VER
#define _WINSOCKAPI_
#include <windows.h>
#include <winsock2.h>
#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")
#include <iphlpapi.h>
#else
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#endif


#include <pcap.h>

using namespace boost::filesystem;

namespace gedeon {

struct Device;

namespace {

static const unsigned int SIZE_MAC_ADDRESS = 6;
static const unsigned int MAX_SIZE_BUF = 1514;
static const unsigned int CAPTURE_TIMEOUT = 1000;

extern "C" GEDEON_EXPORT bool broadcastPacket(Device& device);

extern "C" GEDEON_EXPORT bool getListNetworkDevices(
		std::list<Device*>& list_devices);

extern "C" GEDEON_EXPORT bool getDeviceMACAddress(Device &device);

extern "C" GEDEON_EXPORT std::string packetToString(const char *packet,
		const size_t& max_size, const char& sep = '\0');

} // End anonymous namespace

#include "tigereye_device.cc"

#include "tigereye_grabber.cc"

#include "tigereye_driver.cc"

// List of global functions
namespace {

extern "C" GEDEON_EXPORT bool broadcastPacket(Device& device) {

	char error_buffer[PCAP_ERRBUF_SIZE];
	pcap_t *handle = pcap_open_live(device.name.c_str(), MAX_SIZE_BUF, 1,
			CAPTURE_TIMEOUT, error_buffer);
	if (0 == handle) {
		Log::add().error("broadcastPacket",
				"Couldn't open the device interface. "
						+ std::string(error_buffer));
		return false;
	}

	/*
	 * Filter the packet reception to the only ethernet protocol 0x8040
	 */
	struct bpf_program fp;
	bpf_u_int32 maskp, netp;
	if (-1
			== pcap_lookupnet(device.name.c_str(), &netp, &maskp,
					error_buffer)) {
		Log::add().error("broadcastPacket",
				"Unable to get information from the network device."
						+ std::string(error_buffer));
		return false;
	}
	char* filter = (char*) "ether proto 0x8040";
	if (0 > pcap_compile(handle, &fp, filter, 0, netp)) {
		Log::add().error("broadcastPacket",
				"Unable to compile the packet filter. Check the syntax");
		return false;
	}
	if (-1 == pcap_setfilter(handle, &fp)) {
		Log::add().error("broadcastPacket", "Error while setting the filter");
		return false;
	}

	unsigned char packet[18];
	memset(packet, '\xFF', 6);
	memcpy(packet + 6, device.mac_address.c_str(), 6);
	packet[12] = '\x80';
	packet[13] = '\x40';
	packet[14] = '\x04';
	packet[15] = '\x00';
	packet[16] = '\x00';
	packet[17] = '\x04';

	if (-1 == pcap_sendpacket(handle, packet, 18)) {
		Log::add().error("broadcastPacket", "Couldn't send the packet");
		return false;
	}

	struct pcap_pkthdr header;
	const u_char *received_packet = 0;

	//millisecSleep(100);

	do {
		received_packet = pcap_next(handle, &header);
		if (received_packet != 0) {
			if (header.len == 60
					&& packetToString((char*) received_packet + 12, 2)
							== packetToString("\x80\x40",2)
					&& packetToString((char*) received_packet, 6)
							== packetToString((char*)  device.mac_address.c_str(),6)) {
				device.addTarget(std::string((char*) received_packet + 6, 6));
			}
		}
	} while (0 != received_packet);

	pcap_freecode(&fp);
	pcap_close(handle);

	return true;
}

extern "C" GEDEON_EXPORT bool getListNetworkDevices(
		std::list<Device*>& list_devices) {
	list_devices.clear();
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *alldevsp = 0;
	int ret = pcap_findalldevs(&alldevsp, errbuf);
	if (ret == -1) {
		Log::add().error("getListNetworkDevices",
				std::string("Could not find available network device (")
						+ errbuf + ")");
		return false;
	}
	while (alldevsp != 0) {
		// retain only non-loopback devices and devices with addresses
		if (0 == (alldevsp->flags & PCAP_IF_LOOPBACK)
				&& 0 != alldevsp->addresses) {
			Device* d = new Device();
			d->name = std::string(alldevsp->name);
			if (true == getDeviceMACAddress(*d)) {
				list_devices.push_back(d);
			}
		}
		alldevsp = alldevsp->next;
	}
	
	return true;
}

extern "C" GEDEON_EXPORT std::string packetToString(const char * packet,
		const size_t& max_size, const char& sep) {
	std::stringstream ss;
	for (size_t i = 0; i < max_size; ++i) {
		ss << std::hex << std::uppercase << std::setw(2) << std::setfill('0')
				<< (int) (*(unsigned char*) (&packet[i])) << std::dec;
		if (i < max_size - 1 && sep != '\0')
			ss << sep;
	}
	return ss.str();

}

extern "C" GEDEON_EXPORT bool getDeviceMACAddress(Device &device_local) {
#if defined _WIN32 || defined _MSC_VER
	bool not_found = true;
	ULONG outBufLen = 0;
	PIP_ADAPTER_ADDRESSES pAddresses;
	outBufLen = sizeof (IP_ADAPTER_ADDRESSES);
	pAddresses = (IP_ADAPTER_ADDRESSES *) HeapAlloc(GetProcessHeap(), 0, outBufLen);
	if (GetAdaptersAddresses(AF_UNSPEC, 0, 0, pAddresses, &outBufLen) == ERROR_BUFFER_OVERFLOW) {
		HeapFree(GetProcessHeap(), 0, pAddresses);
		pAddresses = (IP_ADAPTER_ADDRESSES *) HeapAlloc(GetProcessHeap(), 0, outBufLen);
	}
	if (GetAdaptersAddresses(AF_UNSPEC, 0, 0, pAddresses, &outBufLen) != NO_ERROR ) {
		Log::add().error("getDeviceMACAddress","Error getting adapters addresses");
		return false;

	}

	not_found = true;

	while(0 != pAddresses && true == not_found) {
	

		if(device_local.name.find(std::string(pAddresses->AdapterName)) != std::string::npos) {
			char tmp[7];
			for(int i = 0; i < 6; ++i){
				tmp[i] = pAddresses->PhysicalAddress[i];
			}
			tmp[6] = '\0';
			
			std::string address(tmp,6); 
			device_local.mac_address = address;
			not_found = false;
		}
		pAddresses = pAddresses->Next;
	}
	return !not_found;
#else
	int sockfd = socket(PF_INET, SOCK_DGRAM, 0);
	if (0 > sockfd) {
		Log::add().error("getDeviceMACAddress",
				std::string("Call to socket failed: ") + strerror(errno));
		return false;
	}

	struct ifreq ifr;
	strcpy(ifr.ifr_name, device_local.name.c_str());
	if (0 == ioctl(sockfd, SIOCGIFHWADDR, &ifr)) {
		std::string address(ifr.ifr_hwaddr.sa_data);
		device_local.mac_address = address.substr(0, SIZE_MAC_ADDRESS);
		return true;
	}
	Log::add().error("getDeviceMACAddress",
			std::string("Call to ioctl failed: ") + strerror(errno));
	return false;
#endif
}

} // End of global functions

static TigereyeDriver* tigereye_driver = 0;

extern "C" GEDEON_EXPORT_PLUGIN int getVersion(void) {
	return DRIVER_VERSION;
}

extern "C" GEDEON_EXPORT_PLUGIN Driver* getDriver(void) {
	if (0 == tigereye_driver) {
		tigereye_driver = new TigereyeDriver();
	}
	return dynamic_cast<Driver*>(tigereye_driver);
}

extern "C" GEDEON_EXPORT_PLUGIN void releaseDriver(void) {
	if (0 != tigereye_driver) {
		delete tigereye_driver;
		tigereye_driver = 0;
	}
}

} // end namespace gedeon

