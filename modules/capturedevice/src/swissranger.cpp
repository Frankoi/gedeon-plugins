/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * swissranger.cpp created in 05 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * swissranger.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

// Internal includes
#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/converter.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/capturedevice/driver.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <map>
#include <libMesaSR.h>

namespace gedeon {

class SwissRangerGrabber: public Grabber {

private:
	SRCAM camera;
	float * X;
	float * Y;
	float * Z;
	unsigned int width;
	unsigned int height;
	unsigned short* depth_map;
	unsigned short* amplitude_map;
	unsigned short* confidence_map;
	unsigned int timestamp;

	std::string output;
	bool from_file;

	int modulation_frequency;

public:

#ifdef _WIN32
	SwissRangerGrabber(const std::string& filename) :
	Grabber() {
		int num_cams = SR_OpenFile(&(this->camera), filename.c_str());
		if (0 >= num_cams) {
			throw Exception("SwissRangerGrabber::SwissRangerGrabber",
					"Cannot find the SwissRanger camera "
					+ filename);
		}
		char devstr[1024];
		SR_GetDeviceString(this->camera, devstr, 1024);
		this->name = std::string(devstr);

		if (SR_Acquire(this->camera) < 0) {
			throw Exception("SwissRangerGrabber::SwissRangerGrabber",
					"Failed to acquire an image");
		}

		int mode = AM_COR_FIX_PTRN | AM_DENOISE_ANF | AM_CONF_MAP | AM_RESERVED1
		| AM_CONV_GRAY;
		mode |= AM_DENOISE_ANF;
		mode |= AM_MEDIAN;
		SR_SetMode(this->camera, mode);
		SR_SetAutoExposure(this->camera, 255, 0, 0, 0);

		width = SR_GetCols(this->camera);
		height = SR_GetRows(this->camera);

		this->modulation_frequency = SR_GetModulationFrequency(this->camera);

		this->X = new float[width * height];
		this->Y = new float[width * height];
		this->Z = new float[width * height];

		this->depth_map = new unsigned short[width * height];
		this->amplitude_map = new unsigned short[width * height];
		this->confidence_map = new unsigned short[width * height];

		this->rgbdi.reset(new RGBDIImage);

		this->rgbdi.get()->depth.setSize(Size(width, height));
		this->rgbdi.get()->intensity.setSize(Size(width, height));

		this->device_format.depth.resolution.width = width;
		this->device_format.depth.resolution.height = height;
		this->device_format.intensity.resolution.width = width;
		this->device_format.intensity.resolution.height = height;
		this->device_format.depth.framerate_num = 30;
		this->device_format.depth.format = GRAY32F;
		this->device_format.intensity.format = GRAY8U;

		this->from_file = true;

		feedFormat();
		generateProperties();
		generateAuxBuffers();

		this->adaptative_framerate = 30.0f;

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);

	}
#endif

	SwissRangerGrabber(const unsigned int& serial) :
			Grabber() {

		int num_cams = SR_OpenUSB(&(this->camera), serial);
		if (0 >= num_cams) {
			throw Exception("SwissRangerGrabber::SwissRangerGrabber",
					"Cannot find the SwissRanger camera "
							+ IO::numberToString(serial));
		}
		char devstr[1024];
		SR_GetDeviceString(this->camera, devstr, 1024);
		this->name = std::string(devstr);

		if (SR_Acquire(this->camera) < 0) {
			throw Exception("SwissRangerGrabber::SwissRangerGrabber",
					"Failed to acquire an image");
		}

		int mode = AM_COR_FIX_PTRN | AM_DENOISE_ANF | AM_CONF_MAP | AM_RESERVED1
				| AM_CONV_GRAY;
		mode |= AM_DENOISE_ANF;
		mode |= AM_MEDIAN;
		SR_SetMode(this->camera, mode);
		SR_SetAutoExposure(this->camera, 255, 0, 0, 0);

		width = SR_GetCols(this->camera);
		height = SR_GetRows(this->camera);

		this->modulation_frequency = SR_GetModulationFrequency(this->camera);

		this->X = new float[width * height];
		this->Y = new float[width * height];
		this->Z = new float[width * height];

		this->depth_map = new unsigned short[width * height];
		this->amplitude_map = new unsigned short[width * height];
		this->confidence_map = new unsigned short[width * height];

		this->rgbdi.reset(new RGBDIImage);

		this->rgbdi.get()->depth.setSize(Size(width, height));
		this->rgbdi.get()->intensity.setSize(Size(width, height));

		this->device_format.depth.resolution.width = width;
		this->device_format.depth.resolution.height = height;
		this->device_format.intensity.resolution.width = width;
		this->device_format.intensity.resolution.height = height;
		this->device_format.depth.framerate_num = 30;
		this->device_format.depth.format = GRAY32F;
		this->device_format.intensity.format = GRAY8U;

		this->from_file = false;

		feedFormat();
		generateProperties();
		generateAuxBuffers();

		this->adaptative_framerate = 30.0f;

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);

	}

	~SwissRangerGrabber(void) {

		this->stopCapture();
		SR_Close(this->camera);

	}

	void startCapture(void) {
		this->stream_paused = false;
		if (false == this->stream_running) {
			if (0 != this->pthread) {
				this->pthread->interrupt();
				delete this->pthread;
				this->pthread = 0;
			}
			this->pthread = new boost::thread(
					boost::bind(&SwissRangerGrabber::run, this));
			millisecSleep(100);
			emitEvent(this->event_playing);
		}
	}

	RGBDIImagePtr getImage(void) {
		float *depth = this->rgbdi.get()->depth.getData().get();
		{
			this->rgbdi.get()->depth.setTimeStamp(timestamp);
			boost::mutex::scoped_lock l(this->mutex);
			// D_max = lightspeed / (2 *modulation frequency )
			float coeff = (299792458.0f
					/ (getModulationFrequencyf(this->modulation_frequency)
							* 2.0f));
			// depth = value * D_max / 16384
			coeff /= 16384.0f;
			for (unsigned int i = 0; i < width * height; ++i) {
				depth[i] = float((this->depth_map[i] >> 2) * coeff);
			}
		}
		unsigned char *intensity = this->rgbdi.get()->intensity.getData().get();
		{
			this->rgbdi.get()->intensity.setTimeStamp(timestamp);
			boost::mutex::scoped_lock l(this->mutex);
			for (unsigned int i = 0; i < width * height; ++i) {
				intensity[i] = (unsigned char) ((this->amplitude_map[i])
						* 255.0f / 16384.0f);
			}
		}
		{
			std::string s("Vertex map");
			float* p = (float*) this->auxiliary_buffer_map[s].data.get();
			boost::mutex::scoped_lock l(this->mutex);
			for (unsigned int i = 0; i < width * height; ++i) {
				p[i * 3] = X[i];
				p[i * 3 + 1] = Y[i];
				p[i * 3 + 2] = Z[i];
			}
		}
		{
			std::string s("Confidence map");
			unsigned char * p = this->auxiliary_buffer_map[s].data.get();
			boost::mutex::scoped_lock l(this->mutex);
			memcpy(p, this->confidence_map,
					width * height * sizeof(unsigned short));
		}
		return this->rgbdi;
	}

	bool setFormat(const DeviceFormat& df) {

		return true;
	}

	bool setProperty(const Property& property) {

		if (false == this->from_file) {
			if ("Denoising" == property.type.name) {
				int mode = SR_GetMode(this->camera);
				if (property.on_off == false) {
					mode |= AM_DENOISE_ANF;
					mode ^= AM_DENOISE_ANF;
					this->porperties_map[property.type.name].on_off = false;
				} else {
					mode |= AM_DENOISE_ANF;
					this->porperties_map[property.type.name].on_off = true;
				}
				SR_SetMode(this->camera, mode);
				return true;
			}

			if ("Median filter" == property.type.name) {
				int mode = SR_GetMode(this->camera);
				if (property.on_off == false) {
					mode |= AM_MEDIAN;
					mode ^= AM_MEDIAN;
					this->porperties_map[property.type.name].on_off = false;
				} else {
					mode |= AM_MEDIAN;
					this->porperties_map[property.type.name].on_off = true;
				}
				SR_SetMode(this->camera, mode);
				return true;
			}
			if ("Auto exposure" == property.type.name) {
				if (property.on_off == false) {
					SR_SetAutoExposure(this->camera, 255, 0, 0, 0);
					this->porperties_map[property.type.name].on_off = false;
				} else {
					if (this->name.find("SR4000") != std::string::npos) {
						SR_SetAutoExposure(this->camera, 1, 150, 5, 70);
					} else {
						SR_SetAutoExposure(this->camera, 2, 255, 10, 45);
					}
					this->porperties_map[property.type.name].on_off = true;
				}
				return true;
			}
			if ("Integration time" == property.type.name) {
				if (false == property.info.has_ivalue) {
					return false;
				}
				if (property.ivalue
						>= this->porperties_map[property.type.name].info.imin
						&& property.ivalue
								<= this->porperties_map[property.type.name].info.imax) {
					SR_SetIntegrationTime(this->camera,
							static_cast<unsigned char>(property.ivalue));
					this->porperties_map[property.type.name].ivalue =
							static_cast<int>(SR_GetIntegrationTime(this->camera));
					return true;
				}
				return false;
			}
			if ("Amplitude threshold" == property.type.name) {
				if (false == property.info.has_ivalue) {
					return false;
				}
				if (property.ivalue
						>= this->porperties_map[property.type.name].info.imin
						&& property.ivalue
								<= this->porperties_map[property.type.name].info.imax) {
					SR_SetAmplitudeThreshold(this->camera,
							static_cast<unsigned short>(property.ivalue));
					this->porperties_map[property.type.name].ivalue =
							static_cast<int>(SR_GetAmplitudeThreshold(
									this->camera));
					return true;
				}
				return false;
			}
			if ("Modulation frequency" == property.type.name) {
				if (false == property.info.has_ivalue) {
					return false;
				}
				if (property.ivalue
						>= this->porperties_map[property.type.name].info.imin
						&& property.ivalue
								<= this->porperties_map[property.type.name].info.imax) {
					SR_SetModulationFrequency(this->camera,
							static_cast<ModulationFrq>(property.ivalue));
					this->porperties_map[property.type.name].ivalue =
							static_cast<int>(SR_GetModulationFrequency(
									this->camera));
					return true;
				}
				return false;
			}
		}
#ifdef _WIN32
		if ("Record" == property.type.name) {
			if ("" == this->output) {
				if (property.svalue != "") {
					std::string namelower = property.svalue;
					std::string name = property.svalue;
					std::transform(namelower.begin(), namelower.end(),
							namelower.begin(), ::tolower);
					if (false
							== boost::algorithm::ends_with(namelower, ".srs")) {
						name += ".srs";
					}
					this->output = name;
					SR_StreamToFile(this->camera, this->output.c_str(), 0);
					this->porperties_map[property.type.name].svalue = name;
					this->porperties_map["Is recording"].on_off = true;
				} else {
					return false;
				}
			} else {
				SR_StreamToFile(this->camera, this->output, 2);
				this->porperties_map["Is recording"].on_off = false;
				this->output = "";
			}
			return true;
		}
#endif
		return false;
	}

	void run(void) {

		this->stream_finished = false;
		this->stream_running = true;

		Timer timer;
		timer.start();
		long int current_time;

		while (true == this->stream_running) {
			current_time = timer.get();

			if (false == this->stream_paused) {
				int nb_bytes = SR_Acquire(this->camera);
				if (0 < nb_bytes) {
					{
						boost::mutex::scoped_lock l(this->mutex);
						SR_CoordTrfFlt(this->camera, this->X, this->Y, this->Z,
								sizeof(float), sizeof(float), sizeof(float));
						memcpy(this->depth_map,
								(unsigned short *) SR_GetImage(this->camera, 0),
								width * height * sizeof(unsigned short));
						memcpy(this->amplitude_map,
								(unsigned short *) SR_GetImage(this->camera, 1),
								width * height * sizeof(unsigned short));
						memcpy(this->confidence_map,
								(unsigned short *) SR_GetImage(this->camera, 2),
								width * height * sizeof(unsigned short));
						timestamp = getTimeStamp();
					}
					emitEvent(this->event_updated);
				}

				current_time = (1000.0 / this->adaptative_framerate)
						- (timer.get() - current_time);

				if (current_time > 0) {
					boost::this_thread::sleep(
							boost::posix_time::milliseconds(current_time));
					this->adaptative_framerate =
							static_cast<float>(this->device_format.depth.framerate_num)
									/ this->device_format.depth.framerate_denom;
				} else {
					this->adaptative_framerate =
							static_cast<float>(this->device_format.depth.framerate_num)
									/ this->device_format.depth.framerate_denom
									- current_time;
				}

			}

		}

		timer.stop();
		this->stream_finished = true;
	}

private:

	float getModulationFrequencyf(const int& mf) {
		if (mf == MF_30MHz) {
			return 30.0f * 1000000.f;
		}
		if (mf == MF_40MHz) {
			return 40.0f * 1000000.f;
		}
		if (mf == MF_21MHz) {
			return 21.0f * 1000000.f;
		}
		if (mf == MF_20MHz) {
			return 20.0f * 1000000.f;
		}
		if (mf == MF_19MHz) {
			return 19.0f * 1000000.f;
		}
		if (mf == MF_60MHz) {
			return 60.0f * 1000000.f;
		}
		if (mf == MF_15MHz) {
			return 15.0f * 1000000.f;
		}
		if (mf == MF_10MHz) {
			return 10.0f * 1000000.f;
		}
		if (mf == MF_29MHz) {
			return 29.0f * 1000000.f;
		}
		if (mf == MF_31MHz) {
			return 31.0f * 1000000.f;
		}
		if (mf == MF_14_5MHz) {
			return 14.5f * 1000000.f;
		}
		if (mf == MF_15_5MHz) {
			return 15.5f * 1000000.f;
		}

		return 1.0f;
	}

	void feedFormat(void) {
		this->device_format_list.clear();
		DeviceFormat df;
		df.depth.resolution.width = width;
		df.depth.resolution.height = height;
		df.intensity.resolution.width = width;
		df.intensity.resolution.height = height;
		df.depth.framerate_num = 30;
		df.depth.format = GRAY32F;
		df.intensity.format = GRAY8U;
		this->device_format_list.push_back(df);
	}

	void generateProperties(void) {

		if (false == this->from_file) {
			{
				Property p;
				p.type.name = "Denoising";
				p.type.description =
						"This value turns on a hardware-implemented noise filter";
				p.info.has_on_off = true;
				p.info.changeable = true;
				int mode = SR_GetMode(this->camera);
				p.on_off = mode | AM_DENOISE_ANF;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Median filter";
				p.type.description = "This turns on a 3x3 Median filter";
				p.info.has_on_off = true;
				p.info.changeable = true;
				int mode = SR_GetMode(this->camera);
				p.on_off = mode | AM_MEDIAN;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Integration time";
				p.type.description =
						"The integration time is the length of time that the pixels are allowed to collect light. SR400: 0.300ms+(intTime)*0.100 ms ";
				p.info.has_ivalue = true;
				p.info.changeable = true;
				p.info.imin = 0;
				p.info.imax = 255;
				p.ivalue =
						static_cast<int>(SR_GetIntegrationTime(this->camera));
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Modulation frequency";
				p.type.description =
						"0 (SR3k: maximal range 3.75m), 1 (SR3k, SR4k: maximal range 5m), 2 (SR3k: maximal range 7.14m), 3 (SR3k: maximal range 7.5m), 4 (SR3k: maximal range 7.89m), 5 (SR4k: maximal range 2.5m This frequency is for internal testing and should not be used), 6 (SR4k: maximal range 10m This frequency is for internal testing and should not be used), 7 (SR4k: maximal range 15m This frequency is for internal testing and should not be used), 8 (SR4k: maximal range 5.17m), 9 (SR4k: maximal range 4.84m), 10 (SR4k: maximal range 10.34m This frequency is for internal testing and should not be used), 11 (SR4k: maximal range 9.68m This frequency is for internal testing and should not be used)";
				p.info.has_ivalue = true;
				p.info.changeable = true;
				p.info.imin = 0;
				p.info.imax = 11;
				p.ivalue = static_cast<int>(SR_GetModulationFrequency(
						this->camera));
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Auto exposure";
				p.type.description = "";
				p.info.has_on_off = true;
				p.info.changeable = true;
				p.on_off = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Amplitude threshold";
				p.type.description =
						"The parameter amplitude defines a minimum amplitude that needs to be exceeded in order to accept a measurement.";
				p.info.has_ivalue = true;
				p.info.changeable = true;
				p.info.imin = 0;
				p.info.imax = USHRT_MAX;
				p.ivalue = static_cast<int>(SR_GetAmplitudeThreshold(
						this->camera));
				this->porperties_map[p.type.name] = p;
			}
		}
#ifdef _WIN32
		{
			Property p;
			p.type.name = "Record";
			p.type.description =
			"Start/stop to save consecutive frames into a SRS file (string)";
			p.info.has_svalue = true;
			p.svalue = "default.srs";
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Is recording";
			p.on_off = false;
			p.info.has_on_off = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
#endif
	}

	void generateAuxBuffers(void) {
		{
			std::string s("Confidence map");
			AuxiliaryBuffer b;
			b.type.name = s;
			b.size = Size(width, height);
			b.depth = GEDEON_16U;
			b.channels = 1;
			b.data.reset(
					new unsigned char[width * height * sizeof(unsigned short)]);
			this->auxiliary_buffer_map[s] = b;
		}
		{
			std::string s("Vertex map");
			AuxiliaryBuffer b;
			b.type.name = s;
			b.size = Size(width, height);
			b.depth = GEDEON_32F;
			b.channels = 3;
			b.data.reset(new unsigned char[width * height * 3 * sizeof(float)]);
			this->auxiliary_buffer_map[s] = b;
		}
	}

}
;

class SwissRangerDriver: public Driver {

public:

	SwissRangerDriver(void) :
			Driver() {
		this->driver_name = std::string("SwissRanger");
	}

	~SwissRangerDriver(void) {
		this->grabbers.clear();
		this->devices.clear();
	}

	bool update(void) {
		this->list_grabbers.clear();
		this->devices.clear();

		SRCAM cams[16];
		int num_cams = SR_OpenAll(cams, 16, 0, 0xFFFFFFFF);
		if (0 >= num_cams) {
			return true;
		}

		char devstr[1024];
		for (int i = 0; i < num_cams; ++i) {
			SR_GetDeviceString(cams[i], devstr, 1024);
			unsigned int serial = SR_ReadSerial(cams[i]);
			std::string name = std::string(devstr);
			this->list_grabbers.push_back(name);
			this->devices[name] = serial;
			SR_Close(cams[i]);
		}

		/*
		 * Remove no more connected devices
		 */
		std::vector<std::string> to_remove;
		std::map<std::string, GrabberPtr>::const_iterator it_grabbers =
				this->grabbers.begin();
		while (it_grabbers != this->grabbers.end()) {
			if (std::find(this->list_grabbers.begin(),
					this->list_grabbers.end(), it_grabbers->first)
					== this->list_grabbers.end()) {
				to_remove.push_back(it_grabbers->first);
			}
			++it_grabbers;
		}
		std::vector<std::string>::const_iterator it_to_remove =
				to_remove.begin();
		while (it_to_remove != to_remove.end()) {
			this->grabbers.erase(*it_to_remove);
			++it_to_remove;
		}

		to_remove.clear();
		return true;
	}

	GrabberPtr getGrabber(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			if (0 >= this->devices.count(name)) {
				Log::add().error("SwissRangerDriver::getGrabber",
						"This camera was not found (" + name + ")");
				GrabberPtr p;
				p.reset();
				return p;
			}
			try {

				this->grabbers[name].reset(
						new SwissRangerGrabber(this->devices[name]));
			} catch (Exception e) {
				Log::add().error("DepthSenseDriver::getGrabber", e.what());
				GrabberPtr p;
				p.reset();
				return p;
			}
		}

		return this->grabbers[name];
	}

private:

	std::map<std::string, GrabberPtr> grabbers;
	std::map<std::string, unsigned int> devices;

};
// end class DepthSenseDriver

static SwissRangerDriver* swissranger_driver = 0;

extern "C" GEDEON_EXPORT_PLUGIN int getVersion(void) {
	return DRIVER_VERSION;
}

extern "C" GEDEON_EXPORT_PLUGIN Driver* getDriver(void) {
	if (0 == swissranger_driver) {
		swissranger_driver = new SwissRangerDriver();
	}
	return dynamic_cast<Driver*>(swissranger_driver);
}

extern "C" GEDEON_EXPORT_PLUGIN void releaseDriver(void) {
	if (0 != swissranger_driver) {
		delete swissranger_driver;
		swissranger_driver = 0;
	}
}

}
// end namespace gedeon

