/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereye_device.cpp created in 04 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereye_device.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/


struct NetworkData: public EventData {
	NetworkData(const std::string& evname, char* d, const size_t& s) :
			EventData(evname, 0), data(0), size_data(0) {
		if (d != 0 && s != 0) {
			this->data = new unsigned char[s];
			this->size_data = s;
			memcpy(this->data, d, s);
		}
	}
	~NetworkData(void) {
		if (0 != data) {
			delete[] data;
		}
	}
	unsigned char* data;
	size_t size_data;
}; // End struct NetworkData

typedef boost::shared_ptr<NetworkData> NetworkDataPtr;





class Device: public EventSender {

public:
	std::string name;
	std::string mac_address;
	std::list<std::string> list_targets;

private:
	pcap_t *handle;
	boost::thread *pthread;
	bool running_thread;

public:
	Device(void) :
			handle(0), pthread(0), running_thread(false) {
	}

	~Device(void) {
		closeConnection();
		this->list_targets.clear();
	}

	void addTarget(const std::string& target) {
		list_targets.push_back(target);
		addEventName(target);
	}

	void clearTargets(void) {
		std::list<std::string>::iterator it_target = this->list_targets.begin();
		while (this->list_targets.end() != it_target) {
			removeEventName(*it_target);
			++it_target;
		}
		list_targets.clear();
	}

	void showMacAddress(void) const {
		Log::add().info(
				packetToString(this->mac_address.c_str(),
						this->mac_address.length(), ':'));
	}

	bool sendPacket(char* packet, const size_t s){
		if (-1 == pcap_sendpacket(this->handle, (u_char*)packet, s)) {
			std::cerr << "error" << std::endl;
			return false;
		}
		return true;
	}

	bool openConnection(void) {
		
		if (0 != this->handle) {
			return true;
		}

		char error_buffer[PCAP_ERRBUF_SIZE];
		this->handle = pcap_open_live(this->name.c_str(), MAX_SIZE_BUF, 1,
				CAPTURE_TIMEOUT, error_buffer);
		if (0 == handle) {
			Log::add().error("Device::openConnection",
					"Couldn't open the device interface. "
							+ std::string(error_buffer));
			return false;
		}

		/*
		 * Filter the packet reception to the only ethernet protocol 0x8040
		 */
		struct bpf_program fp;
		bpf_u_int32 maskp, netp;
		if (-1
				== pcap_lookupnet(this->name.c_str(), &netp, &maskp,
						error_buffer)) {
			Log::add().error("Device::openConnection",
					"Unable to get information from the network device."
							+ std::string(error_buffer));
			return false;
		}
		char* filter = (char*) "ether proto 0x8040";
		if (0 > pcap_compile(this->handle, &fp, filter, 0, netp)) {
			Log::add().error("Device::openConnection",
					"Unable to compile the packet filter. Check the syntax");
			return false;
		}
		if (-1 == pcap_setfilter(this->handle, &fp)) {
			Log::add().error("Device::openConnection",
					"Error while setting the filter");
			return false;
		}
		pcap_freecode(&fp);




		this->pthread = new boost::thread(boost::bind(&Device::run, this));

		return true;
	}

	void closeConnection(void) {
		if (0 != this->pthread) {
			this->running_thread = false;
			this->pthread->interrupt();
			delete this->pthread;
			this->pthread = 0;
		}
		if (0 != this->handle) {
			pcap_close(this->handle);
			this->handle = 0;
		}
	}
	void run(void) {
		this->running_thread = true;
		struct pcap_pkthdr *header;
		const u_char *pkt_data;
		NetworkDataPtr e;
		while (true == this->running_thread) {
			int ret = pcap_next_ex(this->handle, &header, &pkt_data);
			if (1 == ret) {
				e.reset(
						new NetworkData(std::string((char*) pkt_data+6, 6),
								(char*) pkt_data, header->len));
				emitEvent(e);
			}
		}
	}

}; // End class Device:

