/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereye_driver.cpp created in 04 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereye_driver.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

class TigereyeDriver: public Driver {

public:

	TigereyeDriver(void) :
			Driver() {
		this->driver_name = std::string("tigereye");
	}

	~TigereyeDriver(void) {
		this->grabbers.clear();
		std::list<Device*>::iterator it = this->list_devices.begin();
		while (this->list_devices.end() != it) {
			delete *it;
			++it;
		}
		this->list_devices.clear();
	}

	bool update(void) {

		list_grabbers.clear();

		// Get the list of available interfaces
		getListNetworkDevices(this->list_devices);
		std::list<Device*>::iterator it = this->list_devices.begin();
		while (this->list_devices.end() != it) {

			(*it)->clearTargets();
			broadcastPacket(*(*it));
			if (false == (*it)->list_targets.empty()) {
				std::list<std::string>::iterator it_target =
						(*it)->list_targets.begin();
				while ((*it)->list_targets.end() != it_target) {
					list_grabbers.push_back(
							packetToString(it_target->c_str(),
									it_target->size(), ':'));
					++it_target;
				}
			}
			++it;
		}
		return true;
	}

	GrabberPtr getGrabberFromFile(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			this->grabbers[name].reset(new TigereyeGrabber(name));
		}
		return this->grabbers[name];
	}

	GrabberPtr getGrabber(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			bool found = false;
			std::list<Device*>::iterator it = this->list_devices.begin();
			std::list<std::string>::iterator it_target;
			while (false == found && this->list_devices.end() != it) {
				if (false == (*it)->list_targets.empty()) {
					it_target = (*it)->list_targets.begin();
					while ((*it)->list_targets.end() != it_target
							&& false == found) {
						if (name
								== packetToString(it_target->c_str(),
										it_target->size(), ':')) {
							found = true;
						} else {
							++it_target;
						}
					}
				}
				if (false == found) {
					++it;
				}
			}

			if (true == found) {
				try {
					(*it)->openConnection();
					this->grabbers[name].reset(
							new TigereyeGrabber(*it, *it_target));
				} catch (Exception e) {
					Log::add().error("TigereyeDriver::getGrabber", e.what());
					GrabberPtr p;
					p.reset();
					return p;
				}
			} else {
				throw Exception("TigereyeDriver::getGrabber",
						"Unable to find the device " + name);
			}

		}
		return this->grabbers[name];
	}

	std::map<std::string, GrabberPtr> grabbers;
	std::list<Device*> list_devices;

};
// end class TigereyeDriver
