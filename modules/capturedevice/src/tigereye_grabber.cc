/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * tigereye_grabber.cpp created in 04 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * tigereye_grabber.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

#define SIZE_DWORD 4
#define SIZE_OPCODE 4
#define SIZE_COMMAND SIZE_DWORD*2
#define SIZE_LONG_READ 8

static const unsigned char opcode_write_dma[SIZE_OPCODE] = { '\x0c', '\x00',
		'\x10', '\x02' };
static const unsigned char opcode_command[SIZE_OPCODE] = { '\x0c', '\x02',
		'\x00', '\x02' };
static const unsigned char long_read[SIZE_LONG_READ] = { '\x08', '\x00', '\x00',
		'\x02', '\x00', '\x00', '\x00', '\x10' };
static const unsigned char long_read_reply[SIZE_LONG_READ] = { '\x18', '\x00',
		'\x00', '\x02', '\x00', '\x00', '\x00', '\x10' };

static const unsigned char take_one_image_command[SIZE_COMMAND] = { '\x00',
		'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x01' };
static const unsigned char reset_command[SIZE_COMMAND] = { '\x00', '\x00',
		'\x01', '\x00', '\x00', '\x00', '\x00', '\x00' };
static const unsigned char run_at_rate_command[SIZE_COMMAND] = { '\x00', '\x00',
		'\x00', '\x00', '\x00', '\x01', '\x00', '\x02' };
static const unsigned char stop_command[SIZE_COMMAND] = { '\x00', '\x00',
		'\x00', '\x00', '\x00', '\x00', '\x00', '\x02' };
static const unsigned char update_voltages_command[SIZE_COMMAND] = { '\x00',
		'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x80' };
static const unsigned char NUC_command[SIZE_COMMAND] = { '\x00', '\x02', '\x01',
		'\x12', '\x00', '\x01', '\x00', '\x02' };

static const unsigned char set_framerate_command_ref[SIZE_COMMAND] = { '\x19',
		'\xf9', '\x01', '\x31', '\x19', '\xf9', '\x00', '\x04' };
static const unsigned char set_detector_gain_command_ref[SIZE_COMMAND] = {
		'\x04', '\xeb', '\x01', '\x20', '\x04', '\xeb', '\x00', '\x20' };
static const unsigned char set_amplifier_gain_command_ref[SIZE_COMMAND] = {
		'\x2a', '\x3f', '\x01', '\x22', '\x2a', '\x3f', '\x00', '\x20' };
static const unsigned char set_threshold_filter_command_ref[SIZE_COMMAND] = {
		'\x33', '\x44', '\x01', '\x23', '\x33', '\x44', '\x00', '\x20' };
static const unsigned char set_threshold_level_command_ref[SIZE_COMMAND] = {
		'\xf6', '\xb9', '\x01', '\x2f', '\xf6', '\xb9', '\x00', '\x20' };

static const unsigned int MAX_SIZE_BYTES = 128 * 128 * 4;

static const unsigned int seq_header_size = 1024;
static const unsigned int seq_trailer_size = 512;
static const unsigned int seq_frame_size = 66960;
static const unsigned int seq_frame_data_size = 65540;

class TigereyeGrabber: public Grabber {

private:
	boost::signals2::connection connection_getdata;
	char header[14];
	char write_packet[34];
	Device* device;
	bool confirmation_packet;
	unsigned char grabbed_image[128 * 128 * 4 * 2 + 4]; // Size of the data is 128*128 and each pixel is 4 bytes
	unsigned char config_data[28 * 2];
	unsigned char pending_data[1420 * 2];
	unsigned char current_buffer;
	long long unsigned timestamp;
	float intensity_raw[128 * 128];
	float depth_raw[128 * 128];
	float depth[128 * 128];
	unsigned char intensity[128 * 128];

	std::vector<boost::filesystem::path> file_list_local;
	std::ifstream input_seq_file;
	unsigned nb_frame_seq;

	std::ofstream* outfile;
	int nb_images;
	bool ready;
	bool from_file;

	float framerate;

	boost::mutex mutex_export_txt;

public:

	TigereyeGrabber(const std::string& source) :
			Grabber(), device(0), confirmation_packet(false), current_buffer(0), ready(
					false), from_file(true) {

		this->stream_finished = true;
		this->name = source;
		generateFormats();
		generateProperties(false);

		std::string source_lowercase = source;
		boost::algorithm::to_lower(source_lowercase);
		this->nb_frame_seq = 0;

		// expect txt files
		if (true == is_directory(source)) {
			path my_dir(source);
			directory_iterator end;
			for (directory_iterator iter(my_dir); iter != end; ++iter) {
				source_lowercase = iter->path().string();
				boost::algorithm::to_lower(source_lowercase);
				if (true
						== boost::algorithm::ends_with(source_lowercase, ".txt")
						&& true == is_regular_file(*iter)) {
					this->file_list_local.push_back(*iter);
				}
			}
			this->nb_frame_seq = this->file_list_local.size();
			if (this->file_list_local.size() > 0) {
				sort(this->file_list_local.begin(),
						this->file_list_local.end());
			} else {
				throw Exception("TigereyeGrabber::TigereyeGrabber",
						"No valid file found in the directory " + source);
			}

			this->porperties_map["Framerate"].info.changeable = true;
			this->porperties_map["Framerate"].fvalue = 10.0f;
			this->adaptative_framerate = 10.0f;
			this->framerate = 10.0f;
		}
		// expect seq file
		else {
			if (true == boost::algorithm::ends_with(source_lowercase, ".seq")
					&& true == is_regular_file(source)) {
				// Read the number of frame
				this->input_seq_file.open(source.c_str(),
						std::ifstream::in | std::ifstream::binary);
				if (false == input_seq_file.is_open()) {
					throw Exception("TigereyeGrabber::TigereyeGrabber",
							"Could not open the file " + source);
				}
				this->input_seq_file.seekg(-456, std::ios_base::end);
				if (false == this->input_seq_file.good()) {
					throw Exception("TigereyeGrabber::TigereyeGrabber",
							"Invalid input file " + source);
				}
				this->input_seq_file.read((char*) &(this->nb_frame_seq), 4);
				this->input_seq_file.seekg(0, std::ios_base::beg);

				char header_[seq_header_size];
				this->input_seq_file.read(header_, seq_header_size);
				if (this->input_seq_file.gcount() != seq_header_size) {
					throw Exception("TigereyeGrabber::TigereyeGrabber",
							"Invalid input file " + source);
				}
				if (header_[0x02] != '\x48' || header_[0x03] != '\x42'
						|| header_[0x200] != '\x04' || header_[0x208] != '\x80'
						|| header_[0x20c] != '\x80' || header_[0x228] != '\x24'
						|| header_[0x22c] != '\x24' || header_[0x240] != '\x01'
						|| header_[0x291] != '\x04' || header_[0x295] != '\x03'
						|| header_[0x2f9] != '\x04') {
					throw Exception("TigereyeGrabber::TigereyeGrabber",
							"Invalid input file " + source);
				}
			} else {
				throw Exception("TigereyeGrabber::TigereyeGrabber",
						"Invalid file " + source);
			}
		}

		generateAuxBuffers();

		this->ready = true;

		this->rgbdi.reset(new RGBDIImage);
		this->rgbdi->intensity.setSize(Size(128, 128));
		this->rgbdi->depth.setSize(Size(128, 128));

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);
	}

	TigereyeGrabber(Device* d, const std::string& name) :
			Grabber(), device(d), confirmation_packet(false), current_buffer(0), ready(
					false), from_file(false) {

		generateFormats();
		generateProperties(true);

		this->name = packetToString(name.c_str(), name.length(), ':');

		if (false
				== this->device->connect(name,
						boost::bind(&TigereyeGrabber::receiveData, this, _1),
						this->connection_getdata)) {
			Log::add().error("TigereyeGrabber::TigereyeGrabber",
					"Cannot connect to the event to receive data");
		}

		memcpy(this->header, name.c_str(), 6);
		memcpy(this->header + 6, this->device->mac_address.c_str(), 6);
		this->header[12] = '\x80';
		this->header[13] = '\x40';

		/*
		 * Connect to the target camera
		 */
		memset(this->write_packet, 0, 34);
		memcpy(this->write_packet, this->header, 14);
		// Write operation
		this->write_packet[14] = '\x0c';
		this->write_packet[15] = '\x00';
		this->write_packet[16] = '\x10';
		this->write_packet[17] = '\x02';

		// MAC address
		memcpy(this->write_packet + 18, this->device->mac_address.c_str() + 2,
				4);
		memcpy(this->write_packet + 24, this->device->mac_address.c_str(), 2);
		// Long read operation
		this->write_packet[26] = '\x08';
		this->write_packet[27] = '\x00';
		this->write_packet[28] = '\x00';
		this->write_packet[29] = '\x02';
		this->write_packet[30] = '\x00';
		this->write_packet[31] = '\x00';
		this->write_packet[32] = '\x00';
		this->write_packet[33] = '\x10';

		this->device->sendPacket(this->write_packet, 34);
		millisecSleep(2000);
		if (false == getConfirmation()) {
			throw Exception("TigereyeGrabber::TigereyeGrabber",
					"Connection not confirmed");
		}

		memset(this->write_packet, 0, 34);
		memcpy(this->write_packet, this->header, 14);
		// Write operation
		this->write_packet[14] = '\x0c';
		this->write_packet[15] = '\x02';
		this->write_packet[16] = '\x00';
		this->write_packet[17] = '\x02';
		// Capture one frame
		this->write_packet[25] = '\x01';
		this->write_packet[26] = '\x08';
		this->write_packet[27] = '\x00';
		this->write_packet[28] = '\x00';
		this->write_packet[29] = '\x04';
		this->write_packet[30] = '\x00';
		this->write_packet[31] = '\x00';
		this->write_packet[32] = '\x02';
		this->write_packet[33] = '\x00';

		// Need at least three calls to synchronize
		for (unsigned int i = 0; i < 3; ++i) {
			this->device->sendPacket(this->write_packet, 34);
			if (false == getConfirmation()) {
				throw Exception("TigereyeGrabber::TigereyeGrabber",
						"Take one picture not confirmed");
			}
			millisecSleep(30);
		}

		this->ready = true;

		generateAuxBuffers();

		this->rgbdi.reset(new RGBDIImage);
		this->rgbdi->intensity.setSize(Size(128, 128));
		this->rgbdi->depth.setSize(Size(128, 128));

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);

	}

	~TigereyeGrabber(void) {
		this->stopCapture();
		closeSeqFile(&outfile, nb_images);
		if (this->input_seq_file.is_open()) {
			this->input_seq_file.close();
		}
		this->file_list_local.clear();
		millisecSleep(1000);

	}

	bool setProperty(const Property& property) {
		if (false == this->ready) {
			return false;
		}
		return updateProperty(property);
	}

	bool setFormat(const DeviceFormat& dr) {
		Log::add().error("TigereyeGrabber::setFormat",
				"Cannot change the frame format");
		return false;
	}

	void startCapture(void) {
		if (true == this->ready) {
			this->stream_paused = false;
			if (false == this->stream_running) {
				this->stream_running = true;
				if (true == this->from_file) {
					if (0 != this->pthread) {
						this->pthread->interrupt();
						delete this->pthread;
						this->pthread = 0;
					}
					this->pthread = new boost::thread(
							boost::bind(&TigereyeGrabber::run, this));
					millisecSleep(200);
				} else {
					unsigned char packet[34];
					memset(packet, 0, 34);
					memcpy(packet, this->header, 14);
					packet[14] = '\x0c';
					packet[15] = '\x02';
					packet[16] = '\x00';
					packet[17] = '\x02';
					packet[23] = '\x01';
					packet[25] = '\x02';
					this->device->sendPacket((char*) packet, 34);
				}
				emitEvent(this->event_playing);
			}
		}
	}

	void stopCapture(void) {
		if (true == this->ready) {
			if (true == this->stream_running) {
				this->stream_running = false;

				if (true == this->from_file) {
					while (false == this->stream_finished) {
						millisecSleep(40);
					}
					if (0 != this->pthread) {
						this->pthread->interrupt();
						delete this->pthread;
						this->pthread = 0;
					}
				} else {
					unsigned char packet[34];
					memset(packet, 0, 34);
					memcpy(packet, this->header, 14);
					packet[14] = '\x0c';
					packet[15] = '\x02';
					packet[16] = '\x00';
					packet[17] = '\x02';
					packet[25] = '\x02';
					this->device->sendPacket((char*) packet, 34);
				}

				emitEvent(this->event_stopped);
			}
		}
	}

	RGBDIImagePtr getImage(void) {
		unsigned int local_size = 128 * 128;
		float *depth_ = this->rgbdi.get()->depth.getData().get();
		unsigned char *intensity_ =
				this->rgbdi.get()->intensity.getData().get();
		{
			this->rgbdi.get()->depth.setTimeStamp(this->timestamp);
			this->rgbdi.get()->intensity.setTimeStamp(this->timestamp);

			float * depth_raw_buffer = reinterpret_cast<float*>(this->auxiliary_buffer_map["Raw depth"].data.get());
			float * intensity_raw_buffer = reinterpret_cast<float*>(this->auxiliary_buffer_map["Raw intensity"].data.get());

			boost::mutex::scoped_lock l(this->mutex);
			memcpy(depth_, this->depth, local_size * sizeof(float));
			memcpy(intensity_, this->intensity, local_size);
			memcpy(depth_raw_buffer, this->depth_raw, local_size * sizeof(float));
			memcpy(intensity_raw_buffer, this->intensity_raw, local_size * sizeof(float));
		}
		return this->rgbdi;
	}

private:

	void run(void) {
		this->stream_finished = false;

		unsigned int frame_counter = 0;

		unsigned char frame_data[seq_frame_size];

		Timer timer;
		timer.start();
		long int current_time;

		while (true == this->stream_running) {
			current_time = timer.get();
			if (true == this->input_seq_file.is_open()) {
				this->input_seq_file.seekg(
						seq_header_size + frame_counter * seq_frame_size,
						std::ios_base::beg);
				this->input_seq_file.read((char*) frame_data, seq_frame_size);
				memcpy(config_data,
						frame_data + seq_frame_data_size + 0x00d6 - 0x5a, 2); // detector gain
				memcpy(config_data + 2,
						frame_data + seq_frame_data_size + 0x00de - 0x5a, 2); // amplifier gain
				memcpy(config_data + 4,
						frame_data + seq_frame_data_size + 0x00e2 - 0x5a, 2); // threshold filter
				memcpy(config_data + 6,
						frame_data + seq_frame_data_size + 0x0112 - 0x5a, 2); // threshold level
				memcpy(config_data + 8,
						frame_data + seq_frame_data_size + 0x0158 - 0x5a, 18); // camera status
				memcpy(config_data + 26,
						frame_data + seq_frame_data_size + 0x011a - 0x5a, 2); // framerate
				convert(frame_data, config_data,
						frame_data + seq_frame_data_size);
			} else {
				loadFrameTXT(file_list_local[frame_counter].string());
			}

			++frame_counter;
			if (this->nb_frame_seq <= frame_counter) {
				frame_counter = 0;
			}

			emitEvent(this->event_updated);

			current_time = (1000.0 / this->adaptative_framerate)
					- (timer.get() - current_time);

			if (current_time > 0) {
				boost::this_thread::sleep(
						boost::posix_time::milliseconds(current_time));
				this->adaptative_framerate = this->framerate;
			} else {
				this->adaptative_framerate = this->framerate - current_time;
			}

		}

		this->stream_finished = true;
	}

	unsigned short int getAddress(const unsigned char * const data) {
		//Get address: Two bytes at position 14 + (8 - 2)
		unsigned short int address = *(data + 20) << 8;
		address |= *(data + 21);
		return address;
	}

	bool getConfirmation(void) {
		unsigned int waiter = 0;
		while (false == this->confirmation_packet && 50 > waiter) {
			millisecSleep(5);
			++waiter;
		}
		if (false == this->confirmation_packet) {
			return false;
		}
		this->confirmation_packet = false;
		return true;
	}

	void getIntensityDepth(const unsigned char * packet, float& intensity,
			float& depth) {

		int range = 0;
		range |= packet[3] << 8;
		range |= packet[2];
		range = range >> 2;

		int fractional_feet = 0;
		fractional_feet |= ((packet[2] & 3) << 8);
		fractional_feet |= packet[1];
		fractional_feet = fractional_feet >> 4;
		depth = static_cast<float>(range)
				+ static_cast<float>(fractional_feet) / 64.0f;
		//depth = Converter::feetToMeters(depth); //feet to meters

		int intensity_local = 0;
		intensity_local |= ((packet[1] & 15) << 8);
		intensity_local |= packet[0];
		intensity = static_cast<float>(intensity_local); // / 4095.0f * 255.0f); // [0-4095] to [0-255]
	}

	std::ofstream* startSeqFile(const std::string& filename, int& counter) {
		std::ofstream* ofs = new std::ofstream(filename.c_str(),
				std::ofstream::out | std::ofstream::binary);
		if (false == ofs->is_open()) {
			Log::add().error("TigereyeGrabber::startSeqFile",
					"Output filename " + filename + " does not exist");
			return 0;
		}

		// Add the header to the file
		char header_[1024];
		memset(header_, 0, 1024);
		header_[0x02] = '\x48';
		header_[0x03] = '\x42';
		int address = 0x08;
		header_[address++] = '\x5f';
		header_[address++] = '\xef';
		header_[address++] = '\xa6';
		header_[address++] = '\x3f';
		header_[address++] = '\x66';
		header_[address++] = '\xda';
		header_[address++] = '\xfe';
		header_[address++] = '\x3f';
		header_[address++] = '\xa1';
		header_[address++] = '\x84';
		header_[address++] = '\x39';
		header_[address++] = '\x3f';
		address = 0x24;
		header_[address++] = '\x86';
		header_[address++] = '\x38';
		header_[address++] = '\xba';
		header_[address++] = '\x3f';
		header_[address++] = '\xac';
		header_[address++] = '\xff';
		header_[address++] = '\xbf';
		header_[address++] = '\x3f';
		header_[0x200] = '\x04';
		header_[0x208] = '\x80';
		header_[0x20c] = '\x80';
		address = 0x210;
		header_[address++] = '\xdd';
		header_[address++] = '\x49';
		header_[address++] = '\xc9';
		header_[address++] = '\x42';
		header_[address++] = '\x44';
		header_[address++] = '\x64';
		header_[address++] = '\x12';
		header_[address++] = '\x41';
		header_[0x228] = '\x24';
		header_[0x22c] = '\x24';
		address = 0x232;
		header_[address++] = '\x90';
		header_[address++] = '\x41';
		header_[address++] = '\xb4';
		header_[address++] = '\x1d';
		header_[address++] = '\x3d';
		header_[address++] = '\x4b';
		header_[0x240] = '\x01';
		header_[0x291] = '\x04';
		header_[0x295] = '\x03';
		header_[0x2f9] = '\x04';

		counter = 0;

		ofs->write(header_, 1024);
		this->porperties_map["Is recording"].on_off = true;
		return ofs;
	}

	bool appendFrameSeqFile(std::ofstream** stream, unsigned char* frame,
			unsigned char* data, int& counter) {
		if (stream == 0 || *stream == 0 || false == (*stream)->is_open()) {
			return false;
		}

		(*stream)->write((char*) frame, 65540);
		(*stream)->write((char*) data, 1420);

		counter++;

		return true;
	}

	bool closeSeqFile(std::ofstream** stream, const unsigned int nb_frames) {
		if (stream == 0 || *stream == 0 || false == (*stream)->is_open()) {
			return false;
		}

		char trailer[512];
		memset(trailer, 0, 512);
		trailer[0x00] = '\x04';
		trailer[0x08] = '\x80';
		trailer[0x0c] = '\x80';
		int address = 0x10;
		trailer[address++] = '\xdd';
		trailer[address++] = '\x49';
		trailer[address++] = '\xc9';
		trailer[address++] = '\x42';
		trailer[address++] = '\x44';
		trailer[address++] = '\x64';
		trailer[address++] = '\x12';
		trailer[address++] = '\x41';
		trailer[0x28] = '\x24';
		trailer[0x2c] = '\x24';
		address = 0x32;
		trailer[address++] = '\x90';
		trailer[address++] = '\x41';
		trailer[address++] = '\xb4';
		trailer[address++] = '\x1d';
		trailer[address++] = '\x3d';
		trailer[address++] = '\x4b';
		trailer[0x91] = '\x04';
		trailer[0x95] = '\x03';
		trailer[0xf9] = '\x04';
		trailer[0x38] = nb_frames & '\xFF';
		trailer[0x39] = (nb_frames >> 8) & '\xFF';
		trailer[0x3a] = (nb_frames >> 16) & '\xFF';
		trailer[0x3b] = (nb_frames >> 24) & '\xFF';
		(*stream)->write(trailer, 512);

		(*stream)->close();
		delete *stream;
		*stream = 0;

		this->porperties_map["Is recording"].on_off = false;
		return true;
	}

	void convert(unsigned char *image, unsigned char* config,
			unsigned char* data) {
		unsigned int shift = 0;

		{
			boost::mutex::scoped_lock l2(this->mutex_export_txt);
			boost::mutex::scoped_lock l(this->mutex);
			appendFrameSeqFile(&outfile, image, data, nb_images);
			for (unsigned int i = 0; i < 128; ++i) {
				for (unsigned int j = 0; j < 128; ++j) {
					int pos = (127 - i) * 128 + j;
					getIntensityDepth(image + (i * 128 + j + 1) * 4,
							this->intensity_raw[pos], this->depth_raw[pos]);
					this->depth[pos] = 0.3048f * this->depth_raw[pos]; // feet to meter
					this->intensity[pos] = 0.062271062f
							* this->intensity_raw[pos]; // clamp to max intensity (4095)

					this->timestamp = getTimeStamp();
				}
			}
		}

		emitEvent(this->event_updated);

		if (0 < shift) {
			shift = 26;
		}

		unsigned short value = 0;

		value = config[1] << 8;
		value |= config[0];
		this->porperties_map["Detector gain"].fvalue = (static_cast<float>(value
				& 0x0FFF) * 0.00122 * 28.0f) - 3.0f;
		value = config[3] << 8;
		value |= config[2];
		this->porperties_map["Amplifier gain"].fvalue = static_cast<float>(value
				& 0x0FFF) * 0.00061f;

		value = config[5] << 8;
		value |= config[4];
		this->porperties_map["Threshold filter"].fvalue =
				static_cast<float>(value & 0x0FFF) * 0.00061f;

		value = config[7] << 8;
		value |= config[6];
		this->porperties_map["Threshold level"].fvalue =
				static_cast<float>(value & 0x0FFF) * 0.00061f;

		value = config[9] << 8;
		value |= config[8];
		this->porperties_map["Laser drive"].fvalue = static_cast<float>(value
				& 0x7FFF) * 0.02f * 0.002857143f;

		value = config[11] << 8;
		value |= config[10];
		float tmp = static_cast<float>(value & 0x0FFF);
		float V = tmp * 0.00122f;
		float R = (V * 4990.0f) / (5.0f - V);
		this->porperties_map["Sensor temperature"].fvalue =
				(1.0f
						/ (1.2874e-3 + 2.3573e-4 * log(R)
								+ 9.5053e-8 * (pow(log(R), 3)))) - 273.15;

		value = config[13] << 8;
		value |= config[12];
		tmp = static_cast<float>(value & 0x0FFF);
		V = tmp * 0.00122f;
		R = (V * 4990.0f) / (5.0f - V);
		this->porperties_map["Laser temperature"].fvalue =
				(1.0f
						/ (1.2874e-3 + 2.3573e-4 * log(R)
								+ 9.5053e-8 * (pow(log(R), 3)))) - 273.15;

		value = config[15] << 8;
		value |= config[14];
		this->porperties_map["Sensor voltage"].fvalue = static_cast<float>(value
				& 0x0FFF) * 0.00244f;

		value = config[17] << 8;
		value |= config[16];
		tmp = static_cast<float>(value & 0x0FFF);
		V = tmp * 0.00122f;
		R = (V * 4990.0f) / (5.0f - V);
		this->porperties_map["Sensor heat sink temperature"].fvalue =
				(1.0f
						/ (1.2874e-3 + 2.3573e-4 * log(R)
								+ 9.5053e-8 * (pow(log(R), 3)))) - 273.15;

		value = config[19] << 8;
		value |= config[18];
		tmp = static_cast<float>(value & 0x0FFF);
		V = tmp * 0.00122f;
		R = (V * 4990.0f) / (5.0f - V);
		this->porperties_map["Laser heat sink temperature"].fvalue =
				(1.0f
						/ (1.2874e-3 + 2.3573e-4 * log(R)
								+ 9.5053e-8 * (pow(log(R), 3)))) - 273.15;

		value = config[21] << 8;
		value |= config[20];
		tmp = static_cast<float>(value & 0x0FFF);
		V = tmp * 0.00122f;
		R = (V * 4990.0f) / (5.0f - V);
		this->porperties_map["Laser diod temperature"].fvalue =
				(1.0f
						/ (1.2874e-3 + 2.3573e-4 * log(R)
								+ 9.5053e-8 * (pow(log(R), 3)))) - 273.15;

		value = config[23] << 8;
		value |= config[22];
		this->porperties_map["Laser capacity voltage"].fvalue =
				static_cast<float>(value & 0x0FFF) * 0.0244f;

		value = config[25] << 8;
		value |= config[24];
		this->porperties_map["Process board temperature"].fvalue =
				(static_cast<float>(value & 0x0FFF) * 0.00122f - 0.5f) * 100.0f;

		value = config[27] << 8;
		value |= config[26];
		float framerate = 1.0f / (value * 0.00001504f);
		this->porperties_map["Framerate"].fvalue = framerate;
		this->adaptative_framerate = framerate;
		this->framerate = framerate;
	}

	void receiveData(EventDataWPtr e) {
		EventDataPtr evptr = e.lock();
		try {
			NetworkData * ndata = dynamic_cast<NetworkData*>(evptr.get());
			// Confirmation message
			if (60 == ndata->size_data) {
				this->confirmation_packet = true;
			} else {
				if (1510 == ndata->size_data) {
					unsigned int shift_image = 128 * 128 * 4
							* this->current_buffer;
					unsigned int shift_config = 26 * this->current_buffer;
					unsigned short int address = getAddress(ndata->data);
					if (address != 0x3ff0) {
						memcpy(grabbed_image + shift_image + 4 * address,
								ndata->data + 22, 1488);
					} else {
						memcpy(grabbed_image + shift_image + 4 * address,
								ndata->data + 22, 68);
						memcpy(config_data + shift_config, ndata->data + 0x00d6,
								2); // detector gain
						memcpy(config_data + shift_config + 2,
								ndata->data + 0x00de, 2); // amplifier gain
						memcpy(config_data + shift_config + 4,
								ndata->data + 0x00e2, 2); // threshold filter
						memcpy(config_data + shift_config + 6,
								ndata->data + 0x0112, 2); // threshold level
						memcpy(config_data + shift_config + 8,
								ndata->data + 0x0158, 18); // camera status
						memcpy(config_data + shift_config + 26,
								ndata->data + 0x011a, 2); // framerate
						memcpy(pending_data + 1420 * this->current_buffer,
								ndata->data + 0x5a, 1420);
						if (false == this->stream_paused) {
							boost::thread thread_convert(
									boost::bind(&TigereyeGrabber::convert, this,
											this->grabbed_image + shift_image,
											this->config_data + shift_config,
											pending_data
													+ 1420
															* this->current_buffer));
						}
						this->current_buffer == 0 ?
								this->current_buffer = 1 :
								this->current_buffer = 0;
					}
				}
			}
		} catch (std::bad_cast e) {
			Log::add().error("TigereyeGrabber::receiveData",
					"Wrong type of data received");
		}

	}

	bool setFramerate(const float& value) {

		if (0 == this->device) {
			this->porperties_map["Framerate"].fvalue = value;
			this->adaptative_framerate = value;
			this->framerate = value;
			return true;
		}

		unsigned char packet[34];
		memset(packet, 0, 34);
		memcpy(packet, this->header, 14);
		// Write operation
		packet[14] = '\x0c';
		packet[15] = '\x02';
		packet[16] = '\x00';
		packet[17] = '\x02';

		unsigned short int hex_value =
				static_cast<unsigned short int>(1000000.0f / (value * 15.04));

		packet[18] = packet[22] = (hex_value >> 8) & 0xFF;
		packet[19] = packet[23] = (hex_value) & 0xFF;
		packet[20] = '\x01';
		packet[21] = '\x31';
		packet[24] = '\x00';
		packet[25] = '\x04';

		bool pause_status = this->stream_paused;
		this->stream_paused = true;

		millisecSleep(100);

		this->device->sendPacket((char*) packet, 34);

		millisecSleep(200);
		this->stream_paused = pause_status;
		return true;

	}

	bool setNUC(void) {
		unsigned char packet[34];
		memset(packet, 0, 34);
		memcpy(packet, this->header, 14);
		// Write operation
		packet[14] = '\x0c';
		packet[15] = '\x02';
		packet[16] = '\x00';
		packet[17] = '\x02';

		packet[18] = '\x00';
		packet[19] = '\x02';
		packet[20] = '\x01';
		packet[21] = '\x12';

		packet[22] = '\x00';
		packet[23] = '\x01';
		packet[24] = '\x00';
		packet[25] = '\x02';

		bool pause_status = this->stream_paused;
		this->stream_paused = true;

		millisecSleep(100);

		this->device->sendPacket((char*) packet, 34);

		millisecSleep(200);
		this->stream_paused = pause_status;
		return true;

	}

	void waitReady(void) {
		secSleep(15);
		this->ready = true;
		emitEvent(this->event_ready);
	}

	bool reset(void) {
		unsigned char packet[34];
		memset(packet, 0, 34);
		memcpy(packet, this->header, 14);
		// Write operation
		packet[14] = '\x0c';
		packet[15] = '\x02';
		packet[16] = '\x00';
		packet[17] = '\x02';
		packet[20] = '\x01';

		stopCapture();
		millisecSleep(100);

		this->device->sendPacket((char*) packet, 34);
		this->ready = false;
		boost::thread thread_convert(
				boost::bind(&TigereyeGrabber::waitReady, this));

		return true;

	}

	bool setDetectorGain(const float& value) {
		unsigned char packet[34];
		memset(packet, 0, 34);
		memcpy(packet, this->header, 14);
		// Write operation
		packet[14] = '\x0c';
		packet[15] = '\x02';
		packet[16] = '\x00';
		packet[17] = '\x02';

		unsigned short int hex_value = static_cast<unsigned short int>((value
				+ 3.0f) / 28.0f / 0.00122f);

		packet[18] = packet[22] = (hex_value >> 8) & 0xFF;
		packet[19] = packet[23] = (hex_value) & 0xFF;
		packet[20] = '\x01';
		packet[21] = packet[25] = '\x20';

		this->device->sendPacket((char*) packet, 34);

		return true;

	}

	bool setAmplifierGain(const float& value) {
		unsigned char packet[34];
		memset(packet, 0, 34);
		memcpy(packet, this->header, 14);
		// Write operation
		packet[14] = '\x0c';
		packet[15] = '\x02';
		packet[16] = '\x00';
		packet[17] = '\x02';

		unsigned short int hex_value = static_cast<unsigned short int>((value
				/ 0.00061f) + 0x2000);

		packet[18] = packet[22] = (hex_value >> 8) & 0xFF;
		packet[19] = packet[23] = (hex_value) & 0xFF;
		packet[20] = '\x01';
		packet[21] = '\x22';
		packet[24] = '\x00';
		packet[25] = '\x20';

		this->device->sendPacket((char*) packet, 34);

		return true;
	}

	bool setThresholdFilter(const float& value) {
		unsigned char packet[34];
		memset(packet, 0, 34);
		memcpy(packet, this->header, 14);
		// Write operation
		packet[14] = '\x0c';
		packet[15] = '\x02';
		packet[16] = '\x00';
		packet[17] = '\x02';

		unsigned short int hex_value = static_cast<unsigned short int>((value
				/ 0.00061f) + 0x3000);

		packet[18] = packet[22] = (hex_value >> 8) & 0xFF;
		packet[19] = packet[23] = (hex_value) & 0xFF;
		packet[20] = '\x01';
		packet[21] = '\x23';
		packet[24] = '\x00';
		packet[25] = '\x20';

		this->device->sendPacket((char*) packet, 34);

		return true;
	}

	bool setThresholdLevel(const float& value) {
		unsigned char packet[34];
		memset(packet, 0, 34);
		memcpy(packet, this->header, 14);
		// Write operation
		packet[14] = '\x0c';
		packet[15] = '\x02';
		packet[16] = '\x00';
		packet[17] = '\x02';

		unsigned short int hex_value = static_cast<unsigned short int>((value
				/ 0.00061f) + 0xF000);

		packet[18] = packet[22] = (hex_value >> 8) & 0xFF;
		packet[19] = packet[23] = (hex_value) & 0xFF;
		packet[20] = '\x01';
		packet[21] = '\x2f';
		packet[24] = '\x00';
		packet[25] = '\x20';

		this->device->sendPacket((char*) packet, 34);

		return true;
	}

	bool saveFrameTXT(const std::string& filename) {
		std::ofstream ofs;
		ofs.open(filename.c_str(), std::ofstream::out);
		if (false == ofs.is_open()) {
			Log::add().error("TigereyeGrabber::saveFrameTXT",
					"Input file " + filename + " is not valid");
			return false;
		}
		std::ostringstream output_txt;
		{
			boost::mutex::scoped_lock l(this->mutex_export_txt);
			for (unsigned int i = 0; i < 128; ++i) {
				for (unsigned int j = 0; j < 128; ++j) {
					int pos = (127 - i) * 128 + j;
					output_txt << j << " " << i << " " << this->depth_raw[pos]
							<< " " << (int) this->intensity_raw[pos] << "\r\n";
				}
			}
		}
		std::string s = output_txt.str();
		ofs.write(s.c_str(), s.size());
		ofs.close();
		return true;
	}

	bool loadFrameTXT(const std::string& filename) {
		std::ifstream ifs;
		ifs.open(filename.c_str(), std::ofstream::in);
		if (false == ifs.is_open()) {
			Log::add().error("TigereyeGrabber::loadFrameTXT",
					"Input file " + filename + " is not valid");
			return false;
		}

		{
			boost::mutex::scoped_lock l(this->mutex_export_txt);

			memset(this->depth_raw, 0, 128 * 128 * sizeof(float));
			memset(this->intensity_raw, 0, 128 * 128 * sizeof(float));
			unsigned int i, j;
			float depth, intensity;
			while (ifs.good()) {
				ifs >> i >> j >> depth >> intensity;
				int pos = (127 - j) * 128 + i;
				this->depth_raw[pos] = depth;
				this->intensity_raw[pos] = intensity;
				this->depth[pos] = 0.3048f * this->depth_raw[pos]; // feet to meter
				this->intensity[pos] = 0.062271062f * this->intensity_raw[pos];
			}
		}

		ifs.close();
		return true;
	}

	bool triggerRecord(const std::string& filename) {
		if (this->outfile > 0) {
			return closeSeqFile(&this->outfile, this->nb_images);
		} else {
			this->outfile = startSeqFile(filename, this->nb_images);
			return this->outfile;
		}
	}

	void generateFormats(void) {

		DeviceFormat df;
		df.depth.resolution.height = 128;
		df.depth.resolution.width = 128;
		df.depth.format = GRAY32F;
		df.intensity.resolution.height = 128;
		df.intensity.resolution.width = 128;
		df.intensity.format = GRAY8U;
		this->device_format_list.push_back(df);

		this->device_format.depth.resolution = df.depth.resolution;
		this->device_format.depth.format = df.depth.format;
		this->device_format.intensity.resolution = df.intensity.resolution;
		this->device_format.intensity.format = df.intensity.format;

	}

	void generateProperties(bool all_properties) {

		{
			Property p;
			p.type.name = "Record";
			p.type.description =
					"Start/stop to save consecutive frames into a SEQ file (string)";
			p.info.has_svalue = true;
			p.svalue = "default.seq";
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Capture";
			p.type.description = "Save a single frame into a TXT file (string)";
			p.info.has_svalue = true;
			p.svalue = "default.txt";
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}

		if (true == all_properties) {
			{
				Property p;
				p.type.name = "NUC";
				p.type.description =
						"Trigger the Non-Uniformity Correction (NUC) command to alleviate effects of noise";
				p.info.has_on_off = true;
				p.on_off = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Reset";
				p.type.description =
						"Trigger the reset command (The system need 15 seconds to be ready)";
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Detector gain";
				p.info.has_fvalue = true;
				p.info.fmin = 16.335f;
				p.info.fmax = 44.0042f;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Amplifier gain";
				p.info.has_fvalue = true;
				p.info.fmin = 1.4f;
				p.info.fmax = 1.483f;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Threshold filter";
				p.info.has_fvalue = true;
				p.info.fmin = 1.0f;
				p.info.fmax = 0.883f;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Threshold level";
				p.info.has_fvalue = true;
				p.info.fmin = 0.4f;
				p.info.fmax = 0.883f;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Framerate";
				p.info.has_fvalue = true;
				p.info.fmin = 1.01f;
				p.info.fmax = 20.0f;
				this->adaptative_framerate = 15.0f;
				this->framerate = 15.0f;
				p.fvalue = 15.0f;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
		} else {

			{
				Property p;
				p.type.name = "Framerate";
				p.info.has_fvalue = true;
				p.info.fmin = 1.01f;
				p.info.fmax = 20.0f;
				this->adaptative_framerate = 15.0f;
				p.fvalue = 15.0f;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
		}
		{
			Property p;
			p.type.name = "Laser drive";
			p.type.description =
					"Frequency of the laser trigger (Should be less than 100%)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Sensor temperature";
			p.type.description =
					"Temperature of the sensor (Should be between 20 and 35 degrees)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Laser temperature";
			p.type.description =
					"Temperature of the laser (Should be +/-2 degrees of the setup number)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Sensor voltage";
			p.type.description =
					"Voltage of the sensor (Should be close to 6.3V)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Sensor heat sink temperature";
			p.type.description =
					"Temperature of the sensor heat sink (Should be less than 40 degrees)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Laser heat sink temperature";
			p.type.description =
					"Temperature of the laser heat sink (Should be less than 40 degrees)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Laser diod temperature";
			p.type.description =
					"Temperature of the laser diod (Should follow laser temperature)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Laser capacity voltage";
			p.type.description =
					"Capacity voltage of the laser (Should be between 16V and 32V)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Process board temperature";
			p.type.description =
					"Temperature of the process board (Should be between 0 and 60 degrees)";
			p.info.has_fvalue = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Is recording";
			p.on_off = false;
			p.info.has_on_off = true;
			p.info.changeable = false;
			this->porperties_map[p.type.name] = p;
		}

	}

	bool updateProperty(const Property& p) {
		if (this->porperties_map.count(p.type.name) <= 0) {
			return false;
		}

		Property local_p;

		if ("Framerate" == p.type.name) {
			local_p = this->porperties_map[p.type.name];
			if (p.fvalue >= local_p.info.fmin && p.fvalue <= local_p.info.fmax
					&& true == local_p.info.changeable) {
				return setFramerate(p.fvalue);
			}
			return false;
		}
		if ("Detector gain" == p.type.name) {
			local_p = this->porperties_map[p.type.name];
			if (p.fvalue >= local_p.info.fmin
					&& p.fvalue <= local_p.info.fmax) {
				return setDetectorGain(p.fvalue);
			}
			return false;
		}
		if ("Amplifier gain" == p.type.name) {
			local_p = this->porperties_map[p.type.name];
			if (p.fvalue >= local_p.info.fmin
					&& p.fvalue <= local_p.info.fmax) {
				return setAmplifierGain(p.fvalue);
			}
			return false;
		}
		if ("Threshold filter" == p.type.name) {
			local_p = this->porperties_map[p.type.name];
			if (p.fvalue >= local_p.info.fmin
					&& p.fvalue <= local_p.info.fmax) {
				return setThresholdFilter(p.fvalue);
			}
			return false;
		}
		if ("Threshold level" == p.type.name) {
			local_p = this->porperties_map[p.type.name];
			if (p.fvalue >= local_p.info.fmin
					&& p.fvalue <= local_p.info.fmax) {
				return setThresholdLevel(p.fvalue);
			}
			return false;
		}
		if ("Reset" == p.type.name) {
			return reset();
		}
		if ("NUC" == p.type.name) {
			return setNUC();
		}
		if ("Record" == p.type.name) {
			return triggerRecord(p.svalue);
		}
		if ("Capture" == p.type.name) {
			return saveFrameTXT(p.svalue);
		}

		return false;
	}

	void generateAuxBuffers(void) {

		{
			AuxiliaryBuffer b;
			b.type.name = "Raw intensity";
			b.size = Size(128, 128);
			b.depth = GEDEON_32F;
			b.channels = 1;
			b.data.reset(new unsigned char[128 * 128 * sizeof(float)]);
			this->auxiliary_buffer_map[b.type.name] = b;
		}
		{
			AuxiliaryBuffer b;
			b.type.name = "Raw depth";
			b.size = Size(128, 128);
			b.depth = GEDEON_32F;
			b.channels = 1;
			b.data.reset(new unsigned char[128 * 128 * sizeof(float)]);
			this->auxiliary_buffer_map[b.type.name] = b;
		}

	}

};
// End class TigereyeGrabber
