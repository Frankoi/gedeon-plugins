/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * depthsense.cpp created in 05 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * depthsense.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

// Internal includes
#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/capturedevice/driver.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string.hpp>
#include <map>
#include <OpenNI.h>
#include <PS1080.h>

namespace gedeon {

class ColorCallback: public openni::VideoStream::NewFrameListener,
		public EventSender {
public:
	ColorCallback(unsigned char * d, boost::mutex &m, bool &p) :
			data(d), mutex(m), paused(p) {
		this->ev.reset(new EventData("openni2_color_updated"));
		addEventName("openni2_color_updated");
	}
	~ColorCallback(void) {
		this->ev.reset();

	}
	void onNewFrame(openni::VideoStream& stream) {
		if (false == this->paused) {
			stream.readFrame(&m_frame);
			{
				boost::mutex::scoped_lock l(this->mutex);
				memcpy(data, (const unsigned char*) m_frame.getData(),
						m_frame.getDataSize());
			}
			emitEvent(this->ev);
			millisecSleep(10);
		}
	}

public:
	openni::VideoFrameRef m_frame;
	unsigned char *data;
	boost::mutex &mutex;
	bool &paused;
	EventDataPtr ev;

};

class DepthCallback: public openni::VideoStream::NewFrameListener,
		public EventSender {
public:
	DepthCallback(float * d, boost::mutex &m, bool &p) :
			data(d), mutex(m), paused(p) {
		this->ev.reset(new EventData("openni2_depth_updated"));
		addEventName("openni2_depth_updated");
	}
	void onNewFrame(openni::VideoStream& stream) {
		if (false == this->paused) {
			stream.readFrame(&m_frame);
			const openni::DepthPixel * ref_src =
					(const openni::DepthPixel*) m_frame.getData();
			{
				boost::mutex::scoped_lock l(this->mutex);
				for (int i = 0; i < m_frame.getWidth() * m_frame.getHeight();
						++i) {
					data[i] = static_cast<float>(ref_src[i] * 0.001f);
				}
			}
			emitEvent(this->ev);
			millisecSleep(10);
		}

	}
public:
	openni::VideoFrameRef m_frame;
	float *data;
	boost::mutex &mutex;
	bool &paused;
	EventDataPtr ev;
};

class IRCallback: public openni::VideoStream::NewFrameListener,
		public EventSender {
public:
	IRCallback(unsigned char * d, boost::mutex &m, bool &p) :
			data(d), mutex(m), paused(p) {
		this->ev.reset(new EventData("openni2_color_updated"));
		addEventName("openni2_color_updated");
	}
	void onNewFrame(openni::VideoStream& stream) {
		if (false == this->paused) {
			stream.readFrame(&m_frame);
			const openni::Grayscale16Pixel * ref_src =
					(const openni::Grayscale16Pixel*) m_frame.getData();
			boost::mutex::scoped_lock l(this->mutex);
			for (int i = 0; i < m_frame.getWidth() * m_frame.getHeight(); ++i) {
				data[i] = static_cast<unsigned char>(ref_src[i] * 0.2491234275);
			}
			emitEvent(this->ev);
			millisecSleep(10);
		}

	}
public:
	openni::VideoFrameRef m_frame;
	unsigned char *data;
	boost::mutex &mutex;
	bool &paused;
	EventDataPtr ev;
};

class OpenNI2Grabber: public Grabber {

private:
	openni::Device device;
	openni::VideoStream depth;
	openni::VideoStream color;
	openni::VideoStream ir;

	bool has_depth;
	bool has_color;
	bool has_ir;
	bool is_file;

	ColorCallback* color_callback;
	DepthCallback* depth_callback;
	IRCallback* ir_callback;

	unsigned char * color_buffer;
	unsigned char * ir_buffer;
	float * depth_buffer;

	boost::mutex color_mutex;
	boost::mutex depth_mutex;
	boost::mutex ir_mutex;

	bool received[2];
	boost::signals2::connection c_color;
	boost::signals2::connection c_depth;
	boost::signals2::connection c_ir;

	unsigned int timestamp[2];

	openni::Recorder recorder;

public:

	OpenNI2Grabber(const std::string& file) :
			Grabber() {

		this->name = file;
		this->received[0] = false;
		this->received[1] = false;
		this->received[2] = false;
		this->color_callback = 0;
		this->depth_callback = 0;
		this->ir_callback = 0;
		this->color_buffer = 0;
		this->depth_buffer = 0;
		this->ir_buffer = 0;

		this->has_depth = false;
		this->has_color = false;
		this->has_ir = false;

		this->is_file = true;

		this->rgbdi.reset(new RGBDIImage);

		openni::Status rc = this->device.open(file.c_str());
		if (openni::STATUS_OK != rc) {
			throw Exception("OpenNI2Grabber::OpenNI2Grabber",
					std::string("Could not open the file ") + file + " "
							+ openni::OpenNI::getExtendedError());
		}

		rc = this->depth.create(this->device, openni::SENSOR_DEPTH);
		if (openni::STATUS_OK == rc && this->depth.isValid()) {
			this->has_depth = true;
			openni::VideoMode vm = this->depth.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->depth.setMirroringEnabled(!this->depth.getMirroringEnabled());
			this->rgbdi.get()->depth.setSize(Size(width, height));
			this->depth_buffer = new float[width * height];
			this->depth_callback = new DepthCallback(this->depth_buffer,
					this->depth_mutex, this->stream_paused);
			this->depth_callback->connect("openni2_depth_updated",
					boost::bind(&OpenNI2Grabber::notify, this, _1), c_depth);
			this->device_format.depth.resolution = Size(width, height);
			this->device_format.depth.format = GRAY32F;
			this->device_format.depth.framerate_num = vm.getFps();
		}

		rc = this->color.create(this->device, openni::SENSOR_COLOR);
		if (openni::STATUS_OK == rc && this->color.isValid()) {
			has_color = true;
			openni::VideoMode vm = this->color.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->color.setMirroringEnabled(!this->color.getMirroringEnabled());
			this->rgbdi.get()->color.setSize(Size(width, height));
			this->color_buffer = new unsigned char[width * height * 3];
			this->color_callback = new ColorCallback(this->color_buffer,
					this->color_mutex, this->stream_paused);
			this->color_callback->connect("openni2_color_updated",
					boost::bind(&OpenNI2Grabber::notify, this, _1), c_color);
			this->device_format.color.resolution = Size(width, height);
			this->device_format.color.format = RGB888U;
			this->device_format.color.framerate_num = vm.getFps();
		}

		rc = this->ir.create(this->device, openni::SENSOR_IR);
		if (openni::STATUS_OK == rc && this->ir.isValid()) {
			has_ir = true;
			openni::VideoMode vm = this->ir.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->ir.setMirroringEnabled(!this->ir.getMirroringEnabled());
			this->rgbdi.get()->intensity.setSize(Size(width, height));
			this->ir_buffer = new unsigned char[width * height];
			this->ir_callback = new IRCallback(this->ir_buffer, this->ir_mutex,
					this->stream_paused);
			this->ir_callback->connect("openni2_color_updated",
					boost::bind(&OpenNI2Grabber::notify, this, _1), c_ir);
			this->device_format.intensity.resolution = Size(width, height);
			this->device_format.intensity.format = GRAY8U;
			this->device_format.intensity.framerate_num = vm.getFps();
		}

		generateProperties();
		feedFormat();

		this->device.setImageRegistrationMode(
				openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);
	}

	OpenNI2Grabber(const openni::DeviceInfo& di) :
			Grabber() {

		this->device.open(di.getUri());
		this->name = di.getUri();
		this->received[0] = false;
		this->received[1] = false;
		this->received[2] = false;
		this->color_callback = 0;
		this->depth_callback = 0;
		this->ir_callback = 0;
		this->color_buffer = 0;
		this->depth_buffer = 0;
		this->ir_buffer = 0;

		this->is_file = false;

		this->has_depth = false;
		this->has_color = false;
		this->has_ir = false;

		this->rgbdi.reset(new RGBDIImage);

		openni::Status rc;

		rc = this->depth.create(this->device, openni::SENSOR_DEPTH);
		if (openni::STATUS_OK == rc && this->depth.isValid()) {
			this->has_depth = true;
			openni::VideoMode vm = this->depth.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->depth.setMirroringEnabled(!this->depth.getMirroringEnabled());
			this->rgbdi.get()->depth.setSize(Size(width, height));
			this->depth_buffer = new float[width * height];
			this->depth_callback = new DepthCallback(this->depth_buffer,
					this->depth_mutex, this->stream_paused);
			this->depth_callback->connect("openni2_depth_updated",
					boost::bind(&OpenNI2Grabber::notify, this, _1), c_depth);
			this->device_format.depth.resolution = Size(width, height);
			this->device_format.depth.format = GRAY32F;
			this->device_format.depth.framerate_num = vm.getFps();
		}

		rc = this->color.create(this->device, openni::SENSOR_COLOR);
		if (openni::STATUS_OK == rc && this->color.isValid()) {
			has_color = true;
			openni::VideoMode vm = this->color.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->color.setMirroringEnabled(!this->color.getMirroringEnabled());
			this->rgbdi.get()->color.setSize(Size(width, height));
			this->color_buffer = new unsigned char[width * height * 3];
			this->color_callback = new ColorCallback(this->color_buffer,
					this->color_mutex, this->stream_paused);
			this->color_callback->connect("openni2_color_updated",
					boost::bind(&OpenNI2Grabber::notify, this, _1), c_color);
			this->device_format.color.resolution = Size(width, height);
			this->device_format.color.format = RGB888U;
			this->device_format.color.framerate_num = vm.getFps();
		}

		rc = this->ir.create(this->device, openni::SENSOR_IR);
		if (openni::STATUS_OK == rc && this->ir.isValid()) {
			openni::VideoMode vm = this->ir.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->ir.setMirroringEnabled(!this->ir.getMirroringEnabled());
			this->ir_buffer = new unsigned char[width * height];
			this->ir_callback = new IRCallback(this->ir_buffer, this->ir_mutex,
					this->stream_paused);
			this->ir_callback->connect("openni2_color_updated",
					boost::bind(&OpenNI2Grabber::notify, this, _1), c_ir);
			this->device_format.intensity.resolution = Size(width, height);
			this->device_format.intensity.format = GRAY8U;
			this->device_format.intensity.framerate_num = vm.getFps();
		}

		generateProperties();
		feedFormat();

		this->device.setImageRegistrationMode(
				openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR);

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);
	}

	~OpenNI2Grabber(void) {
		this->stopCapture();

		this->c_color.disconnect();
		this->color.destroy();
		delete this->color_callback;
		delete[] this->color_buffer;

		this->c_depth.disconnect();
		this->depth.destroy();
		delete this->depth_callback;
		delete[] this->depth_buffer;

		this->c_ir.disconnect();
		this->ir.destroy();
		delete this->ir_callback;
		delete[] this->ir_buffer;

		this->device.close();
		this->rgbdi.reset();
	}

	void startCapture(void) {
		this->stream_paused = false;
		if (false == this->stream_running) {
			if (true == this->has_color) {
				this->color.addNewFrameListener(this->color_callback);
				openni::Status rc = this->color.start();
				if (openni::STATUS_OK != rc) {
					Log::add().error("OpenNI2Grabber::startCapture",
							std::string("Could not start the color stream ")
									+ openni::OpenNI::getExtendedError());
				}
			}
			if (true == this->has_depth) {
				this->depth.addNewFrameListener(this->depth_callback);
				openni::Status rc = this->depth.start();
				if (openni::STATUS_OK != rc) {
					Log::add().error("OpenNI2Grabber::startCapture",
							std::string("Could not start the depth stream ")
									+ openni::OpenNI::getExtendedError());
				}
			}
			if (true == this->has_ir) {
				this->ir.addNewFrameListener(this->ir_callback);
				openni::Status rc = this->ir.start();
				if (openni::STATUS_OK != rc) {
					Log::add().error("OpenNI2Grabber::startCapture",
							std::string("Could not start the ir stream ")
									+ openni::OpenNI::getExtendedError());
				}
			}
			this->stream_running = true;
			millisecSleep(100);
			emitEvent(this->event_playing);
		}
	}

	void stopCapture(void) {
		if (true == this->stream_running) {
			this->stream_running = false;
			if (true == this->has_color) {
				this->color.stop();
				this->color.removeNewFrameListener(this->color_callback);
			}
			if (true == this->has_depth) {
				this->depth.stop();
				this->depth.removeNewFrameListener(this->depth_callback);
			}
			if (true == this->has_ir) {
				this->ir.stop();
				this->ir.removeNewFrameListener(this->ir_callback);
			}
			if (this->recorder.isValid()) {
				this->recorder.stop();
				this->recorder.destroy();
				this->porperties_map["Is recording"].on_off = false;
			}

			emitEvent(this->event_stopped);
		}
	}

	RGBDIImagePtr getImage(void) {
		if (true == this->has_color) {
			unsigned char *color = this->rgbdi.get()->color.getData().get();
			{
				size_t buffer_size = this->rgbdi.get()->color.getSize().width
						* this->rgbdi.get()->color.getSize().height * 3;
				this->rgbdi.get()->color.setTimeStamp(timestamp[1]);
				boost::mutex::scoped_lock l(this->color_mutex);
				memcpy(color, this->color_buffer, buffer_size);
			}
		}

		if (true == this->has_ir) {
			unsigned char *infrared =
					this->rgbdi.get()->intensity.getData().get();
			{
				size_t buffer_size =
						this->rgbdi.get()->intensity.getSize().width
								* this->rgbdi.get()->intensity.getSize().height;
				this->rgbdi.get()->intensity.setTimeStamp(timestamp[1]);
				boost::mutex::scoped_lock l(this->color_mutex);
				memcpy(infrared, this->ir_buffer, buffer_size);
			}
		}

		if (true == this->has_depth) {
			float *depth = this->rgbdi.get()->depth.getData().get();
			{
				size_t buffer_size = this->rgbdi.get()->depth.getSize().width
						* this->rgbdi.get()->depth.getSize().height
						* sizeof(float);
				this->rgbdi.get()->depth.setTimeStamp(timestamp[0]);
				boost::mutex::scoped_lock l(this->depth_mutex);
				memcpy(depth, this->depth_buffer, buffer_size);
			}
		}

		return this->rgbdi;
	}

	bool setFormat(const DeviceFormat& df) {
		bool running = this->stream_running;
		stopCapture();
		if (df.color.resolution.width > 0 && df.color.resolution.height > 0
				&& 0 < df.color.framerate_num) {
			bool found = false;
			std::list<DeviceFormat>::const_iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end()) {
				if (it->color.resolution == df.color.resolution) {
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->color.framerates.begin();
					while (itf != it->color.framerates.end()) {
						if (itf->first == df.color.framerate_num) {
							found = true;
							break;
						}
						++itf;
					}
					break;
				}
				++it;
			}
			this->device_format.color.resolution = df.color.resolution;
			this->device_format.color.framerate_num = df.color.framerate_num;
			if (true == found) {
				openni::VideoMode vm = this->color.getVideoMode();
				vm.setResolution(df.color.resolution.width,
						df.color.resolution.height);
				vm.setFps(df.color.framerate_num);
				this->color.setVideoMode(vm);
				this->rgbdi.get()->color.setSize(
						Size(df.color.resolution.width,
								df.color.resolution.height));
				delete[] this->color_buffer;
				this->color_buffer = new unsigned char[df.color.resolution.width
						* df.color.resolution.height * 3];
				this->color_callback->data = this->color_buffer;
			}

		}

		if (df.depth.resolution.width > 0 && df.depth.resolution.height > 0
				&& 0 < df.depth.framerate_num) {
			bool found = false;
			std::list<DeviceFormat>::const_iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end()) {
				if (it->depth.resolution == df.depth.resolution) {
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->depth.framerates.begin();
					while (itf != it->depth.framerates.end()) {
						if (itf->first == df.depth.framerate_num) {
							found = true;
							break;
						}
						++itf;
					}
					break;
				}
				++it;
			}
			this->device_format.depth.resolution = df.depth.resolution;
			this->device_format.depth.framerate_num = df.depth.framerate_num;
			if (true == found) {
				openni::VideoMode vm = this->depth.getVideoMode();
				vm.setResolution(df.depth.resolution.width,
						df.depth.resolution.height);
				vm.setFps(df.depth.framerate_num);
				this->depth.setVideoMode(vm);
				this->rgbdi.get()->depth.setSize(
						Size(df.depth.resolution.width,
								df.depth.resolution.height));
				delete[] this->depth_buffer;
				this->depth_buffer = new float[df.depth.resolution.width
						* df.depth.resolution.height];
				this->depth_callback->data = this->depth_buffer;
			}

		}

		if (df.intensity.resolution.width > 0
				&& df.intensity.resolution.height > 0
				&& 0 < df.intensity.framerate_num) {
			bool found = false;
			std::list<DeviceFormat>::const_iterator it =
					this->device_format_list.begin();
			while (it != this->device_format_list.end()) {
				if (it->intensity.resolution == df.intensity.resolution) {
					std::list<std::pair<unsigned int, unsigned int> >::const_iterator itf =
							it->intensity.framerates.begin();
					while (itf != it->intensity.framerates.end()) {
						if (itf->first == df.intensity.framerate_num) {
							found = true;
							break;
						}
						++itf;
					}
					break;
				}
				++it;
			}
			this->device_format.intensity.resolution = df.intensity.resolution;
			this->device_format.intensity.framerate_num =
					df.intensity.framerate_num;
			if (true == found) {
				openni::VideoMode vm = this->ir.getVideoMode();
				vm.setResolution(df.intensity.resolution.width,
						df.intensity.resolution.height);
				vm.setFps(df.intensity.framerate_num);
				this->ir.setVideoMode(vm);
				this->rgbdi.get()->intensity.setSize(
						Size(df.intensity.resolution.width,
								df.intensity.resolution.height));
				delete[] this->ir_buffer;
				this->ir_buffer =
						new unsigned char[df.intensity.resolution.width
								* df.intensity.resolution.height];
				this->ir_callback->data = this->ir_buffer;
			}

		}

		if (running) {
			startCapture();
		}

		return true;
	}

	bool setProperty(const Property& property) {

		if ("Infrared" == property.type.name) {
			switchColorIR();
			return true;
		}

		if ("Auto exposure" == property.type.name) {
			openni::CameraSettings* cs = this->color.getCameraSettings();
			cs->setAutoExposureEnabled(property.on_off);
			this->porperties_map[property.type.name].on_off = property.on_off;
			return true;
		}

		if ("Auto white balance" == property.type.name) {
			openni::CameraSettings* cs = this->color.getCameraSettings();
			cs->setAutoExposureEnabled(property.on_off);
			this->porperties_map[property.type.name].on_off = property.on_off;
			return true;
		}

		if ("Close range" == property.type.name) {
			this->depth.setProperty(XN_STREAM_PROPERTY_CLOSE_RANGE,
					property.on_off);
			this->porperties_map[property.type.name].on_off = property.on_off;
			return true;
		}

		if ("Hole filter" == property.type.name) {
			this->depth.setProperty(XN_STREAM_PROPERTY_HOLE_FILTER,
					property.on_off);
			this->porperties_map[property.type.name].on_off = property.on_off;
			return true;
		}

		if ("Record" == property.type.name) {
			if (false == this->recorder.isValid()) {
				if (property.svalue != "") {
					std::string namelower = property.svalue;
					std::string name = property.svalue;
					std::transform(namelower.begin(), namelower.end(),
							namelower.begin(), ::tolower);
					if (false
							== boost::algorithm::ends_with(namelower, ".oni")) {
						name += ".oni";
					}
					this->recorder.create(name.c_str());
					this->recorder.attach(this->depth);
					if(this->has_color){
						this->recorder.attach(this->color);
					}else{
						this->recorder.attach(this->ir);
					}
					this->recorder.start();
					this->porperties_map[property.type.name].svalue = name;
					this->porperties_map["Is recording"].on_off = true;
				} else {
					return false;
				}
			} else {
				this->recorder.stop();
				this->recorder.destroy();
				this->porperties_map["Is recording"].on_off = false;
			}
			return true;
		}

		return false;
	}

	void notify(EventDataWPtr e) {
		if (EventDataPtr evptr = e.lock()) {
			if (evptr->name == "openni2_depth_updated") {
				this->timestamp[0] = getTimeStamp();
				boost::mutex::scoped_lock l(this->mutex);
				this->received[0] = true;
			} else {
				this->timestamp[1] = getTimeStamp();
				boost::mutex::scoped_lock l(this->mutex);
				this->received[1] = true;
			}

			if (this->received[0] && this->received[1]) {
				emitEvent(this->event_updated);
				boost::mutex::scoped_lock l(this->mutex);
				this->received[0] = false;
				this->received[1] = false;
			}
		}
	}

private:
	void switchColorIR(void) {
		bool running = this->stream_running;
		stopCapture();
		if(this->recorder.isValid()){
			this->recorder.stop();
			this->recorder.destroy();
			this->porperties_map["Is recording"].on_off = false;
		}
		if (this->has_color) {
			this->has_color = false;
			this->has_ir = true;
			openni::VideoMode vm = this->ir.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->rgbdi.get()->intensity.setSize(Size(width, height));
			this->rgbdi.get()->color.release();
		} else {
			this->has_ir = false;
			this->has_color = true;
			openni::VideoMode vm = this->color.getVideoMode();
			int width = vm.getResolutionX();
			int height = vm.getResolutionY();
			this->rgbdi.get()->color.setSize(Size(width, height));
			this->rgbdi.get()->intensity.release();
		}

		if (running) {
			startCapture();
		}

	}

	void feedFormat(void) {
		this->device_format_list.clear();
		{
			const openni::SensorInfo& sid = this->depth.getSensorInfo();
			const openni::Array<openni::VideoMode>& vmad =
					sid.getSupportedVideoModes();
			for (int i = 0; i < vmad.getSize(); ++i) {
				if (vmad[i].getPixelFormat()
						== openni::PIXEL_FORMAT_DEPTH_1_MM) {
					Size s(vmad[i].getResolutionX(), vmad[i].getResolutionY());
					std::list<DeviceFormat>::iterator it =
							this->device_format_list.begin();
					bool found = false;
					while (it != this->device_format_list.end()) {
						if (it->depth.resolution == s) {
							it->depth.framerates.push_back(
									std::pair<unsigned int, unsigned int>(
											vmad[i].getFps(), 1));
							found = true;
						}
						++it;
					}
					if (false == found) {
						DeviceFormat df;
						df.depth.resolution = s;
						df.depth.format = GRAY32F;
						df.depth.framerates.push_back(
								std::pair<unsigned int, unsigned int>(
										vmad[i].getFps(), 1));
						this->device_format_list.push_back(df);
					}
				}
			}
		}
		{
			const openni::SensorInfo& sic = this->color.getSensorInfo();
			const openni::Array<openni::VideoMode>& vmac =
					sic.getSupportedVideoModes();
			for (int i = 0; i < vmac.getSize(); ++i) {
				if (vmac[i].getPixelFormat() == openni::PIXEL_FORMAT_RGB888) {
					Size s(vmac[i].getResolutionX(), vmac[i].getResolutionY());
					std::list<DeviceFormat>::iterator it =
							this->device_format_list.begin();
					bool found = false;
					while (it != this->device_format_list.end()) {
						if (it->color.resolution == s) {
							it->color.framerates.push_back(
									std::pair<unsigned int, unsigned int>(
											vmac[i].getFps(), 1));
							found = true;
						}
						++it;
					}
					if (false == found) {
						DeviceFormat df;
						df.color.resolution = s;
						df.color.format = RGB888U;
						df.color.framerates.push_back(
								std::pair<unsigned int, unsigned int>(
										vmac[i].getFps(), 1));
						this->device_format_list.push_back(df);
					}
				}
			}
		}
		{
			const openni::SensorInfo& si = this->ir.getSensorInfo();
			const openni::Array<openni::VideoMode>& vma =
					si.getSupportedVideoModes();
			for (int i = 0; i < vma.getSize(); ++i) {
				if (vma[i].getPixelFormat() == openni::PIXEL_FORMAT_GRAY16) {
					Size s(vma[i].getResolutionX(), vma[i].getResolutionY());
					std::list<DeviceFormat>::iterator it =
							this->device_format_list.begin();
					bool found = false;
					while (it != this->device_format_list.end()) {
						if (it->intensity.resolution == s) {
							it->intensity.framerates.push_back(
									std::pair<unsigned int, unsigned int>(
											vma[i].getFps(), 1));
							found = true;
						}
						++it;
					}
					if (false == found) {
						DeviceFormat df;
						df.intensity.resolution = s;
						df.intensity.format = GRAY8U;
						df.intensity.framerates.push_back(
								std::pair<unsigned int, unsigned int>(
										vma[i].getFps(), 1));
						this->device_format_list.push_back(df);
					}
				}
			}
		}

	}

	void generateProperties(void) {

		if (false == this->is_file) {
			{
				Property p;
				p.type.name = "Infrared";
				p.type.description =
						"Switch between infrared and color capture";
				p.on_off = false;
				p.info.has_on_off = true;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			openni::CameraSettings* cs = this->color.getCameraSettings();
			{
				Property p;
				p.type.name = "Auto white balance";
				p.type.description = "For the color Camera";
				p.on_off = cs->getAutoWhiteBalanceEnabled();
				p.info.has_on_off = true;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Auto exposure";
				p.type.description = "For the color Camera";
				p.on_off = cs->getAutoExposureEnabled();
				p.info.has_on_off = true;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Color focal length";
				float vfov = this->color.getVerticalFieldOfView();
				float vres = float(this->color.getVideoMode().getResolutionY());
				p.fvalue = vres / (2.0f * tan(vfov / 2.0f));
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Depth focal length";
				float vfov = this->depth.getVerticalFieldOfView();
				float vres = float(this->depth.getVideoMode().getResolutionY());
				p.fvalue = vres / (2.0f * tan(vfov / 2.0f));
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Infrared focal length";
				float vfov = this->ir.getVerticalFieldOfView();
				float vres = float(this->ir.getVideoMode().getResolutionY());
				p.fvalue = vres / (2.0f * tan(vfov / 2.0f));
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Record";
				p.type.description =
						"Start/stop to save consecutive frames into a ONI file (string)";
				p.info.has_svalue = true;
				p.svalue = "default.oni";
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Close range";
				p.type.description =
						"Trigger the close range mode for the depth camera";
				bool bCloseRange;
				this->depth.getProperty(XN_STREAM_PROPERTY_CLOSE_RANGE,
						&bCloseRange);
				p.on_off = bCloseRange;
				p.info.has_on_off = true;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Hole filter";
				p.type.description =
						"Trigger the hole filtering of the depth camera";
				bool bCloseRange;
				this->depth.getProperty(XN_STREAM_PROPERTY_HOLE_FILTER,
						&bCloseRange);
				p.on_off = bCloseRange;
				p.info.has_on_off = true;
				p.info.changeable = true;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Zero plane distance";
				p.type.description = "The focal length in mm";
				int value;
				this->depth.getProperty(XN_STREAM_PROPERTY_ZERO_PLANE_DISTANCE,
						&value);
				p.ivalue = value;
				p.info.has_ivalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Plane pixel size";
				p.type.description = "The pixel size in mm";
				float value;
				this->depth.getProperty(
						XN_STREAM_PROPERTY_ZERO_PLANE_PIXEL_SIZE, &value);
				p.fvalue = value;
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Max shift";
				p.type.description = "";
				int value;
				this->depth.getProperty(XN_STREAM_PROPERTY_MAX_SHIFT, &value);
				p.ivalue = value;
				p.info.has_ivalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Max depth value";
				p.type.description = "In mm";
				int value = this->depth.getMaxPixelValue();
				p.ivalue = value;
				p.info.has_ivalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Depth Horizontal fov";
				p.fvalue = this->depth.getHorizontalFieldOfView();
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Depth Vertical fov";
				p.fvalue = this->depth.getVerticalFieldOfView();
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Color Horizontal fov";
				p.fvalue = this->color.getHorizontalFieldOfView();
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Color Vertical fov";
				p.fvalue = this->color.getVerticalFieldOfView();
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "IR Horizontal fov";
				p.fvalue = this->ir.getHorizontalFieldOfView();
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "IR Vertical fov";
				p.fvalue = this->ir.getVerticalFieldOfView();
				p.info.has_fvalue = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
			{
				Property p;
				p.type.name = "Is recording";
				p.on_off = false;
				p.info.has_on_off = true;
				p.info.changeable = false;
				this->porperties_map[p.type.name] = p;
			}
		}

	}

}; // End class OpenNI2Grabber

class OpenNI2Driver: public Driver {

public:

	OpenNI2Driver(void) :
			Driver() {
		this->driver_name = std::string("OpenNI2");

		openni::Status rc = openni::STATUS_OK;
		rc = openni::OpenNI::initialize();
		if (openni::STATUS_OK != rc) {
			throw Exception("OpenNI2Driver::OpenNI2Driver",
					openni::OpenNI::getExtendedError());
		}
	}

	~OpenNI2Driver(void) {
		this->devices.clear();
		this->grabbers.clear();
		openni::OpenNI::shutdown();
	}

	bool update(void) {

		this->list_grabbers.clear();
		this->devices.clear();

		openni::Array<openni::DeviceInfo> deviceList;
		openni::OpenNI::enumerateDevices(&deviceList);

		for (int i = 0; i < deviceList.getSize(); ++i) {
			const openni::DeviceInfo& di = deviceList[i];
			std::string name = std::string(di.getName()) + "("
					+ std::string(di.getUri()) + ")";
			this->list_grabbers.push_back(name);
			this->devices[name] = di;
		}

		std::vector<std::string> to_remove;
		std::map<std::string, GrabberPtr>::const_iterator it_grabbers =
				this->grabbers.begin();
		while (it_grabbers != this->grabbers.end()) {
			if (std::find(this->list_grabbers.begin(),
					this->list_grabbers.end(), it_grabbers->first)
					== this->list_grabbers.end()) {
				to_remove.push_back(it_grabbers->first);
			}
			++it_grabbers;
		}

		std::vector<std::string>::const_iterator it_to_remove =
				to_remove.begin();
		while (it_to_remove != to_remove.end()) {
			this->grabbers.erase(*it_to_remove);
			++it_to_remove;
		}

		to_remove.clear();

		return true;
	}

	GrabberPtr getGrabberFromFile(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			this->grabbers[name].reset(new OpenNI2Grabber(name));
		}
		return this->grabbers[name];
	}

	GrabberPtr getGrabber(const std::string& name) {

		if (0 >= this->grabbers.count(name)) {
			if (0 >= this->devices.count(name)) {
				Log::add().error("OpenNI2Driver::getGrabber",
						"This camera was not found (" + name + ")");
				GrabberPtr p;
				p.reset();
				return p;
			}
			try {

				this->grabbers[name].reset(
						new OpenNI2Grabber(this->devices[name]));
			} catch (Exception e) {
				Log::add().error("OpenNI2Driver::getGrabber", e.what());
				GrabberPtr p;
				p.reset();
				return p;
			}
		}

		return this->grabbers[name];
	}

private:

	std::map<std::string, openni::DeviceInfo> devices;
	std::map<std::string, GrabberPtr> grabbers;
};
// end class OpenNI2Driver

static OpenNI2Driver* openni2_driver = 0;

extern "C" GEDEON_EXPORT_PLUGIN int getVersion(void) {
	return DRIVER_VERSION;
}

extern "C" GEDEON_EXPORT_PLUGIN Driver* getDriver(void) {
	if (0 == openni2_driver) {
		openni2_driver = new OpenNI2Driver();
	}
	return dynamic_cast<Driver*>(openni2_driver);
}

extern "C" GEDEON_EXPORT_PLUGIN void releaseDriver(void) {
	if (0 != openni2_driver) {
		delete openni2_driver;
		openni2_driver = 0;
	}
}

}
// end namespace gedeon

