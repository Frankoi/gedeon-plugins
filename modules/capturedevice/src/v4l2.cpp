/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * v4l2.cpp created in 03 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * v4l2.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

// Internal includes
#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/converter.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/capturedevice/driver.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>
#include <map>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/videodev2.h>
#include <libv4l2.h>
#include <fcntl.h>
#include <jpeglib.h>

#define MAX_BUFFERS 4

namespace gedeon {

class V4L2Grabber: public Grabber {

public:

	enum grabMethod {
		IO_READ, IO_MMAP
	};

	enum encodingMethod {
		FMT_YUYV, FMT_MJPEG,
	};

	V4L2Grabber(const std::string& grabber_name) :
			Grabber(), n_buffers(MAX_BUFFERS), buff_length(0), buff_offset(0), mem(
					0), fd(-1), method(IO_MMAP), encoding(FMT_MJPEG), tmp_color_data(
					0) {

		this->name = grabber_name;

		struct stat st;
		// Try to get information about the given file
		if (-1 == stat(name.c_str(), &st)) {
			throw Exception("V4L2Grabber::V4L2Grabber",
					"Unable to open the v4l2 device " + name + " (stat)");
		}

		// Check if it is a character special file
		if (!S_ISCHR(st.st_mode)) {
			throw Exception("V4L2Grabber::V4L2Grabber",
					"Unable to open the v4l2 device " + name + " (S_ISCHR)");
		}

		// Try to open the given file
		this->fd = open(name.c_str(), O_RDWR | O_NONBLOCK, 0);
		if (-1 == this->fd) {
			throw Exception("V4L2Grabber::V4L2Grabber",
					"Unable to open the v4l2 device " + name);
		}

		struct v4l2_format fmt;
		memset(&fmt, 0, sizeof(fmt));
		fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;
		fmt.fmt.pix.field = V4L2_FIELD_ANY; //V4L2_FIELD_INTERLACED;
		fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
		this->encoding = FMT_MJPEG;

		// Try to set format information
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_S_FMT, &fmt)) {
			fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
			this->encoding = FMT_YUYV;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_S_FMT, &fmt)) {
				throw Exception("V4L2Grabber::V4L2Grabber", strerror(errno));
			}
		}

		// Try to get format information
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_FMT, &fmt)) {
			throw Exception("V4L2Grabber::V4L2Grabber", strerror(errno));
		}

		if (false == setCaptureMode()) {
			throw Exception("V4L2Grabber::V4L2Grabber",
					"Unable to set the capture mode");
		}

		int width = fmt.fmt.pix.width;
		int height = fmt.fmt.pix.height;

		this->rgbdi.reset(new RGBDIImage);
		this->rgbdi.get()->color.setSize(Size(width, height));
		this->tmp_color_data = new unsigned char[width * height * 3
				* sizeof(unsigned char)];

		// setup framerate
		unsigned int num, denom;
		getFramerate( num, denom);
		this->adaptative_framerate = static_cast<float>(num) / denom;

		feedFormat();

		generateProperties();

		this->device_format.color.resolution = Size(width, height);
		this->device_format.color.framerate_num = num;
		this->device_format.color.framerate_denom = denom;
		this->device_format.color.format = RGB888U;

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);

	}

	~V4L2Grabber(void) {

		this->stopCapture();
		while (false == this->stream_finished) {
			millisecSleep(100);
		}
		clearCaptureMode();
		if (-1 != this->fd) {
			v4l2_close(this->fd);
		}

		if (0 != this->tmp_color_data) {
			delete[] tmp_color_data;
		}

	}

	bool setProperty(const Property& property) {
		return defineProperty(property);
	}

	bool setFormat(const DeviceFormat& dr) {
		bool ret = setResolution(dr.color.resolution);
		ret &= setFramerate(dr.color.framerate_num, dr.color.framerate_denom);
		return ret;
	}

	void startCapture(void) {
		this->stream_paused = false;
		if (false == this->stream_running) {
			if (0 != this->pthread) {
				this->pthread->interrupt();
				delete this->pthread;
				this->pthread = 0;
			}
			this->pthread = new boost::thread(
					boost::bind(&V4L2Grabber::run, this));
			millisecSleep(200);
			emitEvent(this->event_playing);
		}
	}

	void run(void) {
		this->stream_finished = false;
		this->stream_running = true;

		struct v4l2_buffer buf;
		struct jpeg_decompress_struct cinfo;
		struct jpeg_error_mgr jerr;
		//struct jpeg_source_mgr src_mem;
		JSAMPROW row_pointer[1];

		if (this->method == IO_MMAP) {
			enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_STREAMON, &type)) {
				Log::add().error("V4L2Grabber::run",
						std::string("Unable to start the stream. ")
								+ strerror(errno));
				return;
			}
			boost::this_thread::sleep(boost::posix_time::milliseconds(300));
		}

		if (FMT_MJPEG == this->encoding) {
			cinfo.err = jpeg_std_error(&jerr);
			jpeg_create_decompress(&cinfo);
		}

		Timer timer;
		timer.start();
		long int current_time;

		while (true == this->stream_running) {
			current_time = timer.get();

			if (false == this->stream_paused) {

				unsigned char *buf_tmp = 0;
				size_t buf_tmp_size = 0;

				if (this->method == IO_READ) {

					boost::mutex::scoped_lock l(this->mutex);
					if (-1
							== v4l2_read(this->fd, this->mem[0],
									this->buff_length[0])) {
#ifdef DEBUG
						Log::add().error("V4L2Grabber::run",
								std::string("Unable to grab the image (READ). ")
										+ strerror(errno));
#endif
					}
					buf_tmp = reinterpret_cast<unsigned char*>(this->mem[0]);
					buf_tmp_size = this->buff_length[0];
				} else {
					memset(&buf, 0, sizeof(v4l2_buffer));
					buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
					buf.memory = V4L2_MEMORY_MMAP;
					while (-1 == v4l2_ioctl(this->fd, VIDIOC_DQBUF, &buf))
						;
					buf_tmp =
							reinterpret_cast<unsigned char*>(this->mem[buf.index]);
					buf_tmp_size = this->buff_length[buf.index];

					while (-1 == v4l2_ioctl(this->fd, VIDIOC_QBUF, &buf))
						;
				}

				unsigned int local_size =
						this->rgbdi.get()->color.getSize().width
								* this->rgbdi.get()->color.getSize().height;

				if (FMT_YUYV == this->encoding) {
					local_size /= 2;
					boost::mutex::scoped_lock l(this->mutex);
					for (unsigned int i = 0; i < local_size; ++i) {
						Converter::yuv422ToRgb24(buf_tmp[i * 4],
								buf_tmp[i * 4 + 1], buf_tmp[i * 4 + 2],
								buf_tmp[i * 4 + 3], this->tmp_color_data[i * 6],
								this->tmp_color_data[i * 6 + 1],
								this->tmp_color_data[i * 6 + 2],
								this->tmp_color_data[i * 6 + 3],
								this->tmp_color_data[i * 6 + 4],
								this->tmp_color_data[i * 6 + 5]);
					}
				} else {
					jpeg_mem_src(&cinfo, buf_tmp, buf_tmp_size);
					jpeg_read_header(&cinfo, true);
					jpeg_start_decompress(&cinfo);
					row_pointer[0] = new unsigned char[cinfo.output_width
							* cinfo.num_components];
					unsigned long location = 0;
					boost::mutex::scoped_lock l(this->mutex);
					while (cinfo.output_scanline < cinfo.image_height) {
						jpeg_read_scanlines(&cinfo, row_pointer, 1);
						memcpy(this->tmp_color_data + location, row_pointer[0],
								cinfo.image_width * cinfo.num_components);
						location += cinfo.image_width * cinfo.num_components;
					}
					jpeg_finish_decompress(&cinfo);
					delete[] row_pointer[0];
				}

			}

			/*
			 * Emit a message to confirm the update
			 */
			emitEvent(this->event_updated);

			current_time = (1000.0 / this->adaptative_framerate)
					- (timer.get() - current_time);

			if (current_time > 0) {
				boost::this_thread::sleep(
						boost::posix_time::milliseconds(current_time));
				this->adaptative_framerate = static_cast<float>(this->device_format.color.framerate_num) / this->device_format.color.framerate_denom;
			} else {
				this->adaptative_framerate = static_cast<float>(this->device_format.color.framerate_num) / this->device_format.color.framerate_denom - current_time;
			}
		}

		if (FMT_MJPEG == this->encoding) {
			jpeg_destroy_decompress(&cinfo);
		}

		if (IO_MMAP == this->method) {
			enum v4l2_buf_type type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_STREAMOFF, &type)) {
				Log::add().error("V4L2Grabber::run",
						std::string("Unable to stop the stream. ")
								+ strerror(errno));
			}
		}

		timer.stop();
		this->stream_finished = true;
	}

	RGBDIImagePtr getImage(void) {
		unsigned int local_size = this->rgbdi.get()->color.getSize().width
				* this->rgbdi.get()->color.getSize().height;
		unsigned char *color = this->rgbdi.get()->color.getData().get();
		{
			boost::mutex::scoped_lock l(this->mutex);
			memcpy(color, this->tmp_color_data, local_size * 3);
		}

		return this->rgbdi;
	}

private:

	void generateProperties(void){

		struct v4l2_control control;
		{
			Property p;
			p.type.name = "Exposure auto type";
			p.info.has_ivalue = true;
			p.info.imin = 0;
			p.info.imax = 1;
			p.info.changeable = true;

			memset(&control, 0, sizeof(v4l2_control));
			control.id = V4L2_CID_EXPOSURE_AUTO_PRIORITY;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_CTRL, &control)) {
				Log::add().error("V4L2Grabber::generateProperties",
						std::string("Unable to get control parameter. ")
								+ strerror(errno));
			}else{
				p.ivalue = control.value;
				this->porperties_map[p.type.name] = p;
			}
		}
	}

	Property* getSpecificProperty(const Property& p){
		std::map<std::string, Property>::iterator it = this->porperties_map.begin();
		while (porperties_map.end() != it) {
			if (p.type.name == it->second.type.name) {
				return &(it->second);
			}
			++it;
		}
		return 0;
	}

	bool defineProperty(const Property& p){
		struct v4l2_control control;
		memset(&control, 0, sizeof(v4l2_control));
		if(p.type.name == "Exposure auto type"){
			control.id = V4L2_CID_EXPOSURE_AUTO;
			control.value = p.ivalue;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_S_CTRL, &control)) {
				Log::add().error("V4L2Grabber::defineProperty",
						std::string("Unable to set control parameter. ")
								+ strerror(errno));
				return false;
			}
			getSpecificProperty(p)->ivalue = p.ivalue;

			return true;
		}

		return false;
	}

	bool requestBuffers(void) {
		struct v4l2_requestbuffers req;
		memset(&req, 0, sizeof(v4l2_requestbuffers));
		req.count = this->n_buffers;
		req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		req.memory = V4L2_MEMORY_MMAP;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_REQBUFS, &req)) {
			Log::add().error("V4L2Grabber::requestBuffers",
					std::string("Unable to allocate buffers. ")
							+ strerror(errno));
			return false;
		}
		return true;
	}

	bool unrequestBuffers(void) {
		if (this->fd != -1) {
			struct v4l2_requestbuffers req;
			memset(&req, 0, sizeof(v4l2_requestbuffers));
			req.count = 0;
			req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			req.memory = V4L2_MEMORY_MMAP;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_REQBUFS, &req)) {
				Log::add().error("V4L2Grabber::unrequestBuffers",
						std::string("Unable to allocate buffers. ")
								+ strerror(errno));
				return false;
			}
		}
		return true;
	}

	bool mapBuffers(void) {
		this->mem = new void*[this->n_buffers];
		for (unsigned int i = 0; i < this->n_buffers; i++) {
			this->mem[i] = v4l2_mmap(
					NULL, // start anywhere
					this->buff_length[i], PROT_READ | PROT_WRITE, MAP_SHARED,
					this->fd, this->buff_offset[i]);
			if (this->mem[i] == MAP_FAILED || this->buff_length[i] == 0) {
				Log::add().error("V4L2Grabber::map_buffers",
						std::string("Unable to map a buffer. ")
								+ strerror(errno));
				return false;
			}
		}
		return true;
	}

	bool unmapBuffers(void) {
		for (unsigned int i = 0; i < this->n_buffers; i++) {
			if (MAP_FAILED != this->mem[i] && this->buff_length[i] > 0) {
				if (-1 == v4l2_munmap(this->mem[i], this->buff_length[i])) {
					Log::add().error("V4L2Grabber::ummap_buffers",
							std::string("Unable to unmap a buffer. ")
									+ strerror(errno));
				}
			}
		}
		if (0 != this->mem) {
			delete[] this->mem;
			this->mem = 0;
		}
		return true;
	}

	bool queryBuffers(void) {
		if (this->n_buffers < 2) {
			Log::add().error("V4L2Grabber::query_buffers",
					std::string("number of available buffers is too small"));
			return false;
		}
		this->buff_length = new unsigned int[this->n_buffers];
		this->buff_offset = new unsigned int[this->n_buffers];
		for (unsigned int i = 0; i < this->n_buffers; ++i) {
			struct v4l2_buffer buf;
			memset(&buf, 0, sizeof(struct v4l2_buffer));
			buf.index = i;
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_MMAP;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_QUERYBUF, &buf)) {
				Log::add().error("V4L2Grabber::query_buffers",
						std::string("Unable to query buffers. ")
								+ strerror(errno));
				return false;
			}
			if (buf.length <= 0) {
				Log::add().error("V4L2Grabber::query_buffers",
						std::string("Incorrect buffer length"));
				return false;
			}
			this->buff_length[i] = buf.length;
			this->buff_offset[i] = buf.m.offset;
		}
		return true;
	}

	bool unqueryBuffers(void) {
		if (0 != this->buff_length) {
			delete[] this->buff_length;
		}
		if (0 != this->buff_offset) {
			delete[] this->buff_offset;
		}
		return true;
	}

	bool queueBuffers(void) {
		for (unsigned int i = 0; i < this->n_buffers; ++i) {
			struct v4l2_buffer buf;
			memset(&buf, 0, sizeof(struct v4l2_buffer));
			buf.index = i;
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_MMAP;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_QBUF, &buf)) {
				Log::add().error("V4L2Grabber::queue_buffers",
						std::string("Unable to queue buffers. ")
								+ strerror(errno));
				return false;
			}
		}
		return true;
	}

	bool getFramerate(unsigned int& num, unsigned int& denom) {
		struct v4l2_streamparm my_parm;
		memset(&my_parm, 0, sizeof(v4l2_streamparm));
		my_parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_PARM, &my_parm)) {
			Log::add().error("V4L2Grabber::getFramerate",
					std::string("Unable to access stream parameters. ")
							+ strerror(errno));
			return false;
		}
		if (0 != (V4L2_CAP_TIMEPERFRAME & my_parm.parm.capture.capability)) {
			num = my_parm.parm.capture.timeperframe.denominator;
			denom = my_parm.parm.capture.timeperframe.numerator;
		} else {
			Log::add().error("V4L2Grabber::getFramerate",
					std::string(
							"Unable to access stream parameters V4L2_CAP_TIMEPERFRAME. ")
							+ strerror(errno));
			num = DEFAULT_FRAMERATE;
			denom = 1;
			return false;
		}
		return true;
	}

	bool setFramerate(const int& f, const int& d) {
		bool r = this->stream_running;
		if (r) {
			stopCapture();
		}
		struct v4l2_streamparm my_parm;
		memset(&my_parm, 0, sizeof(v4l2_streamparm));
		my_parm.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_PARM, &my_parm)) {
			Log::add().error("V4L2Grabber::setFramerate",
					std::string("Unable to access stream parameters. ")
							+ strerror(errno));
			if (r) {
				startCapture();
			}
			return false;
		}
		if (0 != (V4L2_CAP_TIMEPERFRAME & my_parm.parm.capture.capability)) {

			clearCaptureMode();
			my_parm.parm.capture.timeperframe.numerator = d;
			my_parm.parm.capture.timeperframe.denominator = f;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_S_PARM, &my_parm)) {
				Log::add().error("V4L2Grabber::setFramerate",
						std::string("Unable to set stream parameters. ")
								+ strerror(errno));
				setCaptureMode();
				if (r) {
					startCapture();
				}
				return false;
			}
			setCaptureMode();
		} else {
			Log::add().error("V4L2Grabber::setFramerate",
					std::string(
							"Unable to access stream parameters V4L2_CAP_TIMEPERFRAME. ")
							+ strerror(errno));

			if (r) {
				startCapture();
			}
			return false;
		}

		this->device_format.color.framerate_denom = d;
		this->device_format.color.framerate_num = f;
		this->adaptative_framerate = float(f) / float(d);

		if (r) {
			startCapture();
		}

		return true;
	}

	bool setResolution(const Size& s) {

		unsigned int w = s.width;
		unsigned int h = s.height;

		bool r = this->stream_running;
		if (r) {
			stopCapture();
		}
		struct v4l2_format fmt;
		memset(&fmt, 0, sizeof(fmt));
		fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_FMT, &fmt)) {
			Log::add().error("V4L2Grabber::init",
					std::string("Unable to get format. ") + strerror(errno));
			return false;
		}
		clearCaptureMode();
		fmt.fmt.pix.width = w;
		fmt.fmt.pix.height = h;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_S_FMT, &fmt)) {
			Log::add().error("V4L2Grabber::init",
					std::string("Unable to set format. ") + strerror(errno));
			setCaptureMode();
			if (r) {
				startCapture();
			}
			return false;
		}
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_FMT, &fmt)) {
			Log::add().error("V4L2Grabber::init",
					std::string("Unable to get format. ") + strerror(errno));
			setCaptureMode();
			if (r) {
				startCapture();
			}
			return false;
		}
		int width = fmt.fmt.pix.width;
		int height = fmt.fmt.pix.height;
		this->rgbdi.get()->color.setSize(Size(width, height));

		delete[] this->tmp_color_data;
		this->tmp_color_data = new unsigned char[width * height * 3
				* sizeof(unsigned char)];

		setCaptureMode();

		unsigned int num, denom;
		getFramerate(num,denom);
		this->device_format.color.resolution.width = width;
		this->device_format.color.resolution.height = height;
		this->device_format.color.framerate_denom = denom;
		this->device_format.color.framerate_num = num;
		this->adaptative_framerate = static_cast<float>(this->device_format.color.framerate_num) / this->device_format.color.framerate_denom;

		if (r) {
			startCapture();
		}
		return true;
	}

	bool clearCaptureMode(void) {
		if (this->fd != -1) {
			if (this->method == 1) {
				unmapBuffers();
				unqueryBuffers();
				unrequestBuffers();
			} else {
				if (0 != this->mem) {
					delete[] this->mem;
					this->mem = 0;
				}
				if (0 != this->buff_length) {
					delete[] this->buff_length;
					this->buff_length = 0;
				}
				if (0 != this->buff_offset) {
					delete[] this->buff_offset;
					this->buff_offset = 0;
				}
			}
		}
		return true;
	}

	bool setCaptureMode(void) {
		struct v4l2_capability cap;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_QUERYCAP, &cap)) {
			Log::add().error("V4L2Grabber::setCaptureMode",
					std::string("Unable to access device capabilities ")
							+ strerror(errno));
			return false;
		}

		if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
			Log::add().error("V4L2Grabber::setCaptureMode",
					std::string("Device is not a capture device"));
			return false;
		}

		if (!(cap.capabilities & V4L2_CAP_STREAMING)) {
			struct v4l2_format fmt;
			memset(&fmt, 0, sizeof(fmt));
			fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_FMT, &fmt)) {
				Log::add().error("V4L2Grabber::init",
						std::string("Unable to get format. ")
								+ strerror(errno));
				return false;
			}
			this->mem = new void*[1];
			this->buff_length = new unsigned int[1];
			this->buff_offset = new unsigned int[1];
			this->buff_length[0] = fmt.fmt.pix.sizeimage;
			this->buff_offset[0] = fmt.fmt.pix.sizeimage;
			this->method = IO_READ;
		} else {
			this->n_buffers = MAX_BUFFERS;
			if (false == requestBuffers()) {
				return false;
			}
			if (false == queryBuffers()) {
				unqueryBuffers();
				return false;
			}
			if (false == mapBuffers()) {
				unmapBuffers();
				unqueryBuffers();
				unrequestBuffers();
				return false;
			}
			if (false == queueBuffers()) {
				unmapBuffers();
				unqueryBuffers();
				unrequestBuffers();
				return false;
			}

			this->method = IO_MMAP;

		}
		return true;
	}

	bool triggerControl(const int& request) {
		struct v4l2_control control;
		memset(&control, 0, sizeof(v4l2_control));
		control.id = request;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_CTRL, &control)) {
			Log::add().error("V4L2Grabber::triggerControl",
					std::string("Unable to access control. ")
							+ strerror(errno));
			return false;
		}

		if (request == V4L2_CID_EXPOSURE_AUTO) {
			if (V4L2_EXPOSURE_APERTURE_PRIORITY == control.value) {
				control.value = V4L2_EXPOSURE_MANUAL;
			} else {
				control.value = V4L2_EXPOSURE_APERTURE_PRIORITY;
			}
		} else {
			control.value = !control.value;
		}

		if (-1 == v4l2_ioctl(this->fd, VIDIOC_S_CTRL, &control)) {
			Log::add().error("V4L2Grabber::triggerControl",
					std::string("Unable to set control. ") + strerror(errno));
			return false;
		}
		return true;
	}

	bool setControl(const int& request, const float& value) {
		struct v4l2_control control;
		memset(&control, 0, sizeof(v4l2_control));
		control.id = request;
		control.value = (int) value;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_S_CTRL, &control)) {
			Log::add().error("V4L2Grabber::setControl",
					std::string("Unable to set control. ") + strerror(errno));
			return false;
		}
		return true;
	}

	bool getControl(const int& request, float& value) {
		struct v4l2_control control;
		memset(&control, 0, sizeof(v4l2_control));
		control.id = request;
		control.value = value;
		if (-1 == v4l2_ioctl(this->fd, VIDIOC_G_CTRL, &control)) {
			Log::add().error("V4L2Grabber::getControl",
					std::string("Unable to get control. ") + strerror(errno));
			return false;
		}
		value = (float) control.value;
		return true;
	}

	void feedFramerate(const unsigned int& width, const unsigned int& height,
			DeviceFormat& df) {
		df.color.framerates.clear();
		int ret = 0;
		struct v4l2_frmivalenum fival;
		memset(&fival, 0, sizeof(fival));
		fival.index = 0;
		if (FMT_YUYV == this->encoding) {
			fival.pixel_format = V4L2_PIX_FMT_YUYV;
		} else {
			fival.pixel_format = V4L2_PIX_FMT_MJPEG;
		}
		fival.width = width;
		fival.height = height;
		while ((ret = v4l2_ioctl(fd, VIDIOC_ENUM_FRAMEINTERVALS, &fival)) == 0) {
			fival.index++;
			if (fival.type == V4L2_FRMIVAL_TYPE_DISCRETE) {
				df.color.framerates.push_back( std::pair<unsigned int ,unsigned int >(fival.discrete.denominator,fival.discrete.numerator));
			}
		}
		if(0 == df.color.framerates.size()){
			df.color.framerates.push_back( std::pair<unsigned int ,unsigned int >(1,1));
		}
	}

	void feedFormat(void) {
		this->device_format_list.clear();
		struct v4l2_frmsizeenum fsize;
		int ret = 0;
		memset(&fsize, 0, sizeof(fsize));
		fsize.index = 0;
		if (FMT_YUYV == this->encoding) {
			fsize.pixel_format = V4L2_PIX_FMT_YUYV;
		} else {
			fsize.pixel_format = V4L2_PIX_FMT_MJPEG;
		}
		while ((ret = v4l2_ioctl(fd, VIDIOC_ENUM_FRAMESIZES, &fsize)) == 0) {
			fsize.index++;
			if (fsize.type == V4L2_FRMSIZE_TYPE_DISCRETE) {
				DeviceFormat df;
				df.color.resolution.height = fsize.discrete.height;
				df.color.resolution.width = fsize.discrete.width;
				feedFramerate(fsize.discrete.width,fsize.discrete.height,df);
				this->device_format_list.push_back(df);
			}
		}
	}

private:

	unsigned int n_buffers;
	unsigned int *buff_length;
	unsigned int *buff_offset;
	void **mem;
	int fd;
	int method;
	int encoding;
	unsigned char *tmp_color_data;
};

using namespace boost::filesystem;

class V4L2Driver: public Driver {

public:

	V4L2Driver(void) :
			Driver() {
		this->driver_name = std::string("v4l2");
	}

	~V4L2Driver(void) {
		this->device_names.clear();
		this->grabbers.clear();
	}

	bool update(void) {

		fillDeviceNames();

		this->list_grabbers.clear();

		/*
		 * Try to open the cameras with an id
		 */
		struct stat st;
		std::vector<std::string>::const_iterator it = device_names.begin();
		while (device_names.end() != it) {
			if (-1 != stat(it->c_str(), &st)) {
				if (S_ISCHR(st.st_mode)) {
					int fd = open(it->c_str(), O_RDWR | O_NONBLOCK, 0);
					if (-1 != fd) {
						this->list_grabbers.push_back(*it);
						close(fd);
					}
				}
			}
			++it;
		}

		if (0 == this->list_grabbers.size()) {
			Log::add().warning("V4L2Driver::refresh",
					"No available V4L2 device found");
			return false;
		}

		return true;
	}

	GrabberPtr getGrabber(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			this->grabbers[name].reset(new V4L2Grabber(name));
		}
		return this->grabbers[name];
	}

	GrabberPtr getGrabberFromFile(const std::string& name) {
		GrabberPtr p;
		p.reset();
		return p;
	}

private:

	void fillDeviceNames(void) {
		this->device_names.clear();
		path dir("/dev/");
		if (exists(dir)) {
			directory_iterator end_iter;
			for (directory_iterator iter(dir); iter != end_iter; ++iter) {
				if (!is_directory(*iter)) {
					unsigned pos = iter->path().string().find("video");
					if (iter->path().string().length() > pos) {
						this->device_names.push_back(iter->path().string());
					}
				}
			}

		}
	}

private:

	std::vector<std::string> device_names;
	std::map<std::string, GrabberPtr> grabbers;

};
// end class V4L2Driver

static V4L2Driver* v4l2_driver = 0;

extern "C" GEDEON_EXPORT_PLUGIN int getVersion(void) {
	return DRIVER_VERSION;
}

extern "C" GEDEON_EXPORT_PLUGIN Driver* getDriver(void) {
	if (0 == v4l2_driver) {
		v4l2_driver = new V4L2Driver();
	}
	return dynamic_cast<Driver*>(v4l2_driver);
}

extern "C" GEDEON_EXPORT_PLUGIN void releaseDriver(void) {
	if (0 != v4l2_driver) {
		delete v4l2_driver;
		v4l2_driver = 0;
	}
}

} // end namespace gedeon

