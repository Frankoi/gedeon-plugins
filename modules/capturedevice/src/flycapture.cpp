/***************************************************************************\
 * Copyright (C) by Francois de Sorbier
 * flycapture.cpp created in 04 2014.
 * property : fdesorbi@hvrl.ics.keio.ac.jp
 *
 * flycapture.cpp is part of the GEDEON Library.
 *
 * The GEDEON Library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GEDEON Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 \***************************************************************************/

// Internal includes
#include "gedeon/core/config.hpp"
#include "gedeon/core/log.hpp"
#include "gedeon/core/io.hpp"
#include "gedeon/core/time.hpp"
#include "gedeon/core/event.hpp"
#include "gedeon/core/converter.hpp"
#include "gedeon/core/exception.hpp"
#include "gedeon/capturedevice/driver.hpp"

// External includes
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>
#include <map>
#include <FlyCapture2.h>

static const unsigned int MAX_BUFFERS = 2;

namespace gedeon {

class FlyCapture2Grabber: public Grabber {

private:

	FlyCapture2::Camera cam;
	unsigned int current_buffer;
	unsigned char *buffers;
	unsigned int width;
	unsigned int height;
	unsigned int timestamp[MAX_BUFFERS];
	bool has_timestamp;

	PixelFormat pf;

public:

	FlyCapture2Grabber(FlyCapture2::PGRGuid& guid) :
			Grabber(), current_buffer(0), buffers(0) {

		/*
		 * Connect to the camera
		 */
		FlyCapture2::Error m_error = this->cam.Connect(&guid);
		if (m_error != FlyCapture2::PGRERROR_OK
				|| false == this->cam.IsConnected()) {
			throw Exception("FlyCapture2Grabber::FlyCapture2Grabber",
					std::string("Unable to connect with the camera ")
							+ m_error.GetDescription());
		}

		feedFormat();

		FlyCapture2::VideoMode pVideoMode;
		FlyCapture2::FrameRate pFrameRate;
		this->cam.GetVideoModeAndFrameRate(&pVideoMode, &pFrameRate);
		if (false
				== convertResolution(pVideoMode,
						this->device_format.color.resolution.width,
						this->device_format.color.resolution.height,
						this->pf)) {
			throw Exception("FlyCapture2Grabber::FlyCapture2Grabber",
					std::string("Unknown video format"));
		}
		if (false
				== convertFramerate(pFrameRate,
						this->device_format.color.framerate_num,
						this->device_format.color.framerate_denom)) {
			throw Exception("FlyCapture2Grabber::FlyCapture2Grabber",
					std::string("Unknown framerate format"));
		}

		FlyCapture2::EmbeddedImageInfo info;
		this->cam.GetEmbeddedImageInfo(&info);
		this->has_timestamp = false;
		if(true == info.timestamp.available){
			info.timestamp.onOff = true;
			this->has_timestamp = true;
			this->cam.SetEmbeddedImageInfo(&info);
		}


		this->rgbdi.reset(new RGBDIImage);
		this->width = this->device_format.color.resolution.width;
		this->height = this->device_format.color.resolution.height;
		this->rgbdi.get()->color.setSize(Size(this->width, this->height));

		this->buffers = new unsigned char[MAX_BUFFERS * 3 * this->width
				* this->height];

		generateProperties();

		this->event_updated->sender = this;
		this->event_ready->sender = this;
		this->event_stopped->sender = this;
		this->event_playing->sender = this;
		this->event_paused->sender = this;
		emitEvent(this->event_ready);

	}

	~FlyCapture2Grabber(void) {
		this->stopCapture();
		this->cam.Disconnect();
		if (this->buffers != 0) {
			delete[] this->buffers;
		}
	}

	bool setProperty(const Property& property) {

		FlyCapture2::Property fc2_p;
		bool ok = false;
		if ("Brightness" == property.type.name) {
			fc2_p.type = FlyCapture2::BRIGHTNESS;
			ok = true;
		}
		if ("Auto exposure" == property.type.name) {
			fc2_p.type = FlyCapture2::AUTO_EXPOSURE;
			ok = true;
		}
		if ("Sharpness" == property.type.name) {
			fc2_p.type = FlyCapture2::SHARPNESS;
			ok = true;
		}
		if ("White balance" == property.type.name) {
			fc2_p.type = FlyCapture2::WHITE_BALANCE;
			ok = true;
		}
		if ("Hue" == property.type.name) {
			fc2_p.type = FlyCapture2::HUE;
			ok = true;
		}
		if ("Saturation" == property.type.name) {
			fc2_p.type = FlyCapture2::SATURATION;
			ok = true;
		}
		if ("Gamma" == property.type.name) {
			fc2_p.type = FlyCapture2::GAMMA;
			ok = true;
		}
		if ("Iris" == property.type.name) {
			fc2_p.type = FlyCapture2::IRIS;
			ok = true;
		}
		if ("Focus" == property.type.name) {
			fc2_p.type = FlyCapture2::FOCUS;
			ok = true;
		}
		if ("Zoom" == property.type.name) {
			fc2_p.type = FlyCapture2::ZOOM;
			ok = true;
		}
		if ("Pan" == property.type.name) {
			fc2_p.type = FlyCapture2::PAN;
			ok = true;
		}
		if ("Tilt" == property.type.name) {
			fc2_p.type = FlyCapture2::TILT;
			ok = true;
		}
		if ("Shutter" == property.type.name) {
			fc2_p.type = FlyCapture2::SHUTTER;
			ok = true;
		}
		if ("Gain" == property.type.name) {
			fc2_p.type = FlyCapture2::GAIN;
			ok = true;
		}

		fc2_p.onOff = property.on_off;
		fc2_p.autoManualMode = property.auto_manual_mode;
		fc2_p.absValue = property.fvalue;

		if (false == ok) {
			Log::add().error("FlyCapture2Grabber::setProperty",
					"Unknown property " + property.type.name);
			return false;
		}

		FlyCapture2::Error m_error = this->cam.SetProperty(&fc2_p);
		if (m_error != FlyCapture2::PGRERROR_OK) {
			Log::add().error("FlyCapture2Grabber::setProperty",
					m_error.GetDescription());
			return false;
		}

		this->porperties_map[property.type.name].on_off = fc2_p.onOff;
		this->porperties_map[property.type.name].auto_manual_mode =
				fc2_p.autoManualMode;
		this->porperties_map[property.type.name].fvalue = fc2_p.absValue;

		return true;
	}

	void startCapture(void) {

		this->stream_paused = false;
		if (false == this->stream_running) {
			this->cam.StartCapture(&FlyCapture2Grabber::callback, this);
			this->stream_running = true;
			millisecSleep(100);
			emitEvent(this->event_playing);
		}
	}

	void stopCapture(void) {
		if (true == this->stream_running) {
			this->stream_running = false;
			this->cam.StopCapture();
			millisecSleep(100);
			emitEvent(this->event_stopped);
		}
	}

	bool receivedData(FlyCapture2::Image *img) {

		if (true == this->stream_paused) {
			return true;
		}
		unsigned char * data = img->GetData();
		if (0 == data || img->GetCols() != this->width
				|| img->GetRows() != this->height) {
			Log::add().error("FlyCapture2Grabber::receivedData",
					"No data or invalid image size");
			return false;
		}
		size_t buffer_size = this->width * this->height * 3;

		if (FlyCapture2::PIXEL_FORMAT_RGB == img->GetPixelFormat()) {
			memcpy(this->buffers + this->current_buffer * buffer_size, data,
					buffer_size);
		} else {
			FlyCapture2::Image img_tmp;
			img->Convert(FlyCapture2::PIXEL_FORMAT_RGB, &img_tmp);
			memcpy(this->buffers + this->current_buffer * buffer_size,
					img_tmp.GetData(), buffer_size);
		}
		if(false == this->has_timestamp || 0 == img->GetTimeStamp().seconds){
			this->timestamp[this->current_buffer] = getTimeStamp();
		}else{
			this->timestamp[this->current_buffer] = img->GetTimeStamp().seconds*1000 + img->GetTimeStamp().microSeconds/1000;
		}
		{
			boost::mutex::scoped_lock l(this->mutex);
			++this->current_buffer;
			this->current_buffer %= MAX_BUFFERS;
		}
		emitEvent(this->event_updated);
		return true;
	}

	RGBDIImagePtr getImage(void) {
		unsigned char *color = this->rgbdi.get()->color.getData().get();
		{
			unsigned int pos = (this->current_buffer - 1) % MAX_BUFFERS;
			size_t buffer_size = this->width * this->height * 3;
			boost::mutex::scoped_lock l(this->mutex);
			memcpy(color, this->buffers + pos * buffer_size, buffer_size);
			this->rgbdi.get()->color.setTimeStamp(this->timestamp[pos]);
		}

		return this->rgbdi;
	}

	bool setFormat(const DeviceFormat& df) {

		FlyCapture2::FrameRate fr;
		bool ret = convertFramerate(df.color.framerate_num, df.color.framerate_denom, fr);
		FlyCapture2::VideoMode vm;
		ret &= convertResolution(df.color.resolution.width,
				df.color.resolution.height, this->pf, vm);

		if (true == ret) {
			bool p = this->stream_running;
			stopCapture();
			boost::mutex::scoped_lock l(this->mutex);

			FlyCapture2::Error m_error = this->cam.SetVideoModeAndFrameRate(vm,
					fr);
			millisecSleep(50);

			if (m_error == FlyCapture2::PGRERROR_OK) {
				this->device_format = df;
				if (df.color.resolution.width != this->width
						|| df.color.resolution.height != this->height) {
					this->width = df.color.resolution.width;
					this->height = df.color.resolution.height;
					delete[] this->buffers;
					this->buffers = new unsigned char[MAX_BUFFERS * 3
							* this->width * this->height];
					this->rgbdi.get()->color.setSize(
							Size(this->width, this->height));
				}
			}

			if (true == p) {
				startCapture();
			}
			if (m_error != FlyCapture2::PGRERROR_OK) {
				return false;
			}
			return true;
		}
		return false;
	}

private:

	void generateProperties(void) {
		{
			Property p;
			p.type.name = "Auto exposure";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::AUTO_EXPOSURE;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Brightness";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::BRIGHTNESS;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Sharpness";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::SHARPNESS;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "White balance";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::WHITE_BALANCE;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Hue";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::HUE;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Saturation";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::SATURATION;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Gamma";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::GAMMA;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Iris";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::IRIS;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Focus";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::FOCUS;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Zoom";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::ZOOM;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Pan";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::PAN;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Tilt";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::TILT;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Shutter";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::SHUTTER;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
		{
			Property p;
			p.type.name = "Gain";
			p.type.description = "";
			FlyCapture2::Property fc2_p;
			FlyCapture2::PropertyInfo fc2_pi;
			fc2_p.type = FlyCapture2::GAIN;
			this->cam.GetProperty(&fc2_p);
			this->cam.GetPropertyInfo(&fc2_pi);
			if (true == fc2_pi.onOffSupported) {
				p.info.has_on_off = true;
				p.on_off = fc2_p.onOff;
			}
			if (true == fc2_pi.absValSupported) {
				p.info.has_fvalue = true;
				p.fvalue = fc2_p.absValue;
				p.info.fmax = fc2_pi.absMax;
				p.info.fmin = fc2_pi.absMin;
			}
			if (true == fc2_pi.autoSupported
					&& true == fc2_pi.manualSupported) {
				p.info.has_auto_manual_mode = true;
			}
			p.auto_manual_mode = fc2_p.autoManualMode;
			p.info.changeable = true;
			this->porperties_map[p.type.name] = p;
		}
	}

	static void callback(FlyCapture2::Image *img, const void *data) {
		void * g2 = const_cast<void *>(data);
		FlyCapture2Grabber * g3 = reinterpret_cast<FlyCapture2Grabber *>(g2);
		g3->receivedData(img);
	}

	bool convertFramerate(const unsigned int& framerate_num,
			const unsigned int& framerate_denom, FlyCapture2::FrameRate& fr) {
		if (framerate_num == 7 && framerate_denom == 8) {
			fr = FlyCapture2::FRAMERATE_1_875;
			return true;
		}
		if (framerate_num == 15 && framerate_denom == 4) {
			fr = FlyCapture2::FRAMERATE_3_75;
			return true;
		}
		if (framerate_num == 15 && framerate_denom == 2) {
			fr = FlyCapture2::FRAMERATE_7_5;
			return true;
		}
		if (framerate_num == 15 && framerate_denom == 1) {
			fr = FlyCapture2::FRAMERATE_15;
			return true;
		}
		if (framerate_num == 30 && framerate_denom == 1) {
			fr = FlyCapture2::FRAMERATE_30;
			return true;
		}
		if (framerate_num == 60 && framerate_denom == 1) {
			fr = FlyCapture2::FRAMERATE_60;
			return true;
		}
		if (framerate_num == 120 && framerate_denom == 1) {
			fr = FlyCapture2::FRAMERATE_120;
			return true;
		}
		if (framerate_num == 240 && framerate_denom == 1) {
			fr = FlyCapture2::FRAMERATE_240;
			return true;
		}
		return false;
	}

	bool convertResolution(const unsigned int& width,
			const unsigned int& height, const PixelFormat& format,
			FlyCapture2::VideoMode& vm) {

		if (width == 160 && height == 120 && format == YUV444) {
			vm = FlyCapture2::VIDEOMODE_160x120YUV444;
			return true;
		}

		if (width == 320 && height == 240 && format == YUV422) {
			vm = FlyCapture2::VIDEOMODE_320x240YUV422;
			return true;
		}

		if (width == 640 && height == 480 && format == YUV411) {
			vm = FlyCapture2::VIDEOMODE_640x480YUV411;
			return true;
		}

		if (width == 640 && height == 480 && format == YUV422) {
			vm = FlyCapture2::VIDEOMODE_640x480YUV422;
			return true;
		}

		if (width == 640 && height == 480 && format == RGB888U) {
			vm = FlyCapture2::VIDEOMODE_640x480RGB;
			return true;
		}

		if (width == 800 && height == 600 && format == YUV422) {
			vm = FlyCapture2::VIDEOMODE_800x600YUV422;
			return true;
		}

		if (width == 800 && height == 600 && format == RGB888U) {
			vm = FlyCapture2::VIDEOMODE_800x600RGB;
			return true;
		}

		if (width == 1024 && height == 768 && format == YUV422) {
			vm = FlyCapture2::VIDEOMODE_1024x768YUV422;
			return true;
		}

		if (width == 1024 && height == 768 && format == RGB888U) {
			vm = FlyCapture2::VIDEOMODE_1024x768RGB;
			return true;
		}

		if (width == 1280 && height == 960 && format == YUV422) {
			vm = FlyCapture2::VIDEOMODE_1280x960YUV422;
			return true;
		}

		if (width == 1280 && height == 960 && format == RGB888U) {
			vm = FlyCapture2::VIDEOMODE_1280x960RGB;
			return true;
		}

		if (width == 1600 && height == 1200 && format == YUV422) {
			vm = FlyCapture2::VIDEOMODE_1600x1200YUV422;
			return true;
		}

		if (width == 1600 && height == 1200 && format == RGB888U) {
			vm = FlyCapture2::VIDEOMODE_1600x1200RGB;
			return true;
		}

		return false;
	}

	bool convertFramerate(const FlyCapture2::FrameRate& fr,
			unsigned int& framerate_num, unsigned int& framerate_denom) {

		if (fr == FlyCapture2::FRAMERATE_1_875) {
			framerate_num = 7;
			framerate_denom = 8;
			return true;
		}
		if (fr == FlyCapture2::FRAMERATE_3_75) {
			framerate_num = 15;
			framerate_denom = 4;
			return true;
		}
		if (fr == FlyCapture2::FRAMERATE_7_5) {
			framerate_num = 15;
			framerate_denom = 2;
			return true;
		}
		if (fr == FlyCapture2::FRAMERATE_15) {
			framerate_num = 15;
			framerate_denom = 1;
			return true;
		}
		if (fr == FlyCapture2::FRAMERATE_30) {
			framerate_num = 30;
			framerate_denom = 1;
			return true;
		}
		if (fr == FlyCapture2::FRAMERATE_60) {
			framerate_num = 60;
			framerate_denom = 1;
			return true;
		}
		if (fr == FlyCapture2::FRAMERATE_120) {
			framerate_num = 120;
			framerate_denom = 1;
			return true;
		}
		if (fr == FlyCapture2::FRAMERATE_240) {
			framerate_num = 240;
			framerate_denom = 1;
			return true;
		}

		return false;
	}

	bool convertResolution(const FlyCapture2::VideoMode& vm,
			unsigned int& width, unsigned int& height, PixelFormat& format) {

		if (vm == FlyCapture2::VIDEOMODE_160x120YUV444) {
			width = 160;
			height = 120;
			format = YUV444;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_320x240YUV422) {
			width = 320;
			height = 240;
			format = YUV422;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_640x480YUV411) {
			width = 640;
			height = 480;
			format = YUV411;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_640x480YUV422) {
			width = 640;
			height = 480;
			format = YUV422;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_640x480RGB) {
			width = 640;
			height = 480;
			format = RGB888U;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_800x600YUV422) {
			width = 800;
			height = 600;
			format = YUV422;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_800x600RGB) {
			width = 800;
			height = 600;
			format = RGB888U;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_1024x768YUV422) {
			width = 1024;
			height = 768;
			format = YUV422;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_1024x768RGB) {
			width = 1024;
			height = 768;
			format = RGB888U;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_1280x960YUV422) {
			width = 1280;
			height = 960;
			format = YUV422;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_1280x960RGB) {
			width = 1280;
			height = 960;
			format = RGB888U;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_1600x1200YUV422) {
			width = 1600;
			height = 1200;
			format = YUV422;
			return true;
		}

		if (vm == FlyCapture2::VIDEOMODE_1600x1200RGB) {
			width = 1600;
			height = 1200;
			format = RGB888U;
			return true;
		}

		return false;
	}

	void iterateFramerate(FlyCapture2::VideoMode vm, DeviceFormat& df) {
		bool supported = false;
		std::pair<unsigned int, unsigned int> f;
		FlyCapture2::FrameRate fr = FlyCapture2::FRAMERATE_1_875;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 7;
			f.second = 8;
			df.color.framerates.push_back(f);
		}
		fr = FlyCapture2::FRAMERATE_3_75;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 15;
			f.second = 4;
			df.color.framerates.push_back(f);
		}
		fr = FlyCapture2::FRAMERATE_7_5;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 15;
			f.second = 2;
			df.color.framerates.push_back(f);
		}
		fr = FlyCapture2::FRAMERATE_15;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 15;
			f.second = 1;
			df.color.framerates.push_back(f);
		}
		fr = FlyCapture2::FRAMERATE_30;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 30;
			f.second = 1;
			df.color.framerates.push_back(f);
		}
		fr = FlyCapture2::FRAMERATE_60;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 60;
			f.second = 1;
			df.color.framerates.push_back(f);
		}
		fr = FlyCapture2::FRAMERATE_120;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 120;
			f.second = 1;
			df.color.framerates.push_back(f);
		}
		fr = FlyCapture2::FRAMERATE_240;
		this->cam.GetVideoModeAndFrameRateInfo(vm, fr, &supported);
		if (true == supported) {
			f.first = 240;
			f.second = 1;
			df.color.framerates.push_back(f);
		}
	}

	void feedFormat(void) {
		this->device_format_list.clear();
		{
			DeviceFormat df;
			df.color.resolution.height = 120;
			df.color.resolution.width = 160;
			df.color.format = YUV444;
			iterateFramerate(FlyCapture2::VIDEOMODE_160x120YUV444, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.height = 240;
			df.color.resolution.width = 320;
			df.color.format = YUV422;
			iterateFramerate(FlyCapture2::VIDEOMODE_320x240YUV422, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.height = 480;
			df.color.resolution.width = 640;
			df.color.format = YUV422;
			iterateFramerate(FlyCapture2::VIDEOMODE_640x480YUV422, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 640;
			df.color.resolution.height = 480;
			df.color.format = YUV411;
			iterateFramerate(FlyCapture2::VIDEOMODE_640x480YUV411, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 640;
			df.color.resolution.height = 480;
			df.color.format = RGB888U;
			iterateFramerate(FlyCapture2::VIDEOMODE_640x480RGB, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 800;
			df.color.resolution.height = 600;
			df.color.format = YUV422;
			iterateFramerate(FlyCapture2::VIDEOMODE_800x600YUV422, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 800;
			df.color.resolution.height = 600;
			df.color.format = RGB888U;
			iterateFramerate(FlyCapture2::VIDEOMODE_800x600RGB, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 1024;
			df.color.resolution.height = 768;
			df.color.format = YUV422;
			iterateFramerate(FlyCapture2::VIDEOMODE_1024x768YUV422, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 1024;
			df.color.resolution.height = 768;
			df.color.format = RGB888U;
			iterateFramerate(FlyCapture2::VIDEOMODE_1024x768RGB, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 1280;
			df.color.resolution.height = 960;
			df.color.format = YUV422;
			iterateFramerate(FlyCapture2::VIDEOMODE_1280x960YUV422, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 1280;
			df.color.resolution.height = 960;
			df.color.format = RGB888U;
			iterateFramerate(FlyCapture2::VIDEOMODE_1280x960RGB, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 1600;
			df.color.resolution.height = 1200;
			df.color.format = YUV422;
			iterateFramerate(FlyCapture2::VIDEOMODE_1600x1200YUV422, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
		{
			DeviceFormat df;
			df.color.resolution.width = 1600;
			df.color.resolution.height = 1200;
			df.color.format = RGB888U;
			iterateFramerate(FlyCapture2::VIDEOMODE_1600x1200RGB, df);
			if (0 < df.color.framerates.size()) {
				this->device_format_list.push_back(df);
			}
		}
	}

}
;

class FlyCapture2Driver: public Driver {

public:

	FlyCapture2Driver(void) :
			Driver() {
		this->driver_name = std::string("flycapture2");
	}

	~FlyCapture2Driver(void) {
		this->grabbers.clear();
		this->camera_guids.clear();
	}

	bool update(void) {
		/*
		 * Get number of connected cameras
		 */
		unsigned int numCameras;
		FlyCapture2::Error m_error = this->busMgr.GetNumOfCameras(&numCameras);
		if (m_error != FlyCapture2::PGRERROR_OK) {
			Log::add().error("FlyCapture2Driver::update",
					std::string("Could not obtain the number of cameras. ")
							+ m_error.GetDescription());
			return false;
		}
		for (unsigned int i = 0; i < numCameras; ++i) {
			FlyCapture2::PGRGuid guid;
			m_error = this->busMgr.GetCameraFromIndex(i, &guid);
			if (m_error != FlyCapture2::PGRERROR_OK) {
				Log::add().warning("FlyCapture2Driver::update",
						std::string("Could access a camera. ")
								+ m_error.GetDescription());
			} else {
				FlyCapture2::Camera cam;
				m_error = cam.Connect(&guid);
				if (m_error != FlyCapture2::PGRERROR_OK) {
					Log::add().warning("FlyCapture2Driver::update",
							"Unable to connect with the camera "
									+ IO::numberToString(i) + ". "
									+ m_error.GetDescription());
				} else {
					FlyCapture2::CameraInfo info;
					cam.GetCameraInfo(&info);
					std::string name = std::string(info.modelName) + " "
							+ IO::numberToString(info.serialNumber);
					this->camera_guids[name] = guid;
					this->list_grabbers.push_back(name);
					cam.Disconnect();
				}
			}

		}
		return true;
	}

	GrabberPtr getGrabber(const std::string& name) {
		if (0 >= this->grabbers.count(name)) {
			if (0 >= this->camera_guids.count(name)) {
				Log::add().error("FlyCapture2Driver::getGrabber",
						"This camera was not found");
				GrabberPtr p;
				p.reset();
				return p;
			}
			try {
				this->grabbers[name].reset(
						new FlyCapture2Grabber(this->camera_guids[name]));
			} catch (Exception e) {
				Log::add().error("FlyCapture2Driver::getGrabber", e.what());
				GrabberPtr p;
				p.reset();
				return p;
			}
		}
		return this->grabbers[name];
	}

private:

	std::map<std::string, GrabberPtr> grabbers;
	std::map<std::string, FlyCapture2::PGRGuid> camera_guids;
	FlyCapture2::BusManager busMgr;

};
// end class FlyCapture2Driver

static FlyCapture2Driver* flycapture2_driver = 0;

extern "C" GEDEON_EXPORT_PLUGIN int getVersion(void) {
	return DRIVER_VERSION;
}

extern "C" GEDEON_EXPORT_PLUGIN Driver* getDriver(void) {
	if (0 == flycapture2_driver) {
		flycapture2_driver = new FlyCapture2Driver();
	}
	return dynamic_cast<Driver*>(flycapture2_driver);
}

extern "C" GEDEON_EXPORT_PLUGIN void releaseDriver(void) {
	if (0 != flycapture2_driver) {
		delete flycapture2_driver;
		flycapture2_driver = 0;
	}
}

}
// end namespace gedeon

