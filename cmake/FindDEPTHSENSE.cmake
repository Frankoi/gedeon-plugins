#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * FindDEPTHSENSE.cmake created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * FindDEPTHSENSE.cmake is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

###############################################################################
# Find DEPTHSENSE
#
# This sets the following variables:
# DEPTHSENSE_FOUND - True if OpenNI was found.
# DEPTHSENSE_INCLUDE_DIR - Directories containing the OpenNI include files.
# DEPTHSENSE_LIBRARIES - Libraries needed to use OpenNI.

IF (WIN32)
	FIND_PATH( DEPTHSENSE_INCLUDE_DIR DepthSense.hxx
		PATHS "C:/Program Files\\SoftKinetic\\DepthSenseSDK\\include"
		"C:/Program Files (x86)\\SoftKinetic\\DepthSenseSDK\\include"
		"$ENV{PROGRAMFILES}\\SoftKinetic\\DepthSenseSDK\\include"
		"$ENV{ProgramW6432}\\SoftKinetic\\DepthSenseSDK\\include"
    		${PROJECT_SOURCE_DIR}/external_lib/include/
		DOC "The directory where DepthSense.hxx resides")
	FIND_LIBRARY(DEPTHSENSE_LIBRARIES
		NAMES DepthSense.lib
		PATHS "C:/Program Files\\SoftKinetic\\DepthSenseSDK\\lib"
		"C:/Program Files (x86)\\SoftKinetic\\DepthSenseSDK\\lib"
		"$ENV{PROGRAMFILES}\\SoftKinetic\\DepthSenseSDK\\lib"
		"$ENV{ProgramW6432}\\SoftKinetic\\DepthSenseSDK\\lib"
    		${PROJECT_SOURCE_DIR}/external_lib/lib/
		DOC "The DEPTHSENSE library")
ELSE (WIN32)
	FIND_PATH( DEPTHSENSE_INCLUDE_DIR DepthSense.hxx
		PATHS /opt/softkinetic/DepthSenseSDK/include/
    		${PROJECT_SOURCE_DIR}/external_lib/include/
		DOC "The directory where DepthSense.hxx resides")
	FIND_LIBRARY(DEPTHSENSE_LIBRARIES
		NAMES DepthSense
		PATHS /opt/softkinetic/DepthSenseSDK/lib/
    		${PROJECT_SOURCE_DIR}/external_lib/lib/
		DOC "The DEPTHSENSE library")

ENDIF(WIN32)

IF (DEPTHSENSE_INCLUDE_DIR AND DEPTHSENSE_LIBRARIES)
	SET( DEPTHSENSE_FOUND ON)
ELSE(DEPTHSENSE_INCLUDE_DIR AND DEPTHSENSE_LIBRARIES)
	SET( DEPTHSENSE_FOUND OFF)
ENDIF(DEPTHSENSE_INCLUDE_DIR AND DEPTHSENSE_LIBRARIES)

MARK_AS_ADVANCED( DEPTHSENSE_FOUND )
if(DEPTHSENSE_FOUND)
  include_directories(${DEPTHSENSE_INCLUDE_DIR})
endif(DEPTHSENSE_FOUND)
