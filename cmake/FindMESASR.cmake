#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * FindMESASR.cmake created in 05 2014.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * FindMESASR.cmake is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/
#
# Try to find mesasr library and include path.
# Once done this will define
#
# MESASR_FOUND
# MESASR_INCLUDE_PATH
# MESASR_LIBRARY
#

IF (WIN32)
	FIND_PATH( MESASR_INCLUDE_PATH libMesaSR.h
		$ENV{PROGRAMFILES}/include
		$ENV{PROGRAMFILES}/MesaImaging/include
		$ENV{PROGRAMFILES}/MesaImaging/
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/include
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/libMesaSR/include
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/libMesaSR/
    		${PROJECT_SOURCE_DIR}/external_lib/include
		DOC "The directory where libMesaSR.h resides")
	FIND_LIBRARY(MESASR_LIBRARY
		NAMES libMesaSR
		PATHS
		$ENV{PROGRAMFILES}/lib
		$ENV{PROGRAMFILES}/MesaImaging/lib
		$ENV{PROGRAMFILES}/MesaImaging/
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/lib
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/libMesaSR/lib
		$ENV{PROGRAMFILES}/MesaImaging/SwissRanger/libMesaSR/
    		${PROJECT_SOURCE_DIR}/external_lib/lib
		DOC "The MESASR library")
ELSE (WIN32)
	FIND_PATH( MESASR_INCLUDE_PATH libMesaSR.h
		/usr/include
		/usr/local/include
		/sw/include
		/opt/local/include
		DOC "The directory where libMesaSR.h resides")
	FIND_LIBRARY( MESASR_LIBRARY
		NAMES mesasr
		PATHS
		/usr/lib64
		/usr/lib
		/usr/local/lib64
		/usr/local/lib
		/sw/lib
		/opt/local/lib
		DOC "The MESASR library")
ENDIF (WIN32)

IF (MESASR_INCLUDE_PATH)
	SET( MESASR_FOUND ON)
ELSE (MESASR_INCLUDE_PATH)
	SET( MESASR_FOUND OFF)
ENDIF (MESASR_INCLUDE_PATH)

MARK_AS_ADVANCED( MESASR_FOUND )
