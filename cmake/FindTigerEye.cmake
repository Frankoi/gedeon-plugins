#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * FindTigerEye.cmake created in 06 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * FindTigerEye.cmake is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

# This sets the following variables:
# TIGEREYE_FOUND - True if OpenNI was found.
# TIGEREYE_HEADERS_PATH - The path to the tigereye header files.
# TIGEREYE_SOURCES_PATH - The path to the tigereye source files.

find_path(TIGEREYE_HEADERS_PATH tigereyegrabber.hpp tigereyedriver.hpp
          PATHS "${GEDEON_SOURCE_DIR}/modules/grabber/include/gedeon/grabber/"
		"${GEDEON_SOURCE_DIR}/modules/non-free/include/gedeon/non-free/")

find_path(TIGEREYE_SOURCES_PATH tigereyegrabber.cpp tigereyedriver.cpp
          PATHS "${GEDEON_SOURCE_DIR}/modules/grabber/src/"
		"${GEDEON_SOURCE_DIR}/modules/non-free/src/")

IF(TIGEREYE_HEADERS_PATH AND TIGEREYE_SOURCES_PATH)

SET(TIGEREYE_FOUND TRUE)
mark_as_advanced(TIGEREYE_FOUND TIGEREYE_HEADERS_PATH TIGEREYE_SOURCES_PATH)

ENDIF(TIGEREYE_HEADERS_PATH AND TIGEREYE_SOURCES_PATH)

