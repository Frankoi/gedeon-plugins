#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * FindFlyCapture.cmake created in 09 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * FindFlyCapture.cmake is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

FIND_PATH(FLYCAPTURE_INCLUDE_DIR FlyCapture2.h
	/usr/include/
	/usr/local/include/
	${CMAKE_SOURCE_DIR}/thirdparty/flycapture/include/
	"C:/Program Files\\Point Grey Research\\FlyCapture2\\include"
	"$ENV{PROGRAMFILES}\\Point Grey Research\\FlyCapture2\\include"
	"$ENV{ProgramW6432}\\Point Grey Research\\FlyCapture2\\include"

      )

 FIND_LIBRARY( FLYCAPTURE_LIBRARY
		NAMES flycapture FlyCapture2.lib
		PATHS
		${PROJECT_SOURCE_DIR}/thirdparty/flycapture/lib/
		"$ENV{PROGRAMFILES}\\Point Grey Research\\FlyCapture2\\lib\\"
		"$ENV{PROGRAMFILES}\\Point Grey Research\\FlyCapture2\\lib32\\"
		"$ENV{PROGRAMFILES}\\Point Grey Research\\FlyCapture2\\lib64\\"
		"$ENV{ProgramW6432}\\Point Grey Research\\FlyCapture2\\lib\\"
		"$ENV{ProgramW6432}\\Point Grey Research\\FlyCapture2\\lib32\\"
		"$ENV{ProgramW6432}\\Point Grey Research\\FlyCapture2\\lib64\\"
		DOC "The FLYCAPTURE library")

SET( FLYCAPTURE_FOUND OFF )

IF(FLYCAPTURE_INCLUDE_DIR AND FLYCAPTURE_LIBRARY)
	SET(FLYCAPTURE_FOUND ON )
	INCLUDE_DIRECTORIES(${FLYCAPTURE_INCLUDE_DIR})
ENDIF()

MARK_AS_ADVANCED( FLYCAPTURE_FOUND FLYCAPTURE_INCLUDE_DIR FLYCAPTURE_LIBRARY)

