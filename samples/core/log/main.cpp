#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * main.cpp created in 02 2014.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * main.cpp is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

#include "gedeon/core/core.hpp"
#include <cstdlib>

using namespace gedeon;


class AnotherLogger : public Logger {
public:
	AnotherLogger(void) : Logger() {}
	~AnotherLogger(void) {}
	void warning(EventDataWPtr e){
		std::cout << "AnotherLogger: Warning" << std::endl;
	}
	void error(EventDataWPtr e){
		std::cout << "AnotherLogger: Error" << std::endl;
	}
	void debug(EventDataWPtr e){
		LogData * ld = getData(e);
		if(0 != ld){
			std::cout << "AnotherLogger Debug: " + ld->func_origin + ", " + ld->msg_description + ", " + timeStampToString(ld->timestamp) << std::endl;
		}
	}
	void info(EventDataWPtr e){
		std::cout << "AnotherLogger: Info" << std::endl;
	}
};


int main(int argc, char **argv) {

	Log::add().info("This should not be displayed");

	Logger *logger = new Logger();

	Log::add().info("This should be displayed");

	Log::add().error("No function","Another example");

	Log::add().error("main","Extra example");
	
	Log::add().debug("main","debug");

	Logger *anlogger = new AnotherLogger();

	Log::add().error("main","Should show two error messages: this one and the one from the new logger");

	delete logger;

	Log::add().warning("main","Warning that will change");

	Log::add().debug("main","a debug message");

	delete anlogger;

	return EXIT_SUCCESS;
}

