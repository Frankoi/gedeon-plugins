#/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
# * main.cpp created in 05 2013.
# * Mail : fdesorbi@hvrl.ics.keio.ac.jp
# *
# * main.cpp is part of the GEDEON Library.
# *
# * The GEDEON Library is free software; you can redistribute it and/or modify
# * it under the terms of the GNU Lesser General Public License as published by
# * the Free Software Foundation; either version 3 of the License, or
# * (at your option) any later version.
# *
# * The GEDEON Library is distributed in the hope that it will be useful,
# * but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# * GNU Lesser General Public License for more details.
# *
# * You should have received a copy of the GNU Lesser General Public License
# * along with this program.  If not, see <http://www.gnu.org/licenses/>.
# *
# ***************************************************************************/

#include "gedeon/core/core.hpp"
#include <cstdlib>

using namespace gedeon;

void myStartEventReceiver(EventDataWPtr e) {
	EventDataPtr evptr = e.lock();
	if (0 != evptr) {
		std::cout << "Start Event received: " << evptr->name << std::endl;
	}
}

void myStopEventReceiver(EventDataWPtr e) {
	EventDataPtr evptr = e.lock();
	if (0 != evptr) {
		std::cout << "Stop Event received: " << evptr->name << std::endl;
	}
}

int main(int argc, char **argv) {

	EventSender sender;

	std::cout << "Add one event named start" << std::endl;
	sender.addEventName("start");

	std::cout << "Add one listener for the start event" << std::endl;
	boost::signals2::connection c1;
	if(false == sender.connect("start",
			boost::bind(myStartEventReceiver, _1),c1)){
		std::cerr << "Error: Cannot connect to the event start" << std::endl;
	}

	std::cout << "Send a start event..." << std::endl;
	EventDataPtr evptr(new EventData("start"));
	if(false == sender.emitEvent(evptr)){
		std::cerr << "Error: The event stop does not exist" << std::endl;
	}

	std::cout << "Send a stop event..." << std::endl;
	evptr->name = "stop";
	if(false == sender.emitEvent(evptr)){
		std::cerr << "Error: The event stop does not exist" << std::endl;
	}

	std::cout << "Add another observer for stop event" << std::endl;
	boost::signals2::connection c2;
	if(false == sender.connect("stop",
			boost::bind(myStopEventReceiver, _1),c1)){
		std::cerr << "Error: Cannot connect to the event stop" << std::endl;
	}

	std::cout << "Add one event named stop" << std::endl;
	sender.addEventName("stop");

	std::cout << "Try again to add another observer for stop event" << std::endl;
	if(false == sender.connect("stop",
				boost::bind(myStopEventReceiver, _1),c2)){
		std::cerr << "Error: Cannot connect to the event stop" << std::endl;
		}

	std::cout << "Send a stop event..." << std::endl;
	if(false == sender.emitEvent(evptr)){
		std::cerr << "Error: The event stop does not exist" << std::endl;
	}

	std::cout << "Remove all the listeners" << std::endl;
	c1.disconnect();
	c2.disconnect();

	std::cout << "Send a stop event..." << std::endl;
	if(false == sender.emitEvent(evptr)){
		std::cerr << "Error: The event stop does not exist" << std::endl;
	}

	return EXIT_SUCCESS;
}

