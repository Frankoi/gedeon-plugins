/***************************************************************************\
# * Copyright (C) by Francois de Sorbier
 # * main.cpp created in 05 2014.
 # * Mail : fdesorbi@hvrl.ics.keio.ac.jp
 # *
 # * main.cpp is part of the GEDEON Library.
 # *
 # * The GEDEON Library is free software; you can redistribute it and/or modify
 # * it under the terms of the GNU Lesser General Public License as published by
 # * the Free Software Foundation; either version 3 of the License, or
 # * (at your option) any later version.
 # *
 # * The GEDEON Library is distributed in the hope that it will be useful,
 # * but WITHOUT ANY WARRANTY; without even the implied warranty of
 # * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # * GNU Lesser General Public License for more details.
 # *
 # * You should have received a copy of the GNU Lesser General Public License
 # * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 # *
 # ***************************************************************************/

#include "gedeon/core/core.hpp"
#include "gedeon/capturedevice/plugin.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace gedeon;

cv::Mat intensity;
cv::Mat color;
cv::Mat depth;
boost::mutex mutex;

void displayImage(EventDataWPtr e) {
	if (EventDataPtr evptr = e.lock()) {
		if (0 != evptr && 0 != evptr->sender) {
			try {
				Grabber * s = dynamic_cast<Grabber*>(evptr->sender);
				RGBDIImage *imgptr = s->getImage().get();
				if (true == imgptr->intensity.isSet()) {
					if (0 == intensity.data || int(s->getImage()->intensity.getSize().height) != intensity.rows) {
						intensity = cv::Mat(
								s->getImage()->intensity.getSize().height,
								s->getImage()->intensity.getSize().width,
								CV_8UC1);
					}
					boost::mutex::scoped_lock l(mutex);
					memcpy(intensity.data, imgptr->intensity.getData().get(),
							s->getImage()->intensity.getSize().width
									* s->getImage()->intensity.getSize().height
									* sizeof(unsigned char));
				}
				if (true == imgptr->color.isSet()) {
					if (0 == color.data || int(s->getImage()->color.getSize().height) != color.rows) {
						color = cv::Mat(s->getImage()->color.getSize().height,
								s->getImage()->color.getSize().width, CV_8UC3);
					}
					boost::mutex::scoped_lock l(mutex);
					memcpy(color.data, imgptr->color.getData().get(),
							s->getImage()->color.getSize().width
									* s->getImage()->color.getSize().height * 3
									* sizeof(unsigned char));
					cvtColor(color, color, cv::COLOR_RGB2BGR);
				}
				if (true == imgptr->depth.isSet()) {
					if (0 == depth.data || int(s->getImage()->depth.getSize().height) != depth.rows) {
						depth = cv::Mat(s->getImage()->depth.getSize().height,
								s->getImage()->depth.getSize().width, CV_32FC1);
					}
					boost::mutex::scoped_lock l(mutex);
					memcpy(depth.data, imgptr->depth.getData().get(),
							s->getImage()->depth.getSize().height
									* s->getImage()->depth.getSize().width
									* sizeof(float));
					depth *= 1.0 / 5.0f;
				}

			} catch (std::bad_cast e) {
				Log::add().error("displayImage",
						"The event sender cannot be converted into a grabber");
			}
		}
	}
}

std::string displayAuxiliaryBuffer(const AuxiliaryBuffer& b) {

	std::string st;

	st.append( "   " + b.type.name + " (" + b.type.description + ")\n");
	st.append( "      Size: " + IO::numberToString(b.size.width) + "x" +  IO::numberToString(b.size.height) + ", depth: " + depthString[b.depth] + ", Channels: " + IO::numberToString(b.channels));
	st.append("\n");

	return st;
}

std::string displayFormat(const Format& f, const std::string& type) {
	std::string st;
	if (f.resolution.width > 0) {
		st.append(
				type + " resolution: " + IO::numberToString(f.resolution.width)
						+ "x" + IO::numberToString(f.resolution.height) + "\n");
		st.append(
				"   Pixel format: " + PixelFormatString[f.format] + " ("
						+ IO::numberToString(f.format) + ")\n");
		st.append(
				"   Current framerate: " + IO::numberToString(f.framerate_num)
						+ "/" + IO::numberToString(f.framerate_denom) + " ("
						+ IO::numberToString(
								float(f.framerate_num) / f.framerate_denom)
						+ ")\n");
		std::string s = "   Available framerates:   ";
		std::list<std::pair<unsigned int, unsigned int> >::const_iterator it =
				f.framerates.begin();
		while (f.framerates.end() != it) {
			s += IO::numberToString(float(it->first) / it->second) + "("
					+ IO::numberToString(it->first) + "/"
					+ IO::numberToString(it->second) + ") ";
			++it;
		}
		s.append("\n");
		st.append(s);
	}
	return st;
}

std::string displayDeviceFormat(const DeviceFormat& df) {
	std::string st;
	st.append("   ");
	st.append(displayFormat(df.color, "Color"));
	st.append("   ");
	st.append(displayFormat(df.depth, "Depth"));
	st.append("   ");
	st.append(displayFormat(df.intensity, "Intensity"));
	return st;
}

std::string displayProperty(const Property& p) {
	std::string st;
	st.append(" *" + p.type.name + " (" + p.type.description + ")\n");
	st.append(
			"     Is changeable: " + IO::numberToString(p.info.changeable)
					+ ", is auto/manual value: "
					+ IO::numberToString(p.info.has_auto_manual_mode) + "("
					+ IO::numberToString(p.auto_manual_mode) + "),\n");
	st.append(
			"     is float value: " + IO::numberToString(p.info.has_fvalue)
					+ "(" + IO::numberToString(p.fvalue) + "), is int value: "
					+ IO::numberToString(p.info.has_ivalue) + "("
					+ IO::numberToString(p.ivalue) + "),\n");
	st.append(
			"     is on/off value: " + IO::numberToString(p.info.has_on_off)
					+ "(" + IO::numberToString(p.on_off)
					+ "), is string value: "
					+ IO::numberToString(p.info.has_svalue) + "("
					+ IO::numberToString(p.svalue) + "),\n");
	st.append("     is a matrix: " + IO::numberToString(p.info.has_matrix) + "(" + IO::numberToString(p.mat_size.width) + "x" + IO::numberToString(p.mat_size.height) + ")\n");
	st.append("\n");
	return st;
}

bool capture(Driver& d, const std::string& s = "") {
	GrabberPtr sensor;

	try {
		if (s.length() == 0) {
			Log::add().info("Looking for " + d.getName() + " devices...");

			d.update();

			Log::add().info(
					"Number of devices found: "
							+ IO::numberToString(d.getCount()));

			if (0 == d.getCount() && s.length() == 0) {
				Log::add().error("capture", "No device available");
				return false;
			}

			std::list<std::string>::const_iterator ibegin;
			std::list<std::string>::const_iterator iend;
			d.populate(ibegin, iend);

			std::list<std::string>::const_iterator ibegincopy = ibegin;
			std::string s_names("List of devices...\n");
			while (ibegincopy != iend) {
				s_names.append(" * " + *ibegincopy + "\n");
				++ibegincopy;
			}
			Log::add().info(s_names);

			Log::add().info("Access the first device...");

			sensor = d.getGrabber(*ibegin);
		} else {
			sensor = d.getGrabberFromFile(s);
		}
	} catch (Exception e) {
		Log::add().error("capture", e.errorString());
		return false;
	}

	boost::signals2::connection c;
	try {
		if (false
				== sensor->connect("updated", boost::bind(displayImage, _1),
						c)) {
			Log::add().error("capture",
					"Cannot connect to the event of the sensor");
		}
	} catch (Exception e) {
		Log::add().error("capture", e.errorString());
		return false;
	}

	Log::add().info("Start the capture...");
	sensor->startCapture();

	bool running = true;
	int count = 0;
	while (true == running) {
		++count;
		char k = cv::waitKey(10);
		if (k == 27 || k == 'q') {
			running = false;
		}
		if (k == 'h') {
			std::string st;
			st.append("\nHelp\n");
			st.append("------------\n");
			st.append("q: quit the program\n");
			st.append("p: pause\n");
			st.append("l: list of sizes and framerates\n");
			st.append("f: Change the resolution/framerate of the device\n");
			st.append("i: Change infrared/color\n");
			st.append("r: Record in a .oni file\n");
			st.append("------------\n");
			st.append("Can use: capturedevice-openni2 [file .oni]\n");
			Log::add().info(st);
		}

		if (k == 'p') {
			sensor->pauseCapture();
		}

		if (k == 'l') {
			std::string st;

			st.append("Current device formats:\n");
			st.append(displayDeviceFormat(sensor->getCurrentFormat()));
			st.append("\n");
			st.append("------------------\n");
			st.append("Device formats:\n");
			std::list<DeviceFormat>::const_iterator from;
			std::list<DeviceFormat>::const_iterator to;
			sensor->getAvailableFormats(from, to);
			while (from != to) {
				st.append("\n");
				st.append(displayDeviceFormat(*from));
				++from;
			}
			st.append("------------------\n");

			st.append("Auxiliary buffers:\n");
			std::map<std::string,AuxiliaryBuffer>::const_iterator from_buff;
			std::map<std::string,AuxiliaryBuffer>::const_iterator to_buff;
			sensor->getAvailableAuxiliaryBuffers(from_buff, to_buff);
			if(from_buff == to_buff){
				st.append("  No auxiliary buffer\n");
			}
			while (from_buff != to_buff) {
				st.append(displayAuxiliaryBuffer(from_buff->second));
				++from_buff;
			}
			st.append("------------------\n");

			st.append("Device Properties:\n");
			std::map<std::string, Property>::const_iterator from_prop;
			std::map<std::string, Property>::const_iterator to_prop;
			sensor->getAvailableProperties(from_prop, to_prop);
			while (from_prop != to_prop) {
				st.append(displayProperty(from_prop->second));
				++from_prop;
			}
			Log::add().info(st);
		}

		if (k == 'i') {
			Property p;
			p.type.name = "Infrared";
			if (true == sensor->getProperty(p)) {
				p.on_off = !p.on_off;
			}
			sensor->setProperty(p);
		}

		if (k == 'r') {
			Property p;
			p.type.name = "Record";
			if (true == sensor->getProperty(p) && true == p.info.has_svalue
					&& true == p.info.changeable) {
				p.svalue = "openni.oni";
			}
			sensor->setProperty(p);
		}

		if (k == 'f') {
			DeviceFormat df = sensor->getCurrentFormat();
			if (df.color.resolution == Size(640, 480)) {
				df.color.resolution = Size(320, 240);
				df.depth.resolution = Size(320, 240);
				df.intensity.resolution = Size(320, 240);
				df.color.framerate_num = 60;
				df.depth.framerate_num = 60;
				df.intensity.framerate_num = 60;
			} else {
				df.color.resolution = Size(640, 480);
				df.depth.resolution = Size(640, 480);
				df.intensity.resolution = Size(640, 480);
				df.color.framerate_num = 30;
				df.depth.framerate_num = 30;
				df.intensity.framerate_num = 30;
			}
			sensor->setFormat(df);
		}

		if (intensity.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Intensity", intensity);
		}
		if (color.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Color", color);
		}
		if (depth.data != 0) {
			boost::mutex::scoped_lock l(mutex);
			cv::imshow("Depth", depth);
		}

	}

	Log::add().info("Stop the capture...");
	sensor->stopCapture();

	c.disconnect();

	return true;
}

void exit(void){
	do {
		Log::add().info("Press the Enter key to continue.");
	} while (std::cin.get() != '\n');
}

int main(int argc, char **argv) {

	Logger logger;

	Plugin *plugin = 0;

	try {
#if defined _WIN32 || defined _MSC_VER
		plugin = new Plugin(".","gedeon_plugin_openni");
#else
		plugin = new Plugin("../plugins", "gedeon_plugin_openni");
#endif
	} catch (Exception& e) {
		Log::add().error("main", e.errorString());
		exit();
		return EXIT_FAILURE;
	}

	Driver& d = plugin->getDriver();

	std::string input = "";
	if (2 == argc) {
		input = argv[1];
	}

	bool runout = capture(d, input);

	cv::destroyAllWindows();

	delete plugin;

	if (false == runout) {
		exit();
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

